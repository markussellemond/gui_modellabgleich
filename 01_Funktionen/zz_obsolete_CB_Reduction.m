function [MM, KK, DD, BB, ASET, A]=CB_Reduction(M, K, D, B, ASET, komp, akt_komp, nodes_bound, anz_AM)
% Function for Craig-Bampton-Reduction of M, K, D, B
%
% Input:    Matrizes M, K, D, B to be reduced 
%           ASET: struct of nodes for all komponents
%           komp: cell with name of all komponents
%           akt_komp: integer number of komponent to be reduced
%           nodes_bound: array with boundary nodes
%           ans_AM: integer number of dynamic modes to be retained
%
% Output:   Reduced Matrizes MM, KK, DD, BB
%           costumized ASET & A

%Kein ASET sondern A
if ~isstruct(ASET)
    [~, bb]=ismember(nodes_bound, ASET); %Boundary
else
    [~, bb]=ismember(nodes_bound, ASET.(komp{akt_komp})(:,3) ); %Boundary
end

bb(bb==0)=[];
bb=unique(sort(bb));

ii1=1:length(M); %Inner Nodes
[~, ii]=setdiff(ii1, bb);

%ASET anpassen
if ~isstruct(ASET)
    A=ASET(bb);
else
    ASET.(komp{akt_komp})=ASET.(komp{akt_komp})(bb,:);
    aset=[];
    for ll=1:length(komp)
        aset=[aset; ASET.(komp{ll})(:,3)];
    end
    ASET.GES = unique(aset);
    A=unique(ASET.GES);
end

%Matrizen aufteilen
M_rr_AB=M(bb,bb);
M_rl_AB=M(bb,ii);
M_lr_AB=M(ii,bb);
M_ll_AB=M(ii,ii);
M_sort=[M_rr_AB, M_rl_AB; M_lr_AB, M_ll_AB];

K_rr_AB=K(bb,bb);
K_rl_AB=K(bb,ii);
K_lr_AB=K(ii,bb);
K_ll_AB=K(ii,ii);
K_sort=[K_rr_AB, K_rl_AB; K_lr_AB, K_ll_AB];

if ~isempty(D)
    D_rr_AB=D(bb,bb);
    D_rl_AB=D(bb,ii);
    D_lr_AB=D(ii,bb);
    D_ll_AB=D(ii,ii);
    D_sort=[D_rr_AB, D_rl_AB; D_lr_AB, D_ll_AB];
end
if ~isempty(B)
    B_rr_AB=B(bb,bb);
    B_rl_AB=B(bb,ii);
    B_lr_AB=B(ii,bb);
    B_ll_AB=B(ii,ii);
    B_sort=[B_rr_AB, B_rl_AB; B_lr_AB, B_ll_AB];
end

% Guyan
 fi_r=-K_ll_AB\K_lr_AB;

% Anzahl Ausgleichsmoden
if anz_AM>size(K_ll_AB,1)
    anz_AM=size(K_ll_AB,1);
end
% [fi_l, w2_l]=eigs_sort_norm_sv(K_ll_AB, M_ll_AB, anz_AM);
% [fi_l, w2_l]=eigs_sort_norm(K_ll_AB, M_ll_AB, anz_AM);
[fi_l, w2_l]=eigs(K_ll_AB,M_ll_AB,anz_AM,'sm');

%Reduktionmatrix R
R=[eye(size(fi_r,2)), zeros(size(fi_r,2), size(w2_l,2)); fi_r, fi_l];
MM=R'*M_sort*R;
KK=R'*K_sort*R;
if ~isempty(D)
    DD=R'*D_sort*R;
else
    DD=[];
end
if ~isempty(B)
    BB=R'*B_sort*R;
else
    BB=[];
end

% [~,wCBnew]=eig(MM\KK);
% wCBnewsort=sort(diag((sqrt(wCBnew)/(2*pi))));
% [~,wnew]=eig(M\K);
% wnewsort=sort(diag((sqrt(wnew)/(2*pi))));
% 
% %bis 500 Hz
% delta_f=(abs(wCBnewsort(1:24,1)-wnewsort(1:24,1)))./wnewsort(1:24,1);
% figure();
% plot(wnewsort(1:24,1),delta_f*100);

end