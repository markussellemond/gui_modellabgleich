function [M_mod, K_mod, D_mod, G, S]=sort4ss(M_Ges,K_Ges,D_Ges,V_Ges,T,s,g)
%% modeles ZRM aus CB
io_port_s=length(s(:,1));
S=zeros(length(M_Ges),io_port_s); %Anpassung auf ZRM Form rb
if isempty(T)
    T = eye(length(M_Ges));
end
for i=1:io_port_s
    if s(i,2)~=0
        S(s(i,1),s(i,2))=1;
    end
end
S(:,~any(S))=[]; %L�scht 0-Spalten (Koppelpunkte die f�r ZRM nicht ben�tigt werden)

io_port_g=length(g(:,1));
G=zeros(length(M_Ges)*2,io_port_g);%Anpassung auf ZRM Form rb
for i=1:io_port_g
    if g(i,2)~=0
        G(g(i,1),g(i,2))=1;
    end
end
G(:,~any(G))=[]; %L�scht 0-Spalten (Koppelpunkte die f�r ZRM nicht ben�tigt werden)

%Modale Transformation
M_mod=(T*(diag(diag(V_Ges'*M_Ges*V_Ges)))*T');
K_mod=(T*(diag(diag(V_Ges'*K_Ges*V_Ges)))*T');
D_mod=(T*(diag(diag(V_Ges'*D_Ges*V_Ges)))*T');

end