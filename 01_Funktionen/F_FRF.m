function [freq, gamma, H, H_1, H_2, phase_1, phase_2] = ...
    F_FRF(t, data_1, data_2, L_signal, N_mit, T_pause, Integration, time_start, time_stop, UF, OF)
%F_FRF - Kurzbeschreibung
%Funktion berechnet Frequenzgangfunktion. Störungen am Ausgang/Eingang werden
%eingeschränkt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FUNKTIONSAUFRUF
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%[freq, gamma, H, H_1, H_2, phase_1, phase_2] = ...
%    F_FRF(t, data_1, data_2, L_signal, N_mit, T_pause, Integration, time_start, time_stop, UF, OF)
%INPUT
%t - Zeitvektor
%data_1 - Anregung
%data_2 - Antwort
%L_signal - Länge des tatsächlichen Nutzsignals
%N_mit - Anzahl der Mittelungen
%T_pause - Pause zwischen den Signalen in sek
%Integration - Gibt an wie oft integriert werden soll
%time_start - Zeit ab der das Nutzsignal ausgewertet wird
%time_stop - Zeit bis das Nutzsignal ausgewertet wird
%UF - Untere Frequenz für die fineFFT
%OF - Obere Frequenz für die fineFFT
%OUTPUT
%freq - Frequenzvektor 
%gamma - Kohärenzfunktion
%H - imaginäre Übertragungsfunktion jeder Mittelung 
%H_1 - imaginäre Übertragungsfunktion nach H1 Methode 
%H_2 - imaginäre Übertragungsfunktion nach H2 Methode 
%phase_1 - Phase der H1 Methode 
%phase_2 - Phase der H2 Methode
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin == 7 
    time_start = 0;
    time_stop = 0;
    UF = 0;
    OF = 0;
end

if nargin == 9 
    UF = 0;
    OF = 0;
end

[freq_1, absH_1, H_fft_1] = F_FFT(t, data_1, L_signal, N_mit, T_pause, 0, time_start, time_stop, UF, OF);
if (numel(freq_1) > 1)
    [freq_2, absH_2, H_fft_2] = F_FFT(t, data_2, L_signal, N_mit, T_pause, Integration, time_start, time_stop, UF, OF);
    if (numel(freq_2) > 1)
        freq = freq_1;

        %H1, H2 und gamma für alle Mittelungen berechnen
        gamma_oben(:,1) = zeros(length(freq),1);
        H_1_oben(:,1) = zeros(length(freq),1);
        H_2_oben(:,1) = zeros(length(freq),1);

        gamma_unten_1(:,1) = zeros(length(freq),1);
        gamma_unten_2(:,1) = zeros(length(freq),1);
        H_1_unten(:,1) = zeros(length(freq),1);
        H_2_unten(:,1) = zeros(length(freq),1);

        for i=1:N_mit

        gamma_oben = gamma_oben + (conj(H_fft_2(:,i)) .* H_fft_1(:,i));
        gamma_unten_1 = gamma_unten_1 + (conj(H_fft_2(:,i)).*H_fft_2(:,i));
        gamma_unten_2 = gamma_unten_2 + (conj(H_fft_1(:,i)).*H_fft_1(:,i));

        H_1_oben = H_1_oben + (conj(H_fft_1(:,i)) .* H_fft_2(:,i));
        H_1_unten = H_1_unten + (conj(H_fft_1(:,i)) .* H_fft_1(:,i));

        H_2_oben = H_2_oben + (conj(H_fft_2(:,i)) .* H_fft_2(:,i));
        H_2_unten = H_2_unten + (conj(H_fft_2(:,i)) .* H_fft_1(:,i));

        H(:,i) = H_fft_2(:,i) ./ H_fft_1(:,i);

        end

        gamma = (abs(gamma_oben./N_mit) .^2) ./ (gamma_unten_1./N_mit .* gamma_unten_2./N_mit);
        H_1 = H_1_oben ./ H_1_unten;
        H_2 = H_2_oben ./ H_2_unten;

        phase_1 = 180/pi*angle(H_1);
        phase_2 = 180/pi*angle(H_2);
        
    else
        freq = 0;
        gamma = 0;
        H = 0; 
        H_1 = 0;
        H_2 = 0;
        phase_1 = 0;
        phase_2 = 0;
    end
    
else
    freq = 0;
    gamma = 0;
    H = 0; 
    H_1 = 0;
    H_2 = 0;
    phase_1 = 0;
    phase_2 = 0;
end


