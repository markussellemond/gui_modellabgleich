function [KGes,DGes,BGes,DGesEq,EV0,EW0]=F_Koppelung_para(MGes,KGes,DGes,BGes,...
                        para,paraKomp,paraName,paraNode,pchKomp,ASET,printWarning)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%F_Koppelung_param - Kurzberschreibung       Funktion f�r Aufruf �ber GUI
%Funktion koppelt die dynamischen Matrizen der einzelnen Komponenten nach
%mit ausgew�hlten Parmetern f�r z.B. PSF, KGT oder AE �ber eigene Federn
%und D�mpfer
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FUNKTIONSAUFRUF: 
%INPUT:
% MGes:         Massenmatrix
% KGes:         Steifigkeitsmatrix
% DGes:         Viskose D�mpfungsmatrix
% BGes:         Hysteretische D�mpfungsmatrix
% para:         Array mit Kopelparametern
% paraKomp:     Komponentennamen zugeh�rig zu den Zeilen in Para (zweite
%                   Spalte aus excel, zB.: Bett, ySchlitten)
% paraName:     Namen der Parameter zugeh�rig zu den Zeilen in Para (erste
%                   Spalte aus excel: AE, MB, PSF, KGT, RM)
% paraNode:     Knotennummern der zu koppelnden BNDfix-Punkte
% pchKomp:      Namen der zu koppelnden Substrukturen. (es werden nur jene
%                   Zeilen des para-Array gekoppelt, die in pchKomp stehen
% ASET:         ASET matrix
% printWarning: Schalter zum ein und ausschalten der Warnung, wenn Knoten
%                   nicht gekoppelt werden k�nnen. (Bei Optimierung 
%                   ausschalten sonst boooom)
%OUTPUT:
% K_Ges, D_Ges, B_Ges D_GesEq - Gekoppelte dyn. Matrix, mit �quivalenter D�mpfungsmatrix
% EV0: Eigenvektoren (nicht normiert)
% EW0: Eigenwerte

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin <= 10
    printWarning = false;
end

ASET_sort=sort(ASET.GES(:,1));

koppelverfahren = 'new'; %um zu debugzwecken zwischen altem (funktionierend) und neuem koppelverfahren umzuschalten

if strcmp(koppelverfahren,'new')
    for n=1:length(paraKomp)%for all coupling elements (each spring/damper)
        coupleFlag = false; %Flag um zu �berpr�fen, ob koppelung erfolgreich war
        dof1=paraNode(n,1);
        dof2=paraNode(n,2);
        dofCounter = 0; % Z�hlt die anzahl der gekoppelten Richtungen je Fhg

        msgKomp = sprintf('%8s : %8s',paraKomp{n},paraName{n}); 
        msgDofs = '';
        msgInit = '';
        msgResult = '';
        try
            if dof1 == 0 || dof2 == 0
    %             fprintf('NodeNumber: 0\n')
                dof1 = NaN;
                dof2 = NaN;
                msgInit = '000';
                msgResult = 'no coupling';
            else    
                if  ~isempty(find(strcmp(paraKomp(n),pchKomp))) && ...%for every spring in the current assembly state (compares component names)
                    ~isnan(paraNode(n,1))

                    % 6 Feder-D�mpfer-Elemente koppeln
                    if ~isempty(strfind(paraName{n},'AE'))    % Aufstellelemente m�ssen gresondert behandelt werden, da nur ein Knoten gekoppelt wird
                        for m = 1:6 
                            % zu Koppelnde Parameter aus para extrahieren
                            k=para(n,m);
                            b=para(n,18+m);
                            d=para(n,24+m)*k;

                            % indices der Fhg finden
                            l1 = find(ASET_sort == dof1*10 + m);
                            l2 = find(ASET_sort == dof2*10 + m);

                            % nur wenn der Freiheitgrad vorhanden ist koppeln
                            if ~isempty(l1)
                                KGes(l1,l1) = KGes(l1,l1)+k;
                                DGes(l1,l1) = DGes(l1,l1)+d;    
                                BGes(l1,l1) = BGes(l1,l1)+b;

                                dofCounter = dofCounter + 1;
                                msgDofs = sprintf('%s [%i-%i]',msgDofs,dof1*10 + m,dof2*10 + m);
                            end               
                        end
                    else
                        for m = 1:6 
                            % zu Koppelnde Parameter aus para extrahieren
                            k=para(n,m);
                            b=para(n,18+m);
                            d=para(n,24+m)*k;

                            % indices der Fhg finden
                            l1 = find(ASET_sort == dof1*10 + m);
                            l2 = find(ASET_sort == dof2*10 + m);

                            % nur wenn der Freiheitgrad vorhanden ist koppeln
                            if ~isempty(l1) && ~isempty(l2) 
                                KGes(l1,l1)=KGes(l1,l1)+k;
                                KGes(l2,l2)=KGes(l2,l2)+k;
                                KGes(l1,l2)=KGes(l1,l2)-k;
                                KGes(l2,l1)=KGes(l2,l1)-k;

                                DGes(l1,l1)=DGes(l1,l1)+d;
                                DGes(l2,l2)=DGes(l2,l2)+d;
                                DGes(l1,l2)=DGes(l1,l2)-d;
                                DGes(l2,l1)=DGes(l2,l1)-d;

                                BGes(l1,l1)=BGes(l1,l1)+b;
                                BGes(l2,l2)=BGes(l2,l2)+b;
                                BGes(l1,l2)=BGes(l1,l2)-b;
                                BGes(l2,l1)=BGes(l2,l1)-b;

                                dofCounter = dofCounter + 1;
                                msgDofs = sprintf('%s [%i-%i]',msgDofs,dof1*10 + m,dof2*10 + m);
                            end               
                        end
                    end


                    %wenn KGT gekoppelt wird, m�ssen zus�tzlich
                    %Kreuzsteifigkeiten gekoppelt werden (translatorische
                    %federn wurden schon vorher eingezogen)
                    if ~isempty(strfind(paraName{n},'KGT')) 
                        l11=find(floor(ASET_sort(:,1)/10)==dof1);
                        l22=find(floor(ASET_sort(:,1)/10)==dof2);
                        l1 = l11(1,1);
                        l2 = l22(1,1);
                        for m=7:9%Cross Stiffnesses KGT %7=x to a / 8=y to b / 9=z to c 
                            k=para(n,m);
                            KGes(l1+m-7,l1+m-4)=KGes(l1+m-7,l1+m-4)+k;%see Oertli p.76 Gl. 103 
                            KGes(l1+m-4,l1+m-7)=KGes(l1+m-4,l1+m-7)+k;
                            KGes(l1+m-7,l2+m-4)=KGes(l1+m-7,l2+m-4)-k;
                            KGes(l1+m-4,l2+m-7)=KGes(l1+m-4,l2+m-7)-k;
                            KGes(l2+m-7,l1+m-4)=KGes(l2+m-7,l1+m-4)-k;
                            KGes(l2+m-4,l1+m-7)=KGes(l2+m-4,l1+m-7)-k;
                            KGes(l2+m-7,l2+m-4)=KGes(l2+m-7,l2+m-4)+k;
                            KGes(l2+m-4,l2+m-7)=KGes(l2+m-4,l2+m-7)+k;
                            dofCounter = dofCounter + 1;
                        end

                        for m=10%Cross Stiffnesses KGT x to b
                            k=para(n,m);
                            KGes(l1,l1+4)=KGes(l1,l1+4)+k;
                            KGes(l1+4,l1)=KGes(l1+4,l1)+k;
                            KGes(l1,l2+4)=KGes(l1,l2+4)-k;
                            KGes(l1+4,l2)=KGes(l1+4,l2)-k;
                            KGes(l2,l1+4)=KGes(l2,l1+4)-k;
                            KGes(l2+4,l1)=KGes(l2+4,l1)-k;
                            KGes(l2,l2+4)=KGes(l2,l2+4)+k;
                            KGes(l2+4,l2)=KGes(l2+4,l2)+k;
                            dofCounter = dofCounter + 1;
                        end

                        for m=11%Cross Stiffnesses KGT x to c
                            k=para(n,m);
                            KGes(l1,l1+5)=KGes(l1,l1+5)+k;
                            KGes(l1+5,l1)=KGes(l1+5,l1)+k;
                            KGes(l1,l2+5)=KGes(l1,l2+5)-k;
                            KGes(l1+5,l2)=KGes(l1+5,l2)-k;
                            KGes(l2,l1+5)=KGes(l2,l1+5)-k;
                            KGes(l2+5,l1)=KGes(l2+5,l1)-k;
                            KGes(l2,l2+5)=KGes(l2,l2+5)+k;
                            KGes(l2+5,l2)=KGes(l2+5,l2)+k;
                            dofCounter = dofCounter + 1;
                        end

                        for m=12%Cross Stiffnesses KGT y to a
                            k=para(n,m);
                            KGes(l1+1,l1+3)=KGes(l1+1,l1+3)+k;
                            KGes(l1+3,l1+1)=KGes(l1+3,l1+1)+k;
                            KGes(l1+1,l2+3)=KGes(l1+1,l2+3)-k;
                            KGes(l1+3,l2+1)=KGes(l1+3,l2+1)-k;
                            KGes(l2+1,l1+3)=KGes(l2+1,l1+3)-k;
                            KGes(l2+3,l1+1)=KGes(l2+3,l1+1)-k;
                            KGes(l2+1,l2+3)=KGes(l2+1,l2+3)+k;
                            KGes(l2+3,l2+1)=KGes(l2+3,l2+1)+k;
                            dofCounter = dofCounter + 1;
                        end

                        for m=13%Cross Stiffnesses KGT y to c
                            k=para(n,m);
                            KGes(l1+1,l1+5)=KGes(l1+1,l1+5)+k;
                            KGes(l1+5,l1+1)=KGes(l1+5,l1+1)+k;
                            KGes(l1+1,l2+5)=KGes(l1+1,l2+5)-k;
                            KGes(l1+5,l2+1)=KGes(l1+5,l2+1)-k;
                            KGes(l2+1,l1+5)=KGes(l2+1,l1+5)-k;
                            KGes(l2+5,l1+1)=KGes(l2+5,l1+1)-k;
                            KGes(l2+1,l2+5)=KGes(l2+1,l2+5)+k;
                            KGes(l2+5,l2+1)=KGes(l2+5,l2+1)+k;
                            dofCounter = dofCounter + 1;
                        end

                        for m=14%Cross Stiffnesses KGT z to a
                            k=para(n,m);
                            KGes(l1+2,l1+3)=KGes(l1+2,l1+3)+k;
                            KGes(l1+3,l1+2)=KGes(l1+3,l1+2)+k;
                            KGes(l1+2,l2+3)=KGes(l1+2,l2+3)-k;
                            KGes(l1+3,l2+2)=KGes(l1+3,l2+2)-k;
                            KGes(l2+2,l1+3)=KGes(l2+2,l1+3)-k;
                            KGes(l2+3,l1+2)=KGes(l2+3,l1+2)-k;
                            KGes(l2+2,l2+3)=KGes(l2+2,l2+3)+k;
                            KGes(l2+3,l2+2)=KGes(l2+3,l2+2)+k;
                            dofCounter = dofCounter + 1;
                        end

                        for m=15%Cross Stiffnesses KGT z to b
                            k=para(n,m);
                            KGes(l1+2,l1+4)=KGes(l1+2,l1+4)+k;
                            KGes(l1+4,l1+2)=KGes(l1+4,l1+2)+k;
                            KGes(l1+2,l2+4)=KGes(l1+2,l2+4)-k;
                            KGes(l1+4,l2+2)=KGes(l1+4,l2+2)-k;
                            KGes(l2+2,l1+4)=KGes(l2+2,l1+4)-k;
                            KGes(l2+4,l1+2)=KGes(l2+4,l1+2)-k;
                            KGes(l2+2,l2+4)=KGes(l2+2,l2+4)+k;
                            KGes(l2+4,l2+2)=KGes(l2+4,l2+2)+k;
                            dofCounter = dofCounter + 1;
                        end

                        for m=16%Cross Stiffnesses KGT a to b
                            k=para(n,m);
                            KGes(l1+3,l1+4)=KGes(l1+3,l1+4)+k;
                            KGes(l1+4,l1+3)=KGes(l1+4,l1+3)+k;
                            KGes(l1+3,l2+4)=KGes(l1+3,l2+4)-k;
                            KGes(l1+4,l2+3)=KGes(l1+4,l2+3)-k;
                            KGes(l2+3,l1+4)=KGes(l2+3,l1+4)-k;
                            KGes(l2+4,l1+3)=KGes(l2+4,l1+3)-k;
                            KGes(l2+3,l2+4)=KGes(l2+3,l2+4)+k;
                            KGes(l2+4,l2+3)=KGes(l2+4,l2+3)+k;
                            dofCounter = dofCounter + 1;
                        end

                        for m=17%Cross Stiffnesses KGT a to c
                            k=para(n,m);
                            KGes(l1+3,l1+5)=KGes(l1+3,l1+5)+k;
                            KGes(l1+5,l1+3)=KGes(l1+5,l1+3)+k;
                            KGes(l1+3,l2+5)=KGes(l1+3,l2+5)-k;
                            KGes(l1+5,l2+3)=KGes(l1+5,l2+3)-k;
                            KGes(l2+3,l1+5)=KGes(l2+3,l1+5)-k;
                            KGes(l2+5,l1+3)=KGes(l2+5,l1+3)-k;
                            KGes(l2+3,l2+5)=KGes(l2+3,l2+5)+k;
                            KGes(l2+5,l2+3)=KGes(l2+5,l2+3)+k;
                            dofCounter = dofCounter + 1;
                        end

                        for m=18%Cross Stiffnesses KGT b to c
                            k=para(n,m);
                            KGes(l1+4,l1+5)=KGes(l1+4,l1+5)+k;
                            KGes(l1+5,l1+4)=KGes(l1+5,l1+4)+k;
                            KGes(l1+4,l2+5)=KGes(l1+4,l2+5)-k;
                            KGes(l1+5,l2+4)=KGes(l1+5,l2+4)-k;
                            KGes(l2+4,l1+5)=KGes(l2+4,l1+5)-k;
                            KGes(l2+5,l1+4)=KGes(l2+5,l1+4)-k;
                            KGes(l2+4,l2+5)=KGes(l2+4,l2+5)+k;
                            KGes(l2+5,l2+4)=KGes(l2+5,l2+4)+k;
                            dofCounter = dofCounter + 1;
                        end

                        msgDofs = sprintf('%s %s',msgDofs,'cross stiffness KGT');
                    end
                end
                msgInit = '+++';
                msgResult = 'success';
                coupleFlag = true; %Koppelung erfolgreich
            end
        catch
            msgInit = '---';
            msgResult = 'failed';
        end
    
        %% R�ckmeldung �ber erfolg des Koppelvorganges
        if printWarning
            fprintf(sprintf('%s %s; nDofs = %3i; %s %s\n',msgInit,msgKomp,dofCounter,msgDofs,msgResult))
        end

    end
elseif strcmp(koppelverfahren,'old')   
%% Alter Stand    
    for n=1:length(paraKomp)%for all coupling elements (each spring/damper)
        coupleFlag = false; %Flag um zu �berpr�fen, ob koppelung erfolgreich war
        dof1=paraNode(n,1);
        dof2=paraNode(n,2);
        dofCounter = 0; % Z�hlt die anzahl der gekoppelten Richtungen je Fhg
        if  ~isempty(find(strcmp(paraKomp(n),pchKomp))) &...%for every spring in the current assembly state (compares component names)
            ~isnan(paraNode(n,1)) & ...                 % there must exist nodes able to be coupled
            ~contains(paraName{n},'AE') & ...            % Aufstellelemente ...
            ~contains(paraName{n},'MB') & ...            % Motorbremse ...
            ~contains(paraName{n},'RM')                  % Riemen ben�tigen besondere Behandlung, da nicht alle FHG gekoppelt werden
            try
                %find line to couple spring
                l11=find(floor(ASET_sort(:,1)/10)==dof1);
                l22=find(floor(ASET_sort(:,1)/10)==dof2);
                l1=l11(1,1);
                l2=l22(1,1);

                for m=1:6%for 6D 
                    k=para(n,m)
                    l1+m-1
                    l2+m-1
                    b=para(n,18+m);
                    d=para(n,24+m)*k;

                    KGes(l1+m-1,l1+m-1)=KGes(l1+m-1,l1+m-1)+k;
                    KGes(l2+m-1,l2+m-1)=KGes(l2+m-1,l2+m-1)+k;
                    KGes(l1+m-1,l2+m-1)=KGes(l1+m-1,l2+m-1)-k;
                    KGes(l2+m-1,l1+m-1)=KGes(l2+m-1,l1+m-1)-k;

                    DGes(l1+m-1,l1+m-1)=DGes(l1+m-1,l1+m-1)+d;
                    DGes(l2+m-1,l2+m-1)=DGes(l2+m-1,l2+m-1)+d;
                    DGes(l1+m-1,l2+m-1)=DGes(l1+m-1,l2+m-1)-d;
                    DGes(l2+m-1,l1+m-1)=DGes(l2+m-1,l1+m-1)-d;

                    BGes(l1+m-1,l1+m-1)=BGes(l1+m-1,l1+m-1)+b;
                    BGes(l2+m-1,l2+m-1)=BGes(l2+m-1,l2+m-1)+b;
                    BGes(l1+m-1,l2+m-1)=BGes(l1+m-1,l2+m-1)-b;
                    BGes(l2+m-1,l1+m-1)=BGes(l2+m-1,l1+m-1)-b;
                end

                for m=7:9%Cross Stiffnesses KGT %7=x to a / 8=y to b / 9=z to c 
                    k=para(n,m)
                    l1
                    l2
                    KGes(l1+m-7,l1+m-4)=KGes(l1+m-7,l1+m-4)+k;%see Oertli p.76 Gl. 103 
                    KGes(l1+m-4,l1+m-7)=KGes(l1+m-4,l1+m-7)+k;
                    KGes(l1+m-7,l2+m-4)=KGes(l1+m-7,l2+m-4)-k;
                    KGes(l1+m-4,l2+m-7)=KGes(l1+m-4,l2+m-7)-k;
                    KGes(l2+m-7,l1+m-4)=KGes(l2+m-7,l1+m-4)-k;
                    KGes(l2+m-4,l1+m-7)=KGes(l2+m-4,l1+m-7)-k;
                    KGes(l2+m-7,l2+m-4)=KGes(l2+m-7,l2+m-4)+k;
                    KGes(l2+m-4,l2+m-7)=KGes(l2+m-4,l2+m-7)+k;
                end

                for m=10%Cross Stiffnesses KGT x to b
                    k=para(n,m)
                    l1
                    l2
                    KGes(l1,l1+4)=KGes(l1,l1+4)+k;
                    KGes(l1+4,l1)=KGes(l1+4,l1)+k;
                    KGes(l1,l2+4)=KGes(l1,l2+4)-k;
                    KGes(l1+4,l2)=KGes(l1+4,l2)-k;
                    KGes(l2,l1+4)=KGes(l2,l1+4)-k;
                    KGes(l2+4,l1)=KGes(l2+4,l1)-k;
                    KGes(l2,l2+4)=KGes(l2,l2+4)+k;
                    KGes(l2+4,l2)=KGes(l2+4,l2)+k;
                end

                for m=11%Cross Stiffnesses KGT x to c
                    k=para(n,m)
                    l1
                    l2
                    KGes(l1,l1+5)=KGes(l1,l1+5)+k;
                    KGes(l1+5,l1)=KGes(l1+5,l1)+k;
                    KGes(l1,l2+5)=KGes(l1,l2+5)-k;
                    KGes(l1+5,l2)=KGes(l1+5,l2)-k;
                    KGes(l2,l1+5)=KGes(l2,l1+5)-k;
                    KGes(l2+5,l1)=KGes(l2+5,l1)-k;
                    KGes(l2,l2+5)=KGes(l2,l2+5)+k;
                    KGes(l2+5,l2)=KGes(l2+5,l2)+k;
                end

                for m=12%Cross Stiffnesses KGT y to a
                    k=para(n,m)
                    l1
                    l2
                    KGes(l1+1,l1+3)=KGes(l1+1,l1+3)+k;
                    KGes(l1+3,l1+1)=KGes(l1+3,l1+1)+k;
                    KGes(l1+1,l2+3)=KGes(l1+1,l2+3)-k;
                    KGes(l1+3,l2+1)=KGes(l1+3,l2+1)-k;
                    KGes(l2+1,l1+3)=KGes(l2+1,l1+3)-k;
                    KGes(l2+3,l1+1)=KGes(l2+3,l1+1)-k;
                    KGes(l2+1,l2+3)=KGes(l2+1,l2+3)+k;
                    KGes(l2+3,l2+1)=KGes(l2+3,l2+1)+k;
                end

                for m=13%Cross Stiffnesses KGT y to c
                    k=para(n,m)
                    l1
                    l2
                    KGes(l1+1,l1+5)=KGes(l1+1,l1+5)+k;
                    KGes(l1+5,l1+1)=KGes(l1+5,l1+1)+k;
                    KGes(l1+1,l2+5)=KGes(l1+1,l2+5)-k;
                    KGes(l1+5,l2+1)=KGes(l1+5,l2+1)-k;
                    KGes(l2+1,l1+5)=KGes(l2+1,l1+5)-k;
                    KGes(l2+5,l1+1)=KGes(l2+5,l1+1)-k;
                    KGes(l2+1,l2+5)=KGes(l2+1,l2+5)+k;
                    KGes(l2+5,l2+1)=KGes(l2+5,l2+1)+k;
                end

                for m=14%Cross Stiffnesses KGT z to a
                    k=para(n,m)
                    l1
                    l2
                    KGes(l1+2,l1+3)=KGes(l1+2,l1+3)+k;
                    KGes(l1+3,l1+2)=KGes(l1+3,l1+2)+k;
                    KGes(l1+2,l2+3)=KGes(l1+2,l2+3)-k;
                    KGes(l1+3,l2+2)=KGes(l1+3,l2+2)-k;
                    KGes(l2+2,l1+3)=KGes(l2+2,l1+3)-k;
                    KGes(l2+3,l1+2)=KGes(l2+3,l1+2)-k;
                    KGes(l2+2,l2+3)=KGes(l2+2,l2+3)+k;
                    KGes(l2+3,l2+2)=KGes(l2+3,l2+2)+k;
                end

                for m=15%Cross Stiffnesses KGT z to b
                    k=para(n,m)
                    l1
                    l2
                    KGes(l1+2,l1+4)=KGes(l1+2,l1+4)+k;
                    KGes(l1+4,l1+2)=KGes(l1+4,l1+2)+k;
                    KGes(l1+2,l2+4)=KGes(l1+2,l2+4)-k;
                    KGes(l1+4,l2+2)=KGes(l1+4,l2+2)-k;
                    KGes(l2+2,l1+4)=KGes(l2+2,l1+4)-k;
                    KGes(l2+4,l1+2)=KGes(l2+4,l1+2)-k;
                    KGes(l2+2,l2+4)=KGes(l2+2,l2+4)+k;
                    KGes(l2+4,l2+2)=KGes(l2+4,l2+2)+k;
                end

                for m=16%Cross Stiffnesses KGT a to b
                    k=para(n,m)
                    l1
                    l2
                    KGes(l1+3,l1+4)=KGes(l1+3,l1+4)+k;
                    KGes(l1+4,l1+3)=KGes(l1+4,l1+3)+k;
                    KGes(l1+3,l2+4)=KGes(l1+3,l2+4)-k;
                    KGes(l1+4,l2+3)=KGes(l1+4,l2+3)-k;
                    KGes(l2+3,l1+4)=KGes(l2+3,l1+4)-k;
                    KGes(l2+4,l1+3)=KGes(l2+4,l1+3)-k;
                    KGes(l2+3,l2+4)=KGes(l2+3,l2+4)+k;
                    KGes(l2+4,l2+3)=KGes(l2+4,l2+3)+k;
                end

                for m=17%Cross Stiffnesses KGT a to c
                    k=para(n,m)
                    l1
                    l2
                    KGes(l1+3,l1+5)=KGes(l1+3,l1+5)+k;
                    KGes(l1+5,l1+3)=KGes(l1+5,l1+3)+k;
                    KGes(l1+3,l2+5)=KGes(l1+3,l2+5)-k;
                    KGes(l1+5,l2+3)=KGes(l1+5,l2+3)-k;
                    KGes(l2+3,l1+5)=KGes(l2+3,l1+5)-k;
                    KGes(l2+5,l1+3)=KGes(l2+5,l1+3)-k;
                    KGes(l2+3,l2+5)=KGes(l2+3,l2+5)+k;
                    KGes(l2+5,l2+3)=KGes(l2+5,l2+3)+k;
                end

                for m=18%Cross Stiffnesses KGT b to c
                    k=para(n,m)
                    l1
                    l2
                    KGes(l1+4,l1+5)=KGes(l1+4,l1+5)+k;
                    KGes(l1+5,l1+4)=KGes(l1+5,l1+4)+k;
                    KGes(l1+4,l2+5)=KGes(l1+4,l2+5)-k;
                    KGes(l1+5,l2+4)=KGes(l1+5,l2+4)-k;
                    KGes(l2+4,l1+5)=KGes(l2+4,l1+5)-k;
                    KGes(l2+5,l1+4)=KGes(l2+5,l1+4)-k;
                    KGes(l2+4,l2+5)=KGes(l2+4,l2+5)+k;
                    KGes(l2+5,l2+4)=KGes(l2+5,l2+4)+k;
                end
                coupleFlag = true; %Koppelung erfolgreich
            catch
            end
        elseif ~isempty(find(strcmp(paraKomp(n),pchKomp))) && contains(paraName{n},'AE')%for every spring in the current assembly state
            try
                %find line to couple spring
                l11=find(floor(ASET_sort/10)==dof1);
                l1=l11(1,1);
                for m=1:6%for 6D 
                    k=para(n,m)
                    l1 + m - 1
                    l2 = NaN
                    b=para(n,18+m);
                    d=para(n,24+m)*k;
                    KGes(l1+m-1,l1+m-1) = KGes(l1+m-1,l1+m-1)+k;
                    DGes(l1+m-1,l1+m-1) = DGes(l1+m-1,l1+m-1)+d;    
                    BGes(l1+m-1,l1+m-1) = BGes(l1+m-1,l1+m-1)+b;
                end
                coupleFlag = true; %Koppelung erfolgreich            
            catch
            end

        elseif ~isempty(find(strcmp(paraKomp(n),pchKomp))) && contains(paraName{n},'MB')
            try
                m = find(para(n,:),1); %freiheitsgrad finden, der gesperrt werden soll
                if ~isempty(m)
                    l11=find(floor(ASET_sort)==dof1*10+m);
                    l22=find(floor(ASET_sort)==dof2*10+m);
                    l1=l11(1,1);
                    l2=l22(1,1);

                    k=para(n,m)
                    l1
                    l2

                    KGes(l1,l1)=KGes(l1,l1)+k;
                    KGes(l2,l2)=KGes(l2,l2)+k;
                    KGes(l1,l2)=KGes(l1,l2)-k;
                    KGes(l2,l1)=KGes(l2,l1)-k;
                end
                coupleFlag = true; %Koppelung erfolgreich
            catch
            end
        elseif ~isempty(find(strcmp(paraKomp(n),pchKomp))) && contains(paraName{n},'RM')
            try
                for m = 1:6
                    l11=find(floor(ASET_sort)==dof1*10+m,1);
                    l22=find(floor(ASET_sort)==dof2*10+m,1);
                    if ~isempty(l11) & ~isempty(l22) % nur freiheitsgrade koppeln, die auch wirklich vorhanden sind
                        l1 = l11(1);
                        l2 = l22(1);

                        k=para(n,m)
                        l1
                        l2
                        KGes(l1,l1)=KGes(l1,l1)+k;
                        KGes(l2,l2)=KGes(l2,l2)+k;
                        KGes(l1,l2)=KGes(l1,l2)-k;
                        KGes(l2,l1)=KGes(l2,l1)-k;

                        b=para(n,18+m);
                        BGes(l1,l1)=BGes(l1,l1)+b;
                        BGes(l2,l2)=BGes(l2,l2)+b;
                        BGes(l1,l2)=BGes(l1,l2)-b;
                        BGes(l2,l1)=BGes(l2,l1)-b;

                        d=para(n,24+m)*k;
                        DGes(l1,l1)=DGes(l1,l1)+d;
                        DGes(l2,l2)=DGes(l2,l2)+d;
                        DGes(l1,l2)=DGes(l1,l2)-d;
                        DGes(l2,l1)=DGes(l2,l1)-d;
                    end
                end
                coupleFlag = true; %Koppelung erfolgreich
            catch
            end
        end
        if printWarning
            if coupleFlag % Der Parameter konnte gekoppelt werden
%                 msg = sprintf('+++ %8s : %8s \tcoupled node %i and node %i\n',paraKomp{n},paraName{n},dof1,dof2);
%                 fprintf(msg);
            else % der Parameter konnte nicht gekoppelt werden
%                 errormsg = sprintf('--- %8s : %8s \tunable to couple node %i and node %i\n',paraKomp{n},paraName{n},dof1,dof2);
%                 fprintf(errormsg);
            end
        end
    end
end


%% falls n�tig: Modaltrafo und �quivalente D�#mpfungsmatrix
if nargout >= 4 % wenn kein D_gesEq ausgegeben werden soll, diese Berechung �berspringen um Zeit zu sparen
    [EV0,EW0] = eig(KGes,MGes);
    EV=EV0*sqrt(diag(1./diag(EV0'*MGes*EV0))); % speed-up durch vermeiden des invertierens der diagonalmatrix
    wGes = diag(sqrt(abs(EW0)));
    
    [wGes,I] = sort(wGes);
    EV = EV(:,I);

    % ein mal modaltransformieren -> Koppeln -> r�cktransformieren
    D01 = EV'*DGes*EV*diag(1./wGes); %hysteretisch
    B01 = EV'*BGes*EV;               %viskos
    DGesEq = MGes'*EV*(B01+D01)*EV'*MGes; %ersatzd�mpfung
end

end

%----- F_Koppelung END