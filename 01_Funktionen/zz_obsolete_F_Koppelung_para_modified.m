function [K_Ges,D_Ges,B_Ges,D_GesEq,EV0,EW0]=F_Koppelung_para(M_Ges,K_Ges,D_Ges,B_Ges,para,para_name,para_node,komp,ASET,printWarning)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%F_Koppelung_param - Kurzberschreibung
%Funktion koppelt die dynamischen Matrizen der einzelnen Komponenten nach
%mit ausgew�hlten Parmetern f�r z.B. PSF, KGT oder AE �ber eigene Federn
%und D�mpfer
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FUNKTIONSAUFRUF: 
%[K_Ges,K]=F_Koppelung_para(K_Ges,para,komp,ASET,M_Ges);
%PARAMETERS:
%INPUT:
%con - Anzahl an Koppelstellenfreiheitsgraden im Gesamtsystem(constrains)

%OUTPUT:
%Ges - Gekoppelte dyn. Matrix

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin <= 9
    printWarning = false;
end

ASET_sort=sort(ASET.GES);

for n=1:length(para_name)%for all coupling elements (each spring/damper)
    if  ~isempty(find(strcmp(para_name(n),komp))) && ~isnan(para_node(n,1))%for every spring in the current assembly state (compares component names)
        try
            dof1=para_node(n,1);
            dof2=para_node(n,2);
            %find line to couple spring
            l11=find(floor(ASET_sort(:,1)/10)==dof1);
            l22=find(floor(ASET_sort(:,1)/10)==dof2);
            l1=l11(1,1);
            l2=l22(1,1);

            for m=1:6%for 6D 
                zust1 = find(ASET_sort(:,1) == para_nodes(n,1)*10+m,1);
                zust2 = find(ASET_sort(:,1) == para_nodes(n,2)*10+m,1);
                
                k=para(n,m);
                b=para(n,18+m);
                d=para(n,24+m)*k;

                K_Ges(zust1-1,zust1-1)=K_Ges(zust1-1,zust1-1)+k;
                K_Ges(zust1-1,zust1-1)=K_Ges(zust1-1,zust1-1)+k;
                K_Ges(zust1-1,zust1-1)=K_Ges(zust1-1,zust1-1)-k;
                K_Ges(zust1-1,zust1-1)=K_Ges(zust1-1,zust1-1)-k;

                D_Ges(zust1-1,zust1-1)=D_Ges(zust1-1,zust1-1)+d;
                D_Ges(zust1-1,zust1-1)=D_Ges(zust1-1,zust1-1)+d;
                D_Ges(zust1-1,zust1-1)=D_Ges(zust1-1,zust1-1)-d;
                D_Ges(zust1-1,zust1-1)=D_Ges(zust1-1,zust1-1)-d;

                B_Ges(zust1-1,zust1-1)=B_Ges(zust1-1,zust1-1)+b;
                B_Ges(zust1-1,zust1-1)=B_Ges(zust1-1,zust1-1)+b;
                B_Ges(zust1-1,zust1-1)=B_Ges(zust1-1,zust1-1)-b;
                B_Ges(zust1-1,zust1-1)=B_Ges(zust1-1,zust1-1)-b;
                end

            for m=7:9%Cross Stiffnesses KGT %7=x to a / 8=y to b / 9=z to c 
            k=para(n,m);
            K_Ges(zust1-7,zust1-4)=K_Ges(zust1-7,zust1-4)+k;%see Oertli p.76 Gl. 103 
            K_Ges(zust1-4,zust1-7)=K_Ges(zust1-4,zust1-7)+k;
            K_Ges(zust1-7,zust1-4)=K_Ges(zust1-7,zust1-4)-k;
            K_Ges(zust1-4,zust1-7)=K_Ges(zust1-4,zust1-7)-k;
            K_Ges(zust1-7,zust1-4)=K_Ges(zust1-7,zust1-4)-k;
            K_Ges(zust1-4,zust1-7)=K_Ges(zust1-4,zust1-7)-k;
            K_Ges(zust1-7,zust1-4)=K_Ges(zust1-7,zust1-4)+k;
            K_Ges(zust1-4,zust1-7)=K_Ges(zust1-4,zust1-7)+k;
            end

            for m=10%Cross Stiffnesses KGT x to b
            k=para(n,m);
            K_Ges(l1,l1+4)=K_Ges(l1,l1+4)+k;
            K_Ges(l1+4,l1)=K_Ges(l1+4,l1)+k;
            K_Ges(l1,l2+4)=K_Ges(l1,l2+4)-k;
            K_Ges(l1+4,l2)=K_Ges(l1+4,l2)-k;
            K_Ges(l2,l1+4)=K_Ges(l2,l1+4)-k;
            K_Ges(l2+4,l1)=K_Ges(l2+4,l1)-k;
            K_Ges(l2,l2+4)=K_Ges(l2,l2+4)+k;
            K_Ges(l2+4,l2)=K_Ges(l2+4,l2)+k;
            end

            for m=11%Cross Stiffnesses KGT x to c
            k=para(n,m);
            K_Ges(l1,l1+5)=K_Ges(l1,l1+5)+k;
            K_Ges(l1+5,l1)=K_Ges(l1+5,l1)+k;
            K_Ges(l1,l2+5)=K_Ges(l1,l2+5)-k;
            K_Ges(l1+5,l2)=K_Ges(l1+5,l2)-k;
            K_Ges(l2,l1+5)=K_Ges(l2,l1+5)-k;
            K_Ges(l2+5,l1)=K_Ges(l2+5,l1)-k;
            K_Ges(l2,l2+5)=K_Ges(l2,l2+5)+k;
            K_Ges(l2+5,l2)=K_Ges(l2+5,l2)+k;
            end

            for m=12%Cross Stiffnesses KGT y to a
            k=para(n,m);
            K_Ges(l1+1,l1+3)=K_Ges(l1+1,l1+3)+k;
            K_Ges(l1+3,l1+1)=K_Ges(l1+3,l1+1)+k;
            K_Ges(l1+1,l2+3)=K_Ges(l1+1,l2+3)-k;
            K_Ges(l1+3,l2+1)=K_Ges(l1+3,l2+1)-k;
            K_Ges(l2+1,l1+3)=K_Ges(l2+1,l1+3)-k;
            K_Ges(l2+3,l1+1)=K_Ges(l2+3,l1+1)-k;
            K_Ges(l2+1,l2+3)=K_Ges(l2+1,l2+3)+k;
            K_Ges(l2+3,l2+1)=K_Ges(l2+3,l2+1)+k;
            end

            for m=13%Cross Stiffnesses KGT y to c
            k=para(n,m);
            K_Ges(l1+1,l1+5)=K_Ges(l1+1,l1+5)+k;
            K_Ges(l1+5,l1+1)=K_Ges(l1+5,l1+1)+k;
            K_Ges(l1+1,l2+5)=K_Ges(l1+1,l2+5)-k;
            K_Ges(l1+5,l2+1)=K_Ges(l1+5,l2+1)-k;
            K_Ges(l2+1,l1+5)=K_Ges(l2+1,l1+5)-k;
            K_Ges(l2+5,l1+1)=K_Ges(l2+5,l1+1)-k;
            K_Ges(l2+1,l2+5)=K_Ges(l2+1,l2+5)+k;
            K_Ges(l2+5,l2+1)=K_Ges(l2+5,l2+1)+k;
            end

            for m=14%Cross Stiffnesses KGT z to a
            k=para(n,m);
            K_Ges(l1+2,l1+3)=K_Ges(l1+2,l1+3)+k;
            K_Ges(l1+3,l1+2)=K_Ges(l1+3,l1+2)+k;
            K_Ges(l1+2,l2+3)=K_Ges(l1+2,l2+3)-k;
            K_Ges(l1+3,l2+2)=K_Ges(l1+3,l2+2)-k;
            K_Ges(l2+2,l1+3)=K_Ges(l2+2,l1+3)-k;
            K_Ges(l2+3,l1+2)=K_Ges(l2+3,l1+2)-k;
            K_Ges(l2+2,l2+3)=K_Ges(l2+2,l2+3)+k;
            K_Ges(l2+3,l2+2)=K_Ges(l2+3,l2+2)+k;
            end

            for m=15%Cross Stiffnesses KGT z to b
            k=para(n,m);
            K_Ges(l1+2,l1+4)=K_Ges(l1+2,l1+4)+k;
            K_Ges(l1+4,l1+2)=K_Ges(l1+4,l1+2)+k;
            K_Ges(l1+2,l2+4)=K_Ges(l1+2,l2+4)-k;
            K_Ges(l1+4,l2+2)=K_Ges(l1+4,l2+2)-k;
            K_Ges(l2+2,l1+4)=K_Ges(l2+2,l1+4)-k;
            K_Ges(l2+4,l1+2)=K_Ges(l2+4,l1+2)-k;
            K_Ges(l2+2,l2+4)=K_Ges(l2+2,l2+4)+k;
            K_Ges(l2+4,l2+2)=K_Ges(l2+4,l2+2)+k;
            end

            for m=16%Cross Stiffnesses KGT a to b
            k=para(n,m);
            K_Ges(l1+3,l1+4)=K_Ges(l1+3,l1+4)+k;
            K_Ges(l1+4,l1+3)=K_Ges(l1+4,l1+3)+k;
            K_Ges(l1+3,l2+4)=K_Ges(l1+3,l2+4)-k;
            K_Ges(l1+4,l2+3)=K_Ges(l1+4,l2+3)-k;
            K_Ges(l2+3,l1+4)=K_Ges(l2+3,l1+4)-k;
            K_Ges(l2+4,l1+3)=K_Ges(l2+4,l1+3)-k;
            K_Ges(l2+3,l2+4)=K_Ges(l2+3,l2+4)+k;
            K_Ges(l2+4,l2+3)=K_Ges(l2+4,l2+3)+k;
            end

            for m=17%Cross Stiffnesses KGT a to c
            k=para(n,m);
            K_Ges(l1+3,l1+5)=K_Ges(l1+3,l1+5)+k;
            K_Ges(l1+5,l1+3)=K_Ges(l1+5,l1+3)+k;
            K_Ges(l1+3,l2+5)=K_Ges(l1+3,l2+5)-k;
            K_Ges(l1+5,l2+3)=K_Ges(l1+5,l2+3)-k;
            K_Ges(l2+3,l1+5)=K_Ges(l2+3,l1+5)-k;
            K_Ges(l2+5,l1+3)=K_Ges(l2+5,l1+3)-k;
            K_Ges(l2+3,l2+5)=K_Ges(l2+3,l2+5)+k;
            K_Ges(l2+5,l2+3)=K_Ges(l2+5,l2+3)+k;
            end

            for m=18%Cross Stiffnesses KGT b to c
            k=para(n,m);
            K_Ges(l1+4,l1+5)=K_Ges(l1+4,l1+5)+k;
            K_Ges(l1+5,l1+4)=K_Ges(l1+5,l1+4)+k;
            K_Ges(l1+4,l2+5)=K_Ges(l1+4,l2+5)-k;
            K_Ges(l1+5,l2+4)=K_Ges(l1+5,l2+4)-k;
            K_Ges(l2+4,l1+5)=K_Ges(l2+4,l1+5)-k;
            K_Ges(l2+5,l1+4)=K_Ges(l2+5,l1+4)-k;
            K_Ges(l2+4,l2+5)=K_Ges(l2+4,l2+5)+k;
            K_Ges(l2+5,l2+4)=K_Ges(l2+5,l2+4)+k;
            end
        catch
            if printWarning
                errormsg = sprintf('unable to couple node %i and node %i \nskipping nodes for %s\n',dof1,dof2,para_name{n});
                fprintf(errormsg);
            end
        end
    elseif ~isempty(find(strcmp(para_name(n),'AE'))) & find(ismember(komp,'Bett'),1)%for every spring in the current assembly state
        dof1=para_node(n,1);
        %find line to couple spring
        l11=find(floor(ASET_sort/10)==dof1);
        l1=l11(1,1);
        for m=1:6%for 6D 
            k=para(n,m);
            b=para(n,18+m);
            d=para(n,24+m)*k;
            K_Ges(zust1-1,zust1-1)=K_Ges(zust1-1,zust1-1)+k;
            D_Ges(zust1-1,zust1-1)=D_Ges(zust1-1,zust1-1)+d;    
            B_Ges(zust1-1,zust1-1)=B_Ges(zust1-1,zust1-1)+b;
        end


    elseif ~isempty(find(strcmp(para_name(n),'Motorbremse')))
        try
            % es wird nur einf Fhg gekoppelt
            m = find(para(n,:),1); %freiheitsgrad finden, der gesperrt werden soll
            fitMB = false;
            if (m == 4 & find(strcmp(komp,'xSchlitten'))) | ...
               (m == 5 & find(strcmp(komp,'ySchlitten'))) | ...
               (m == 6 & find(strcmp(komp,'zSchlitten')))
                fitMB = true; %nur wenn die Entsprechende Komponente montiert wird, muss die Motorbremse gefkoppelt werden
            end
            if ~isempty(m) & fitMB
                dof1=para_node(n,1);
                dof2=para_node(n,2);
                l11=find(floor(ASET_sort)==dof1*10+m);
                l22=find(floor(ASET_sort)==dof2*10+m);
                l1=l11(1,1);
                l2=l22(1,1);

                k=para(n,m);

                K_Ges(l1,l1)=K_Ges(l1,l1)+k;
                K_Ges(l2,l2)=K_Ges(l2,l2)+k;
                K_Ges(l1,l2)=K_Ges(l1,l2)-k;
                K_Ges(l2,l1)=K_Ges(l2,l1)-k;
            end
        catch
            if printWarning
                errormsg = sprintf('unable to couple node %i and node %i \nskipping nodes for %s \n',dof1,dof2,para_name{n});
                fprintf(errormsg);
            end
        end
    end
end

if nargout >= 4 % wenn kein D_gesEq ausgegeben werden soll, diese Berechung �berspringen um Zeit zu sparen
    [EV0,EW0] = eig(K_Ges,M_Ges);
%     EV = EV0 * sqrt(inv(diag(diag(EV0'*M_Ges*EV0)))); %Massen normieren
    EV=EV0*sqrt(diag(1./diag(EV0'*M_Ges*EV0))); % speed-up durch vermeiden des invertierens
    wGes = diag(sqrt(abs(EW0)));

    % ein mal modaltransformieren -> Koppeln -> r�cktransformieren
    D01 = EV'*D_Ges*EV*diag(1./wGes); %hysteretisch
    B01 = EV'*B_Ges*EV;               %viskos
    D_GesEq = M_Ges'*EV*(B01+D01)*EV'*M_Ges; %ersatzd�mpfung
end

end

%----- F_Koppelung END