function  [fi_sort_norm,w2_sort_norm]=eigs_sort_norm(K,M,zenbat)
%‹bernommen von Garitaonandia (University of the BasqueCountry UPV/EHU, Bilbao)
[fi_unsort_nonorm, ~]=eigs(K,M,zenbat,'sm');

mr=fi_unsort_nonorm'*M*fi_unsort_nonorm;
kr=fi_unsort_nonorm'*K*fi_unsort_nonorm;

diagonal_mr=diag(mr);
mr=diag(diagonal_mr);

inversa=inv(mr);
raiz_inversa=sqrt(inversa);
w2_unsort_norm=inversa*kr;
fi_unsort_norm=fi_unsort_nonorm*raiz_inversa;

frek2_unsort_norm=diag(w2_unsort_norm);
[frek2_sort_norm,orden]=sort(frek2_unsort_norm);

w2_sort_norm=diag(frek2_sort_norm);
fi_sort_norm=fi_unsort_norm(:,orden);
end


