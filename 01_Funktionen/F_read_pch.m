% Read Nastran pch
% Reads Boundary Stiffness and Mass Matrixes KAAX and MAAX
% for Craig-Bampton-Red

function [ASET0, KAAX0, MAAX0, BAAX0, DAAX0, nodes]=F_read_pch(path_in)
%% PCH laden und formatiern
h=fopen(path_in);
block_size = 100000000;
while ~feof(h)
    format='%s';
    text = textscan(h,format,block_size,'delimiter','\n');  %Input
end
fclose(h);

%% Grid finden

%f�r MD Nastran
grid_nr=find(strncmpi(text{1,1}(:,1),'GRID',4)); 
grid_nodes=text{1,1}(grid_nr,:);
grid_nodes=cell2mat(grid_nodes);
%Preallocation
nodes=zeros(size(grid_nodes,1),4);
for aa=1:size(grid_nodes,1)
    i1=strfind(grid_nodes(aa,:),'  ');
    i2=find(diff(i1)>1);
    nodes(aa,1) = sscanf(grid_nodes(aa,i1(i2):i1(i2+1)), '%f*');
    nodes(aa,2) = sscanf(grid_nodes(aa,25:32), '%f*');
    nodes(aa,3) = sscanf(grid_nodes(aa,33:40), '%f*');
    nodes(aa,4) = sscanf(grid_nodes(aa,41:48), '%f*');
end

% f�r NX Nastran
% grid_nr=find(strncmpi(text{1,1}(:,1),'GRID*',5)); 
% grid_nodes=text{1,1}(grid_nr,:);
% grid_nodes=cell2mat(grid_nodes);
% 
% grid_nodes2=text{1,1}(grid_nr+1,:); 
% grid_nodes2=cell2mat(grid_nodes2); 
% %Preallocation
% nodes=zeros(size(grid_nodes,1),4);
% for aa=1:size(grid_nodes,1)
%     i1=strfind(grid_nodes(aa,:),'  ');
%     i2=find(diff(i1)>1);
%     nodes(aa,1) = sscanf(grid_nodes(aa,i1(i2):i1(i2+1)), '%f*');
%     nodes(aa,2) = sscanf(grid_nodes(aa,end-31:end-16), '%f*');
%     nodes(aa,3) = sscanf(grid_nodes(aa,end-15:end), '%f*');
%     nodes(aa,4) = sscanf(grid_nodes2(aa,2:25), '%f*');
% end

%f�r beide
ASET0=[];
[KAAX0, ASET0]=F_read_mat_pch('DMIG*   KAAX');
BAAX0=F_read_mat_pch('DMIG*   BAAX');
DAAX0=F_read_mat_pch('DMIG*   K4AAX');
MAAX0=F_read_mat_pch('DMIG*   MAAX');

%falls MD nastran (D Potzen entspircht Faktor 1000 bis auf normal modes terme)
% con=find(ASET0(:,2))%constrains
% KAAX0=KAAX0./1000;
% KAAX0(length(con)+1:end,length(con)+1:end)=KAAX0(length(con)+1:end,length(con)+1:end).*1000;
% BAAX0=BAAX0./1000;
% BAAX0(length(con)+1:end,length(con)+1:end)=BAAX0(length(con)+1:end,length(con)+1:end).*1000;
% DAAX0=DAAX0./1000;
% DAAX0(length(con)+1:end,length(con)+1:end)=DAAX0(length(con)+1:end,length(con)+1:end).*1000;
% MAAX0=MAAX0./1000;
% MAAX0(length(con)+1:end,length(con)+1:end)=MAAX0(length(con)+1:end,length(con)+1:end).*1000;

%ZeroMasses l�schen bzw mit sehr kleinen Eintr�gen f�llen -> sonst singul�re Matrix
zeroMass=find(any(MAAX0)==0);
if ~isempty(zeroMass)
    MAAX0(zeroMass, zeroMass)=1e-10*eye(length(zeroMass));
    warning('Zeromass detected');
end

%Normalmodes aus ASET werfen
ASET0(ASET0(:,2)==0,:)=[];

function [mat_ges, aset]=F_read_mat_pch(inputString)
        dmig=find(strncmpi(text{1,1}(:,1),'DMIG*  ',7));
        %DMIG*   KAAX finden
        kaax1_h=find(strncmpi(text{1,1}(:,1),inputString,12));
        
        %Bis zur ersten anderen Matrix auslesen
        [~,ende_ind]=ismember(kaax1_h, dmig);
        kaax1=dmig(ende_ind);
        
        if isempty(ASET0)
            kaax2=text{1,1}(kaax1,1);
            kaax3=cell2mat(kaax2);
            
            kaax51=sscanf(kaax3(:,30:end)','%f',[2 size(kaax3,1)]);
            kaax5=kaax51';
            
            aset=kaax5;
            aset(:,3)=kaax5(:,1)*10+kaax5(:,2);
            ASET0=aset;
        end
        size_mat=size(ASET0,1);
        
        if isempty(kaax1)
            mat_ges=zeros(size_mat);
            return
        end
       
        
        %Anfang n�chster Matrix hinten anf�gen
        kaax1(end+1)=dmig(ende_ind(end)+1)-1;
        
        %Dreiecksmatrix auslesen
        ind_column=kaax1;
        ind_row=kaax1+1;
        mat_o=zeros(size_mat);
        
        for ii=1:length(ind_row)-1
            %Spalte suchen
            mat_spalte=text{1,1}(ind_column(ii),1);
            mat_sp=cell2mat(mat_spalte);
            
            sp_ind=sscanf(mat_sp(:,30:end)','%f',[2 size(mat_sp,1)]);
            spalten_index=sp_ind';
            
            sp_ind=spalten_index(:,1)*10+spalten_index(:,2);
            [~,ind_sp]=ismember(sp_ind,ASET0(:,3));
            
            %Spalte auslesen
            mat1=text{1,1}(ind_row(ii):ind_row(ii+1)-2,1);
            mat1 = strrep(mat1,'D','E');%for MD Nastran only (convert D to E)
            mat2=cell2mat(mat1);
            
            mat33=sscanf(mat2(:,6:end)','%f',[3 size(mat2,1)]);
            mat4=mat33';
            node_index1=mat4(:,1:2);
            mat3=mat4(:,3);
            
            node_index=node_index1(:,1)*10+node_index1(:,2);
            
            [~,ind_n]=ismember(node_index, ASET0(:,3));
            
            %Nullzeilen l�schen ???
            mat3(ind_n==0)=[];
            ind_n(ind_n==0)=[];
            
            %Spalte assemblieren
            mat_o(ind_n,ind_sp)=mat3;
        end
        %Dreiecksmatrix spiegeln
        mat_ges=mat_o+tril(mat_o',-1);
    end
end