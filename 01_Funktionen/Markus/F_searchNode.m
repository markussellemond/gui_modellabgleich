function [] = F_searchNode(dataObj,nodeNr)



    Nodes = dataObj.simulation.nodesGes;
    components = dataObj.simulation.nodesKomp;
    
if ismember(nodeNr,Nodes)        
    loc = Nodes(:,5) ~= 0;
    Nodes = Nodes(loc,:);

    fig = figure;
    nodeNameChkBx = uicontrol(fig,'Style','checkbox',...
                    'Units','normalized',...
                    'Position',[0.25,0.05,0.04,0.04],...
                    'Value',1,...
                    'Callback',@CB_nodeNameChkBx);                
    nodeNameStr=uicontrol(fig,'Style','text',...
                    'String','Node Names:',...
                    'Units','normalized',...
                    'HorizontalAlignment','left',...
                    'Position',[0.1,0.04,0.14,0.0476]);
    nodeNumberChkBx = uicontrol(fig,'Style','checkbox',...
                    'Units','normalized',...
                    'Position',[0.25,0.01,0.04,0.04],...
                    'Value',0,...
                    'Callback',@CB_nodeNumberChkBx);                
    nodeNumberStr=uicontrol(fig,'Style','text',...
                    'String','Node Numbers:',...
                    'Units','normalized',...
                    'HorizontalAlignment','left',...
                    'Value',0,...
                    'Position',[0.1,0.0,0.14,0.0476]);
                
    
    %% Knoten der einzelnen Komponenten Ermitteln
    compLoc = cell(1,1);
    for n = 1:length(components)
        compLoc{n} = ismember(Nodes(:,1),components(n).nodes(:,1));
    end
    
    %% plotten             
    ax1 = gca;
    
    compColor = [0 0 0;
                 1 0 0;
                 0 1 0;
                 0 0 1;
                 1 0 1;
                 0 1 1];
    for n = 1:length(compLoc)   
        %statischer Plot
        compPlot(n) = plot3(Nodes(compLoc{n},2),Nodes(compLoc{n},3),Nodes(compLoc{n},4),...
                        'LineStyle','none','Marker','o','Color',compColor(n,:));
        set(ax1,'NextPlot','add');
    end
    
    %% Knotennummern und Knotenindices plotten
    tNumbers = text(Nodes(:,2),Nodes(:,3),Nodes(:,4),num2str((1:size(Nodes,1))'),'Visible','off'); %Knotennummern wie in Matlab
    tNames = [];
    for n = 1:length(components)
        tNames = [tNames;...
                  text(Nodes(compLoc{n},2),Nodes(compLoc{n},3),Nodes(compLoc{n},4),...
                       num2str(Nodes(compLoc{n},1)),'Color',compColor(n,:),'Visible','off')];   %Knotennamen aus LMS 
    end
    
    %% Kosy plotten
    plot3([0,500],[0,0],[0,0],'g-')
    plot3([0,0],[0,500],[0,0],'b-')
    plot3([0,0],[0,0],[0,500],'r-')
    
    
    xMin = min(Nodes(:,2))-500;
    xMax = max(Nodes(:,2))+500;
    
    yMin = min(Nodes(:,3))-500;
    yMax = max(Nodes(:,3))+500;
    
    zMin = min(Nodes(:,4))-500;
    zMax = max(Nodes(:,4))+500;
    
    axis equal
    set(ax1,'XLim',[xMin,xMax],'YLim',[yMin,yMax],'ZLim',[zMin,zMax])
%     set(ax1.Title,'String',sprintf('Mode No.: %.i, Frequenz: %.3f',...
%         modeN,EVsim{modeN}.freq),...
%         'Interpreter','latex');

    %% Knoten Suchen und Plotten
    nodeIdx = [];
    for n = 1:length(components)
        nodeIdx = find(components(n).nodes(:,1) == nodeNr,1,'first');
        if ~isempty(nodeIdx)
            compID = components(n).ID;
            if ~isempty(F_findMat(dataObj.para.paraNode,nodeNr))
                [idx2,idx1] = F_findMat(dataObj.para.paraNode,nodeNr);
                compID = [compID,' : ',dataObj.para.paraName{idx1}];
            end
            nodeCoord = components(n).nodes(nodeIdx,2:4);
        end
    end
    
    plot3(nodeCoord(1),nodeCoord(2),nodeCoord(3),'ro','MarkerSize',20,'LineWidth',2)
    text(nodeCoord(1),nodeCoord(2),nodeCoord(3),sprintf('%s : %i',compID,nodeNr),'FontSize',12,'FontWeight','bold')
    
else
    msgbox('Gesuchter Knoten konnte nicht gefunden werden!')
end
    
%% Callbacks

    function CB_nodeNameChkBx(scr,event)
        printNodeNames = get(scr,'Value');
        set(nodeNumberChkBx,'Value',0);
        for m = 1:length(tNames)
            if printNodeNames
                set(tNames(m),'Visible','on')
                set(tNumbers(m),'Visible','off')
            else
                set(tNames(m),'Visible','off')
            end 
        end
    end

    function [] = CB_nodeNumberChkBx(scr,event)
        printNodeNumbers = get(scr,'Value');
        set(nodeNameChkBx,'Value',0);                
        for m = 1:length(tNumbers)
            if printNodeNumbers
                set(tNumbers(m),'Visible','on')
                set(tNames(m),'Visible','off')
            else
                set(tNumbers(m),'Visible','off')
            end 
        end
    end

    function CB_statMdlChkBx(scr,event)
        showStatMdl = get(scr,'Value');
        if showStatMdl
            for m = 1:length(compPlot)
                set(compPlot(m),'Visible','on')
            end
        else
            for m = 1:length(compPlot)
                set(compPlot(m),'Visible','off')
            end
        end
    end

end

