function [] = F_autoMAC(EV,nodes)
%% MAC matrix berechnen
    livingNodes = logical(nodes(:,5)); % "dead nodes" sind knoten, zu denen keine vollst�ndigen mess bzw simulationsdaten f�r alle Raumrichtungen vorliegen
    m = 1;
    while m <= length(EV)
        if ~isempty(strfind(EV{m}.ID,'Residual'))    % residual modes entfernen
            EV(m) = [];
            m = m-1; % da das element gel�scht wird, muss der aufr�ckende Eintrag an derselben stelle nochmal untersucht werden
        end
        m = m+1;
    end
    
    MACmat = zeros(length(EV),length(EV));
    for m = 1:length(EV)
        f1{m} = sprintf('%3.1f',EV{m}.freq);
        EV1 = [EV{m}.Nodes(livingNodes,2);...
               EV{m}.Nodes(livingNodes,3);...
               EV{m}.Nodes(livingNodes,4)];
        for n = 1:length(EV)
            f2{n} = sprintf('%3.1f',EV{n}.freq);
            EV2 = [EV{n}.Nodes(livingNodes,2);...
                   EV{n}.Nodes(livingNodes,3);...
                   EV{n}.Nodes(livingNodes,4)];
            MACmat(m,n) = F_calculateMAC(EV1,EV2);
        end
    end
    
    MACmat(end+1,:) = 0;
    MACmat(:,end+1) = 0; % f�r darstellungszwecke mittels pcolor muss noch eine Zeile und spalte eingef�gt werden

    
%% figure erzeugen
    fontSize = 12;
    
    fig = figure;    
    pcolor(MACmat)
    colormap(1-gray)
    caxis([0.3,1]);
    ax = gca;
    set(ax,'TickLabelInterpreter','Latex','FontSize',10)
%     set(ax.Title,'String','MAC-Matrix','Interpreter','Latex')
    set(ax,'XTick',(1:size(MACmat,2))+0.5)
    set(ax.XLabel,'String','Frequenz in Hz','FontSize',fontSize,'Interpreter','Latex')
    set(ax,'XTickLabel',f1)
    set(ax,'XTickLabelRotation',90)
    
    set(ax,'YTick',(1:size(MACmat,1))+0.5)
    set(ax,'YTickLabel',f2)
    set(ax.YLabel,'String','Frequenz in Hz','FontSize',fontSize,'Interpreter','Latex')
    
    set(fig,'WindowStyle','normal');
    set(fig,'PaperSize', fig.Position(3:4));
    for m = 1:size(MACmat,1)-1
        for n = 1:size(MACmat,2)-1
            if MACmat(m,n) > 0.65
                col = [1 1 1];
            else
                col = [0 0 0];
            end
            if MACmat(m,n) >= 0.3
                t = text(n+0.2,m+0.5,sprintf('%.2f',MACmat(m,n)),'Interpreter','Latex','FontSize',8,'Color',col);
            end
        end
    end


end

