function [] = GUI_defineParaPrototype(guiHandle,dataObj)

GUI.fig = figure;
set(GUI.fig,...
        'Units','centimeters',...
        'Position',[10,10,30,18],...
        'Name', 'Parameter f�r Fit ausw�hlen',...
        'MenuBar','none',...
        'NumberTitle','off',...
        'CloseRequestFcn',@CB_figCloseRequest)

GUI.UITpara = uitable;
set(GUI.UITpara,...
        'Parent',GUI.fig,...
        'Units','centimeters',...
        'ColumnWidth','auto',...
        'ColumnEditable',false,...
        'Data',{},...
        'Position',[0,3,30,15],...
        'CellSelectionCallback',@CB_cellSelection);
    
GUI.ThowTo = uicontrol;
set(GUI.ThowTo,...
        'Style','Text',...
        'Parent',GUI.fig,...
        'Units','centimeters',...
        'Position',[0.5,0.5,29,0.5],...
        'String','Bitte jene Parameter ausw�hlen, die in der Optimierungsschleife angepasst werden sollen!')

% Zeilennamen ermitteln
rowName = cell(length(dataObj.fit.paraGroupsCurrent.groups),1);
for n = 1:length(dataObj.fit.paraGroupsCurrent.groups)
    rowName{n,1} = [dataObj.fit.paraGroupsCurrent.groups(n).ID];
end

% Zeilen und Spaltennamen in Tabelle schreiben
set(GUI.UITpara,'ColumnName',dataObj.para.titleBar(6:end),...
                'RowName',rowName);
 
if isempty(dataObj.fit.paraGroupsCurrent.groupsPrototype)   % groupsprototype dient der Speicherung der Daten f�r die anzeige in der Listbox
    dataObj.fit.paraGroupsCurrent.groupsPrototype = zeros(length(rowName),length(dataObj.para.titleBar(6:end)));
end

set(GUI.UITpara,'Data',cell(length(rowName),length(dataObj.para.titleBar(6:end))));

for m = 1:length(rowName) % listbox mit alten daten f�llen
    for n = 1:length(dataObj.para.titleBar(6:end))
        if dataObj.fit.paraGroupsCurrent.groupsPrototype(m,n) ~= 0
            GUI.UITpara.Data{m,n} = 'fit';
        end
    end
end


uiwait(guiHandle.fig)

%% Callbacks 
    function CB_cellSelection(scr,eventData)
        if ~isempty(eventData.Indices)
            if isempty(GUI.UITpara.Data{eventData.Indices(1),eventData.Indices(2)}) % wenn noch nitx drinne steht
                GUI.UITpara.Data{eventData.Indices(1),eventData.Indices(2)} = 'fit';
            else    % wenn schon hinzugef�gt wurde, dann entfernen
                GUI.UITpara.Data{eventData.Indices(1),eventData.Indices(2)} = '';
            end
        end
    end
    
    function CB_figCloseRequest(~,~)
        try
            F_generatePrototype();
        catch
            msgbox('Paraprototyp konnte nicht generiert werden')
        end
        delete(GUI.fig)
        uiresume(guiHandle.fig)
    end

%% ander Funktionen
    function F_generatePrototype()
        dataObj.fit.paraOpt = [];
        dataObj.fit.paraMin = [];
        dataObj.fit.paraMax = [];
        nParameter = 1; %Fortlaufende Nummer f�r kennzeichnung im ParaPrototype und im Vektor der Optimierungsvariablen
        paraPrototype = zeros(size(dataObj.para.para));
        groupsPrototype = zeros(length(dataObj.fit.paraGroupsCurrent.groups),size(dataObj.fit.paraGroupsCurrent.groupsPrototype,2));
        for m = 1:size(GUI.UITpara.Data,1)  %Schleife �ber Komponentengruppen
            for n = 1:size(GUI.UITpara.Data,2)  %Schleife �ber Parameter
                if ~isempty(GUI.UITpara.Data{m,n})
                    groupsPrototype(m,n) = nParameter;
                    paraPrototype(dataObj.fit.paraGroupsCurrent.groups(m).memberIdx,n) = nParameter;
                    
                    % parameterwerte in Optimierungsveriable schreiben um Grenzen zu definieren
                    dataObj.fit.paraOpt(nParameter,1) = ...
                        dataObj.para.para(dataObj.fit.paraGroupsCurrent.groups(m).memberIdx(1),n);
                    dataObj.fit.paraMin(nParameter,1) = dataObj.fit.paraOpt(nParameter)/5;
                    dataObj.fit.paraMax(nParameter,1) = dataObj.fit.paraOpt(nParameter)*5;
                    
                    % Z�hler Updaten
                    nParameter = nParameter+1;
                end
            end
        end
        dataObj.fit.paraPrototypeCurrent = paraPrototype;
        dataObj.fit.paraGroupsCurrent.groupsPrototype = groupsPrototype;
    end
    
end