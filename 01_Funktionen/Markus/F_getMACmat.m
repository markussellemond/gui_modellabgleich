function [MACmat] = F_getMACmat(EVmeas,EVsim,match)
    MACmat = zeros(length(EVmeas),length(EVsim));
    for m = 1:length(EVmeas)
        for s = 1:length(EVsim)
            simEV = EVsim{s}.Nodes(match(:,2),1:3);
            measEV = EVmeas{m}.Nodes(match(:,1),2:4);
            MACmat(m,s) = F_calculateMAC([simEV(:,1);simEV(:,2);simEV(:,3)],...
                                        [measEV(:,1);measEV(:,2);measEV(:,3)]);
        end
    end
end

