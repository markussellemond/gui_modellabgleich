function [Hges] = F_bode(EV,MMod,KMod,DMod,input,output,omega)
    ddir = sign(input).*sign(output); %wenn Anschlagrichtung und Wessrichtung 
                    % entgegengesetzt sind, muss das Vorzeichen gewechselt werden
                    % Kodierung erfolgt �ber das Vorzeichen von Eingang und
                    % Ausgang
    input = abs(input);
    output = abs(output);
    
    Hges = zeros(length(output),length(input),size(omega,1));
    H = zeros(size(omega));
    for idxOut = 1:length(output)
        for idxIn = 1:length(input)
            H = 0*H;
            for m = 1:length(MMod)
                H  = H + ddir(idxOut)* EV(input(idxIn),m)*EV(output(idxOut),m)./(-MMod(m,m)*omega.^2 + 1i*DMod(m,m)*omega + KMod(m,m));
            end
            Hges(idxOut,idxIn,:) = H;
        end
    end
%     if length(input) == 1 && length(output) == 1
%         Hges = squeeze(Hges);
%     end
end

