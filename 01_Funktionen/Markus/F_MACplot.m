function [fig] = F_MACplot(EVmeas,EVsim,match)
    fontSize = 12;
    
    %% MAC-Matrix berechnen
    MACmat = zeros(length(EVsim),length(EVmeas));
    for m = 1:length(EVsim)
        fSim{m} = sprintf('%3.1f',EVsim{m}.freq);
        simEV = [EVsim{m}.Nodes(match(:,2),1);...
                 EVsim{m}.Nodes(match(:,2),2);...
                 EVsim{m}.Nodes(match(:,2),3)];
        for n = 1:length(EVmeas)
            fMeas{n} = sprintf('%3.1f',EVmeas{n}.freq);
            measEV = [EVmeas{n}.Nodes(match(:,1),2);...
                      EVmeas{n}.Nodes(match(:,1),3);...
                      EVmeas{n}.Nodes(match(:,1),4)];
            MACmat(m,n) = F_calculateMAC(simEV,measEV);
        end
    end
    
    MACmat(end+1,:) = 0;    %f�r die Darstellung eine leerzeile und Spalte einf�gen
    MACmat(:,end+1) = 0;
    
    %% Plotten
    fig = figure;    
    pcolor(MACmat);
    colormap(1-gray);
    caxis([0.3,1]);
    
    ax = gca;
    set(ax,'TickLabelInterpreter','Latex','FontSize',10)
    set(ax,'XTick',(1:size(MACmat,2))+0.5)
    set(ax.XLabel,'String','Frequenz aus Messung in Hz','FontSize',fontSize,'Interpreter','Latex')
    set(ax,'XTickLabel',fMeas)
    set(ax,'XTickLabelRotation',90)
    
    set(ax,'YTick',(1:size(MACmat,1))+0.5)
    set(ax,'YTickLabel',fSim)
    set(ax.YLabel,'String','Frequenz aus Simulation in Hz','FontSize',fontSize,'Interpreter','Latex')
    
    set(fig,'WindowStyle','normal');
    set(fig,'PaperSize', fig.Position(3:4));
    for m = 1:size(MACmat,1)-1
        for n = 1:size(MACmat,2)-1
            if MACmat(m,n) > 0.65
                col = [1 1 1];
            else
                col = [0 0 0];
            end
            if MACmat(m,n) > 0.3
                t = text(n+0.2,m+0.5,sprintf('%.2f',MACmat(m,n)),'Interpreter','Latex','FontSize',8,'Color',col);
            end
        end
    end

end

