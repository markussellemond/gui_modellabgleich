function [nodes_ges,A,M_GesUngekoppelt,K_GesUngekoppelt,D_GesUngekoppelt,B_GesUngekoppelt,ASET,nodesKomp,normal_modes] = ...
            F_loadFEM(path,file_komp,komp)
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here
ASET.GES=[];
nodes_ges=[];

for i=1:length(komp) 
    [ASET0, KAAX0, MAAX0, BAAX0, DAAX0, nodes]=F_read_pch([path{1,1} file_komp{i} '.pch']); %�nderung Markus 20190628: der Unterordner \PCH muss jetzt mit der variable path mit �bergeben werden
    ASET.(sprintf(komp{i}))=ASET0;
    KAAX.(sprintf(komp{i}))=KAAX0;
    MAAX.(sprintf(komp{i}))=MAAX0;
    DAAX.(sprintf(komp{i}))=DAAX0;
    BAAX.(sprintf(komp{i}))=BAAX0;
    ASET.GES=[ASET.GES; ASET.(sprintf(komp{i}))(:,3)];
    nodesKomp(i).ID = sprintf(komp{i});
    nodesKomp(i).nodes = nodes;
    nodes_ges=[nodes_ges;nodes];
end

A=unique(sort(ASET.GES));               % A enth�lt den inhalt von ASET.Ges in sortierter reihenfolge. in dieser Reihenfolge befinden sich die Freiheitsgrade im ZUstandsvektor
ASET.GES(:,3)=ASET.GES(:,1);%f�r auswertungszwecke
con=length(A);
normal_modes.ges=0;
for i=1:length(komp)
    KAAX.(sprintf(komp{i}))=F_Koppelform(KAAX.(sprintf(komp{i})),A,ASET.(sprintf(komp{i})));
    MAAX.(sprintf(komp{i}))=F_Koppelform(MAAX.(sprintf(komp{i})),A,ASET.(sprintf(komp{i})));
    DAAX.(sprintf(komp{i}))=F_Koppelform(DAAX.(sprintf(komp{i})),A,ASET.(sprintf(komp{i})));
    BAAX.(sprintf(komp{i}))=F_Koppelform(BAAX.(sprintf(komp{i})),A,ASET.(sprintf(komp{i})));

    normal_modes.(sprintf(komp{i}))=length(KAAX.(sprintf(komp{i})))-con;
    normal_modes.ges=normal_modes.ges+normal_modes.(sprintf(komp{i}));
end

[M_GesUngekoppelt]=F_Koppelung(con,normal_modes,komp,MAAX);%M_Ges=CB-Darstellung nicht normiert!
[K_GesUngekoppelt]=F_Koppelung(con,normal_modes,komp,KAAX);
[D_GesUngekoppelt]=F_Koppelung(con,normal_modes,komp,DAAX); %hysteretisch
[B_GesUngekoppelt]=F_Koppelung(con,normal_modes,komp,BAAX); %viskos


% Zust�nde zu den Knoten finden
nod1 = nodes_ges(:,1)*10+1;
nod2 = nodes_ges(:,1)*10+2;
nod3 = nodes_ges(:,1)*10+3;

[~,m1] = ismember(nod1,A);
[~,m2] = ismember(nod2,A);
[~,m3] = ismember(nod3,A);

m1(m1==0 | m2==0 | m3==0) = 0;
m2(m1==0 | m2==0 | m3==0) = 0;
m3(m1==0 | m2==0 | m3==0) = 0;

nodes_ges(:,5:7) = [m1,m2,m3];

end

