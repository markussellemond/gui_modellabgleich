function [val] = F_objektivD3(dOpt,MDKungekoppelt,komp,ASET,...
    exitationNode,exitationDir,para,paraName,paraNode,paraPrototype,...
    matchFreq,freqRange,Hmeas,wMeas,EVmeas,match,options)
%% F_objektivK3
%INPUT:   =================================================================
%   dOpt: Steifigkeiten f�r die das G�tema� ermittelt werden soll
%   MDKungekoppelt: Struct mit den oeffizientenmatritzen der
%           Bewegungsgleichung
%   komp: cell array mit den namen der zu koppelnden Komponenten
%   ASET: vektor mit den Knotennamen
%   exitationNode: Zeile in match, Knoten der Angeregt werden soll
%   exitationDir: richtung in die Angeregt werden soll ('x','y','z'). Muss
%           als Cellarray {'x'} �bergeben werden. f�r length(exitationDir)
%           > 1 wird die Summe beider direct FRFs gebildet.
%   para: para array aus der xls Tabelle
%   paraName: namen zu den Parametern aus der Tabelle
%   paraPrototype: kennzeichnet die Werte in para, die durch kOpt ersetzt
%           werden sollen. f�r paraprototype ~= 0 kann dieser matrix der
%           index in kOpt f�r den entsprechenden wert entnommen werden.
%           size(paraPrototype) == size(para)
%   matchFreq: Frequenzen, deren Amplitude angepasst werden soll
%           wenn isempty(mF) = gemessene EF und �ber MAC gematchte simulierte
%           wenn size(mF) = (n,1): verwende gemessene EF und freq aus matchFreq
%           wenn size(mF) = (n,2): gemessene und simulierte Frequenzen aus mF
%   freqRange: Radius um matchFreq, um den herum ein Maximum gesucht wird
%   Hmeas: Gemessener komplexer Nachgiebigkeitsfrequenzgang
%   wMeas: zu Hmeas zugeh�riger Kreisfrequenzvektor
%   EVmeas: struct mit den Eigenvektoren der Messung
%   match: zuordnung von Knoten aus Messung und Simulation
%   options: optionen f�r die Berechnung des G�tema�es
%       options -> objectiveFkt: z.B. 'witt'
%       options -> useGrad: zus�tzlich Ableigung von val zur�ckgeben: true/false
%                           (nur experimentell)
%       options -> matchNmodesRange: untere und obere Grenze der zu
%                                    matchenden Moden
%       options -> matchMethod: methode mit denen die zu vergleichenden
%                     Frequenzen ermittelt werden
%                   -> 'MAC': vgl Amplitude der mit MAC gematchten EF 
%                   -> 'freq': vgl Amplitude der im matchFreq angegeben freq
%       options -> useRed: modal reduzieren um Zeit zu sparen (0: voll, n:
%                           erste n Moden). Im reduzierten Modell wird mit
%                           modalen Koordinaten und diagonalisierter
%                           D�mpfungsmatrix gerechnet, im nicht reduzierten
%                           wird mit einem nodalen ss gerechnet. F�r volles
%                           Modell, aber in modalen Koordinaten: useRed =
%                           length(MGes). modale Koordinaten
%                           Geschwindigkeitsreduktion um 90%!!!
%       options -> optimRange: vektor mit unterer und oberer Frequenz um
%                           den bereich einzugrenzen, in dem der Frequenzgang
%                           Optimiert werden soll (Fl�che unter FRF
%                           minimieren)
%OUTPUT:   ================================================================
%   val: G�tema�, berechnet auf basis der Eingaben. Die Methode zur
%           Berechnung von val kann dem options-Struct entnommen werden
    
    %% zu optimierende Parameter auf Basis von paraPrototype in para schreiben
    for m = 1:size(para,1)
        for n = 1:size(para,2)
            if paraPrototype(m,n) > 0 && paraPrototype(m,n) <= length(dOpt)
                para(m,n) = dOpt(paraPrototype(m,n));
            end
        end
    end
    
    %% koppeld�mpfungen in Koeffizientenmatritzen einbringen
    [K_Ges,~,~,D_GesEq,EV,EW]=F_Koppelung_para(MDKungekoppelt.M,MDKungekoppelt.K,...
        MDKungekoppelt.D,MDKungekoppelt.B,para,paraName,paraNode,komp,ASET);
    M_Ges = MDKungekoppelt.M;
    
    %% Eigenwerte und Eigenwvektoren bestimmen
    if strcmp(options.matchMethod,'MAC') || useRed
        if ~exist('EV')
            [EVges0,EWges]= eig(K_Ges,M_Ges);
        else
            EVges0 = EV;
            EWges = EW;
        end
%         EVges=EVges0*sqrt(inv(diag(diag(EVges0'*M_Ges*EVges0))));%massen-normalisieren
        EVges=EVges0*sqrt(diag(1./diag(EVges0'*M_Ges*EVges0))); % speed-up durch vermeiden des invertierens
        wGes = diag(sqrt(EWges));
        [wSort,I] = sort(wGes);
        EVgesSort = EVges(:,I);
        EWgesSort = EWges(I,I);
    end
    
    %% mit MAC eigen moden aus Simulation und Messung matchen
    wFitMeas = zeros(size(EVmeas))';
    MACmat = zeros(length(EVmeas),options.matchNmodesRange(2));
    
    for m = 1:length(EVmeas)
        wFitMeas(m,1) = EVmeas{m}.freq*2*pi;
        measEV = [EVmeas{m}.Nodes(match(:,1),2);...
                  EVmeas{m}.Nodes(match(:,1),3);...
                  EVmeas{m}.Nodes(match(:,1),4)];
        for n = options.matchNmodesRange(1):options.matchNmodesRange(2)
            simEV = [EVgesSort(match(:,4),n);...
                     EVgesSort(match(:,4)+1,n);...
                     EVgesSort(match(:,4)+2,n)];
            MACmat(m,n) = F_calculateMAC(measEV,simEV);
        end
    end
    
    MACmatch = zeros(length(EVmeas),1);
    MAC = zeros(length(EVmeas),1);
    for m = 1:length(EVmeas) % aus MACmat beste MAC werte suchen (es gibt keine doppelten zuordnungen)
        [idx2,idx1] = F_findMat(MACmat,max(max(MACmat)));
        MACmatch(idx1) = idx2;
        MAC(idx1) = MACmat(idx1,idx2);
        MACmat(idx1,:) = 0; % Zeile und spalte "l�schen"
        MACmat(:,idx2) = 0;
    end
    
    if options.printMACmatch
        fprintf('######### MAC match #########\n')
        for m = 1:length(EVmeas)
            fprintf('f_meas_%i: \t %7.2f \t f_sim_%i: \t %7.2f \t MAC:   %4.3f\n',m,EVmeas{m}.freq, MACmatch(m),wSort(MACmatch(m))/2/pi,MAC(m));
        end
    end
    
    %% zu vergleichende Frequenzen ermitteln
    if isempty(matchFreq)
        matchFreq = zeros(length(EVmeas),2);
        matchFreq(:,1) = wSort(MACmatch)/2/pi;  % erste Spalte Simulation
        for m = 1:length(EVmeas)
            matchFreq(m,2) = EVmeas{m}.freq;    % zweite Spalte Messung
        end
    elseif size(matchFreq,2) == 1
        matchFreq(:,2) = 0;
        for m = 1:length(EVmeas)
            matchFreq(m,2) = EVmeas{m}.freq;
        end
    end
        
    %% Systemantwort berechnen   
    invertDir = zeros(size(exitationDir));
    dir = zeros(size(invertDir));
    node = match(exitationNode,4);

    for n = 1:length(invertDir)
        if ismember('x',exitationDir{n})
            dir(n) = 0;
        elseif ismember('y',exitationDir{n})
            dir(n) = 1;
        elseif ismember('z',exitationDir{n})
            dir(n) = 2;
        else
            error('Anregungsrichtung nicht ermittelbar');
        end
        if ismember('-',exitationDir{n})
            invertDir(n) = true;
        end
    end
   
    if options.useRed
        T = EVgesSort(:,1:options.useRed);

        KMod = T'*K_Ges*T;
        KMod = diag(diag(KMod));
        DMod = T'*D_GesEq*T;
        %DMod = diag(diag(DMod));
        MMod = eye(size(KMod,1));
    
        % Reduziertes ZRM matlab Bode funktion
%         invM = eye(size(MMod,1));
%         len = size(MMod,1);
% 
%         AMatRed = [zeros(size(MMod)),eye(size(MMod,1));-invM*KMod, -invM*DMod];
%         BMatRed = [zeros(size(T'));invM*T'];
%         BMatRed = BMatRed(:,node+dir);
%         CMatRed = [T,zeros(size(T))]*1/1000;
%         CMatRed = CMatRed(node+dir,:);
%         DMatRed = 0;
% 
%         sys = ss(AMatRed,BMatRed,CMatRed,DMatRed);
%                
%         % in Frequenzgang umwandeln
%         [mag,phase,wout] = bode(sys,wMeas);
%         mag = squeeze(mag);
%         phase = squeeze(phase)/180*pi;
% 
%         HsimRed = mag.*exp(1i*phase); % in komplexen Frequenzgang umwandeln
%         Hsim = HsimRed;

        % Frequenzgang mit Modalen Matritzen ohne Matlab Bode berechnen
        inp = node+dir;
        outp = node+dir;
        outp(logical(invertDir)) = -outp(logical(invertDir)); % wenn anregung und Messung entgegengesetzt sind, output negatives Vorzeichen, f�r F_bode
        Hsim = F_bode(T,MMod,KMod,DMod,inp,outp,wMeas)/1000;
        
        Htemp = zeros(length(Hsim),1);
        for n = 1:length(inp)
            Htemp = Htemp + squeeze(Hsim(n,n,:));
        end
        Hsim = Htemp;      
    else
        invM = inv(M_Ges);%1\M_Ges;  %%ACHTUNG!!!! macht gravierenden unterschied ob invM mit inv(M) oder 1\M berechnet wird. inv() besser
        len = size(M_Ges,1);

        AMat = [zeros(size(M_Ges)),eye(size(M_Ges));-invM*K_Ges,-invM*D_GesEq];
        BMatGes = [zeros(size(M_Ges));invM];
        BMat = BMatGes(:,node + dir);
        BMat(:,logical(invertDir)) = -BMat(:,logical(invertDir)); % f�r invertierte Anregung Eingang negieren
        CMat = [zeros(length(dir),len),zeros(length(dir),len)];
        for n = 1:length(dir)
            CMat(n,node+dir(n)) = 1/1000;
        end
        DMat = 0;

        sys = ss(AMat,BMat,CMat,DMat);

       
%         T = EVgesSort(:,1:options.useRed);
% 
%         KMod = T'*K_Ges*T;
%         KMod = diag(diag(KMod));
%         DMod = T'*D_GesEq*T;
%         MMod = eye(size(KMod,1));
%     
%         % Reduziertes ZRM
%         invM = eye(size(MMod,1));%1\M_Ges;  %%ACHTUNG!!!! macht gravierenden unterschied ob invM mit inv(M) oder 1\M berechnet wird. inv() besser
%         len = size(MMod,1);
% 
%         AMat = [zeros(size(MMod)),eye(size(MMod,1));-invM*KMod, -invM*DMod];
%         BMat = [zeros(size(T'));invM*T'];
%         BMat = BMat(:,node+dir);
%         CMat = [T,zeros(size(T))]*1/1000;
%         CMat = CMat(node+dir,:);
%         DMat = 0;
% 
%         sys = ss(AMatRed,BMatRed,CMatRed,DMatRed);

        
        
        
        % in Frequenzgang umwandeln
        [mag,phase,wout] = bode(sys,wMeas);
        phase = phase/180*pi;

        Hsim = mag.*exp(1i*phase); % in komplexen Frequenzgang umwandeln
        Htemp = zeros(length(Hsim),1);
        for n = 1:length(dir)
            Htemp = Htemp + squeeze(Hsim(n,n,:));
        end
        Hsim = Htemp;
    end
    
    if strcmp(options.objectiveFkt,'FRAC')
        val = 1-F_calculateFRAC(Hmeas,Hsim);
    elseif strcmp(options.objectiveFkt,'CSF')
        val = 1-F_calculateCSF(Hmeas,Hsim);
    elseif strcmp(options.objectiveFkt,'LS')
        val = F_calculateLS(Hmeas,Hsim);
        fprintf('ACHTUNG: experimentelles G�tema�\n')
    elseif strcmp(options.objectiveFkt,'PAC')
        val = 1-F_calculatePAC(Hmeas,Hsim);
        fprintf('ACHTUNG: experimentelles G�tema�\n')
    elseif strcmp(options.objectiveFkt,'witt')
        val = F_calculateWitt(wMeas,Hmeas,Hsim,matchFreq*2*pi,freqRange*2*pi);
    elseif strcmp(options.objectiveFkt,'wittUG')
        val = F_calculateWittUG(wMeas,Hmeas,Hsim,matchFreq*2*pi,freqRange*2*pi);
    end
    
    if options.printMACmatch
        try
            fprintf('Objective fcn: %s: %.3f\n',options.objectiveFkt,val);
        end
    end

 %% Plotten
    if options.plotH
        figure
        semilogy(wMeas/2/pi,abs(Hmeas),'LineWidth',1);
        hold on
        semilogy(wMeas/2/pi,abs(Hsim),'LineWidth',1);
        
%         for n = 1:size(matchFreq,1)
%             plot(ones(2,1)*matchFreq(n,2),[1e-50,1e10],'k:')
%             plot(ones(2,1)*matchFreq(n,1),[1e-50,1e10],'m:')
%         end
        
        ylim([1e-10,1e-6])
        legend('Messung','Simulation')
        xlabel('Frequenz in Hz')
        ylabel('Nachgiebigkeit in m/N')
        grid on
        
        titleString = sprintf('FRAC: %.3f; CSF: %.3f',F_calculateFRAC(Hmeas,Hsim),F_calculateCSF(Hmeas,Hsim));
        title(titleString)
        
        ax = gca;
        fig = gcf;
        set(fig,'Units','centimeters','Position',[10,10,16,8])
        set(ax.Legend,'Interpreter','Latex','FontSize',10);
        set(ax.XLabel,'Interpreter','Latex','FontSize',10)
        set(ax.YLabel,'Interpreter','Latex','FontSize',10)
        set(ax.Title,'Interpreter','Latex','FontSize',11)
        set(ax,'TickLabelInterpreter','Latex');
        
        axPos = get(ax,'Position');
        legPos = get(ax.Legend,'Position');
        set(ax.Legend,'Position',[axPos(1)+axPos(3)-legPos(3) axPos(2)+axPos(4)-legPos(4) legPos(3) legPos(4)]);
        
    end
      
        
end


function [MAC] = F_calculateMAC(Phi1,Phi2)
    MAC = abs(Phi1'*Phi2)^2/((Phi1'*Phi1)*(Phi2'*Phi2));
end

function [FRAC] = F_calculateFRAC(Hmeas,Hsim)
    FRAC = abs(transpose(Hmeas)*conj(Hsim))^2/((transpose(Hmeas)*conj(Hmeas))*(transpose(Hsim)*conj(Hsim)));
end

function [PAC] = F_calculatePAC(Hmeas,Hsim)
    phiMeas = angle(Hmeas)*180/pi;
    phiSim = angle(Hsim)*180/pi;
    
    pMeas = sign(phiMeas);
    pSim = sign(phiSim);
    
    N = length(pMeas);
    
    PAC = pSim'*pMeas/N;    
end

function [CSF] = F_calculateCSF(Hmeas,Hsim)
    CSF = 2*abs(ctranspose(Hmeas)*Hsim)/((ctranspose(Hmeas)*Hmeas)+(ctranspose(Hsim)*Hsim));
end

function [LS] = F_calculateLS(Hmeas,Hsim)
    LS = (transpose(Hsim-Hmeas)*conj(Hsim-Hmeas))/(transpose(Hmeas)*conj(Hmeas));
end

function [witt] = F_calculateWitt(wMeas,Hmeas,Hsim,matchFreq,freqRange)
    aMeas = zeros(size(matchFreq,1),1);
    aSim = aMeas;
    for m = 1:size(matchFreq,1)
        % Simulation
        idx1 = find(abs((wMeas - (matchFreq(m,1)-freqRange))) < (wMeas(2)-wMeas(1))/2);
        idx2 = find(abs((wMeas - (matchFreq(m,1)+freqRange))) < (wMeas(2)-wMeas(1))/2);       
        aSim(m,1) =  max(abs(Hsim(idx1:idx2)));
        
        % Messung
        idx1 = find(abs((wMeas - (matchFreq(m,2)-freqRange))) < (wMeas(2)-wMeas(1))/2);
        idx2 = find(abs((wMeas - (matchFreq(m,2)+freqRange))) < (wMeas(2)-wMeas(1))/2);       
        aMeas(m,1) =  max(abs(Hmeas(idx1:idx2)));
    end
    witt = sum(abs(aMeas-aSim)./aMeas.*[size(matchFreq,1):-1:1]');
end

function [wittUG] = F_calculateWittUG(wMeas,Hmeas,Hsim,matchFreq,freqRange)
    aMeas = zeros(size(matchFreq,1),1);
    aSim = aMeas;
    for m = 1:size(matchFreq,1)
        % Simulation
        idx1 = find(abs((wMeas - (matchFreq(m,1)-freqRange))) < (wMeas(2)-wMeas(1))/2);
        if isempty(idx1)
            idx1 = 1;
        end
        idx2 = find(abs((wMeas - (matchFreq(m,1)+freqRange))) < (wMeas(2)-wMeas(1))/2);
        if isempty(idx2) 
            idx2 = length(Hmeas); 
        end
        aSim(m,1) =  max(abs(Hsim(idx1:idx2)));
        
        % Messung
        idx1 = find(abs((wMeas - (matchFreq(m,2)-freqRange))) < (wMeas(2)-wMeas(1))/2);
        idx2 = find(abs((wMeas - (matchFreq(m,2)+freqRange))) < (wMeas(2)-wMeas(1))/2);       
        aMeas(m,1) =  max(abs(Hmeas(idx1:idx2)));
    end
    wittUG = sum(abs(aMeas-aSim)./aMeas)/length(aMeas);
end


