function [val,dVal,MACmatch] = F_objectiveStiffness(kOpt,MDKungekoppelt,pchKomp,ASET,...
    para,paraName,paraNode,paraKomp,paraPrototype,EVmeas,EV2track,match,options)
%% F_objektivK3
%INPUT:   =================================================================
%   kOpt: Steifigkeiten f�r die das G�tema� ermittelt werden soll
%   MDKungekoppelt: Struct mit den oeffizientenmatritzen der
%           Bewegungsgleichung
%   pchKomp: namen der zu doppelnden Komponenten wie in Tabelle in GUI
%               definiert. Namen m�ssen mit paraKomp �bereinstimmen
%   ASET: vektor mit den Knotennamen
%   para: para array aus der xls Tabelle
%   paraName: namen zu den Parametern aus der Tabelle
%   paraNode: Knotennummern der BNDfix Punkte der Substrukturen
%   paraKomp: komponentenName der Substrukturen in Exceltabelle
%   paraPrototype: kennzeichnet die Werte in para, die durch kOpt ersetzt
%           werden sollen. f�r paraprototype ~= 0 kann dieser matrix der
%           index in kOpt f�r den entsprechenden wert entnommen werden.
%           size(paraPrototype) == size(para)
%   EVmeas: struct mit den Eigenvektoren der Messung wird verwendet f�r
%           den Vergleich der Gemessenen und simulierten EV f�r das G�tema�
%   EV2track: Strukt mit dem selben Aufbau wie EVmeas, die Eigenvektoren
%           von EV2track werden f�r den MAC vergleich f�r das Mode Tracking verwendet.
%           Im optimalfall: EV2track = EVmeas
%   match: zuordnung von Knoten aus Messung und Simulation
%   options: optionen f�r die Berechnung des G�tema�es
%       options -> objectiveFkt: z.B. 'witt'
%       options -> useGrad: zus�tzlich Ableigung von val zur�ckgeben: true/false
%                           (nur experimentell)
%       options -> printMACmatch: ausgabe am Bildschirm: true/false
%       options -> plotMACmat: MAC matrix plotten
%       options -> matchNmodesRange: untere und obere Grenze der zu
%                                    Vergleichenden Moden
%OUTPUT:   ================================================================
%   val: G�tema�, berechnet auf basis der Eingaben. Die Methode zur
%           Berechnung von val kann dem options-Struct entnommen werden
    
    %% zu optimierende Parameter auf Basis von paraPrototype in para schreiben
    para0 = para; % para ohne Koppelparameter speichern f�r die Gradientenberechnung
    for m = 1:size(para,1)
        for n = 1:size(para,2)
            if paraPrototype(m,n) > 0 && paraPrototype(m,n) <= length(kOpt)
                para(m,n) = kOpt(paraPrototype(m,n));
            end
        end
    end
    
    %% koppelSteifigkeiten in Koeffizientenmatritzen einbringen
    if options.advancedMACmatch
        [K_Ges,D_Ges,B_Ges,D_GesEq,EV0,EW0]=F_Koppelung_para(MDKungekoppelt.M,MDKungekoppelt.K,...
            MDKungekoppelt.D,MDKungekoppelt.B,para,paraKomp,paraName,paraNode,pchKomp,ASET);
    else
        [K_Ges,~,~]=F_Koppelung_para(MDKungekoppelt.M,MDKungekoppelt.K,...
            MDKungekoppelt.D,MDKungekoppelt.B,para,paraKomp,paraName,paraNode,pchKomp,ASET);
    end
    M_Ges = MDKungekoppelt.M;
    %% Eigenwerte und Eigenwvektoren bestimmen
    [EVges0,EWges]= eig(K_Ges,M_Ges);
    %EVges=EVges0*sqrt(inv(diag(diag(EVges0'*M_Ges*EVges0))));%massen-normalisieren
    EVges=EVges0*sqrt(diag(1./diag(EVges0'*M_Ges*EVges0))); % speed-up durch vermeiden des invertierens
    %EVges = EVges0; %EVges0 bereits massennormiert
    wGes = diag(sqrt(EWges));
    [wSort,I] = sort(wGes);
    EVgesSort = EVges(:,I);
    
    %% zustandsraum berechnen (experimentell)
    if options.advancedMACmatch
        exitationNode = 4;
        exitationDir = 'x';

        dir = 0;
        node = match(exitationNode,4);

        if strcmp(exitationDir,'x')
            dir = 0;
        elseif strcmp(exitationDir,'y')
            dir = 1;
        elseif strcmp(exitationDir,'z')
            dir = 2;
        else
            error('Anregungsrichtung nicht ermittelbar');
        end

        T = EVgesSort(:,1:50);

        KMod = T'*K_Ges*T;
        KMod = diag(diag(KMod));
        DMod = T'*D_GesEq*T;
        MMod = eye(size(KMod,1));

        % Reduziertes ZRM
        invM = eye(size(MMod,1));%1\M_Ges;  %%ACHTUNG!!!! macht gravierenden unterschied ob invM mit inv(M) oder 1\M berechnet wird. inv() besser
        len = size(MMod,1);

        AMatRed = [zeros(size(MMod)),eye(size(MMod,1));-invM*KMod, -invM*DMod];
        BMatRed = [zeros(size(T'));invM*T'];
        BMatRed = BMatRed(:,node+dir);
        CMatRed = [T,zeros(size(T))]*1/1000;
        CMatRed = CMatRed(node+dir,:);
        DMatRed = 0;

        sys = ss(AMatRed,BMatRed,CMatRed,DMatRed);

        % in Frequenzgang umwandeln
        [mag,phase,wout] = bode(sys,wGes);
        mag = squeeze(mag);
        phase = squeeze(phase)/180*pi;

        HsimRed = mag.*exp(1i*phase); % in komplexen Frequenzgang umwandeln
        Hsim = HsimRed;
    end
    
    %% Ableitung der Eigenvektoren bestimmen
    
    if strcmp(options.objectiveFkt,'quad')
        dk = kOpt * 0.02; %kOpt um 0.2 % varieren
        for n = 1:length(dk)
            kOpt2 = zeros(size(kOpt));
            para2 = para;
            kOpt2(n) = dk(n);
            for m = 1:size(para2,1)
                for k = 1:size(para2,2)
                    if paraPrototype(m,k) ~= 0
                        para2(m,k) = para2(m,k) + kOpt2(paraPrototype(m,k));  %(x+deltaX)
                    end
                end
            end
            [K_Ges2,~,~]=F_Koppelung_para(MDKungekoppelt.M,MDKungekoppelt.K,...
                    MDKungekoppelt.D,MDKungekoppelt.B,para2,paraName,paraNode,pchKomp,ASET);
            dK_Ges(:,:,n) = (K_Ges2-K_Ges)/dk(n); %deltaF/deltaX = (f(x2)-f(x1))/(x2-x1) = (f(x1+deltax)-f(x1))/(x1+deltax-x1)
            %eval(sprintf('dK_Ges%i = (K_Ges-K_Ges2)/(dk(%i));\n',n,n));
        end
    end
    
    %% mit MAC Eigenmoden aus Simulation und Messung matchen (Match mit EV2match)
    wFitMeas = zeros(size(EV2track))';
    MACmat = zeros(length(EV2track),options.matchNmodesRange(2));
    
    for m = 1:length(EV2track)
        wFitMeas(m,1) = EV2track{m}.freq*2*pi;
        measEV = [EV2track{m}.Nodes(match(:,1),2);...
                  EV2track{m}.Nodes(match(:,1),3);...
                  EV2track{m}.Nodes(match(:,1),4)];
        for n = options.matchNmodesRange(1):options.matchNmodesRange(2)
            simEV = [EVgesSort(match(:,4),n);...
                     EVgesSort(match(:,4)+1,n);...
                     EVgesSort(match(:,4)+2,n)];
            MACmat(m,n) = F_calculateMAC(measEV,simEV);
        end
        MACfreqMeas{m} = num2str(EVmeas{m}.freq);
    end
   
    MACfreqSim = wSort(options.matchNmodesRange(1):options.matchNmodesRange(2))/2/pi;
    
    if options.plotMACmat
        temp = zeros(size(MACmat)+1);
        temp(1:size(MACmat,1),1:size(MACmat,2)) = MACmat;
        figure
        pcolor(temp')
        colormap (1-gray)
        title('MAC matrix (experimentell)')
        ax = gca;
        set(ax,'YTick',(1:size(MACmat,2))+0.5)
        set(ax,'YTickLabel',MACfreqSim)
        
        set(ax,'XTick',(1:size(MACmat,1))+0.5)
        set(ax,'XTickLabel',MACfreqMeas)
        set(ax,'XTickLabelRotation',90)
        
        ylabel('Simulation')
        xlabel('Messung')
    end
    
    MACmatch = zeros(length(EV2track),1);
    MAC = zeros(length(EV2track),1);
    modeSearchRange = 2; % es wird innerhalb von +- modeSearchRange um idx2 nach der Mode mit der h�chsten Amplitude gesucht
    for m = 1:length(EV2track) % aus MACmat beste MAC werte suchen (es gibt keine doppelten zuordnungen)
        [idx2,idx1] = F_findMat(MACmat,max(max(MACmat)));     
        if MACmat(idx1,idx2) < 0.6 & options.advancedMACmatch % wenn MAC nicht so gut automatismus einschr�nken (experimentell)
            try %noch optimieren
                HsimTemp = abs(Hsim(idx2-modeSearchRange:idx2+modeSearchRange));
                HsimTemp(MACmat(idx1,idx2-modeSearchRange:idx2+modeSearchRange) < 0.1) = 0; % die Moden die verglichen werden m�ssen zumindest etwas korrelieren
                idx2 = idx2 - modeSearchRange-1 + find(HsimTemp == max(HsimTemp),1);
                if options.printMACmatch
                    fprintf('#### MAC match modified  ####\n')
                end
            end
        end
        MACmatch(idx1) = idx2;
        MAC(idx1) = MACmat(idx1,idx2);
        MACmat(idx1,:) = 0; % Zeile und spalte "l�schen"
        MACmat(:,idx2) = 0;
    end
    
    %% MACvergleich Simulation mit Messung
    
    for m = 1:length(EV2track)
        measEV = [EVmeas{m}.Nodes(match(:,1),2);...
                  EVmeas{m}.Nodes(match(:,1),3);...
                  EVmeas{m}.Nodes(match(:,1),4)];
        simEV = [EVgesSort(match(:,4),MACmatch(m));...
                 EVgesSort(match(:,4)+1,MACmatch(m));...
                 EVgesSort(match(:,4)+2,MACmatch(m))];
        MACmeas(m,1) = F_calculateMAC(measEV,simEV);
    end
    
    %% EV2track f�r modetracking updaten
%      for m = 1:length(EV2track)
%          trackEVprev = [EV2track{m}.Nodes(match(:,1),2),...
%                         EV2track{m}.Nodes(match(:,1),3),...
%                         EV2track{m}.Nodes(match(:,1),4)];
%          simEV = [EVgesSort(match(:,4),MACmatch(m)),...
%                   EVgesSort(match(:,4)+1,MACmatch(m)),...
%                   EVgesSort(match(:,4)+2,MACmatch(m))];
%          EV2track{m}.Nodes(match(:,1),2:4) = simEV;
%          if options.printMACmatch
%              if F_calculateMAC(simEV,trackEVprev) < 0.8
%                  fprintf('Mode-Tracking Qualit�t < 0.8 \n')
%              end
%          end
%      end
    
    %% ergebnisse MACmatch ausgeben
    if options.printMACmatch
        fprintf('######### MAC match #########\n')
        for m = 1:length(EV2track)
            nfd = abs(EV2track{m}.freq-wSort(MACmatch(m))/2/pi)/min(EV2track{m}.freq,wSort(MACmatch(m))/2/pi)*100;
            fprintf('f_meas_%i: \t %7.3f \t f_sim_%i: \t %7.3f \t MAC(tracking):   %4.3f \t MAC (meas)   %4.3f \t NFD: %3.3f\n',m,EV2track{m}.freq, MACmatch(m),wSort(MACmatch(m))/2/pi,MAC(m),MACmeas(m),nfd);
        end
    end
        
    
    %% G�tema�e berechnen 
    
    wSim = wSort(MACmatch);
    
    for m = 1:length(EVmeas)
        wMeas(m,1) = EVmeas{m}.freq*2*pi;
    end
        
   
    % witt
    witt = sum((abs(wSim-wMeas)./wMeas).*(length(wMeas):-1:1)')/length(wSim);
    wittUG = sum(abs(wSim-wMeas)./wMeas)/length(wSim);
        
    % maxDeltaW
    maxDeltaW = max(abs(wSim-wMeas)./wMeas);
    
    % quad
    quad = sum((wMeas.^2 - wSim.^2).^2./wMeas.^2);
    
    % weighteQuad
    weightedQuad = sum((wMeas.^2 - wSim.^2).^2./wMeas.^4.*(length(wMeas):-1:1)');    
        %Ableitung
    dVal = 0;
    if strcmp(options.objectiveFkt,'quad')
        dVal = zeros(size(kOpt));
        for n = 1:length(wSim) %schleife �ber alle EigenModen
            for m = 1:length(kOpt) %Schleife �ber alle Variablen nach denen auzuleiten ist
                dVal(m) = dVal(m) -2*(wMeas(n)^2-wSim(n)^2)/wMeas(n)^4 * ...
                    EVgesSort(:,MACmatch(n))'*dK_Ges(:,:,m)*EVgesSort(:,MACmatch(n));
            end
        end
    end
    
    % mac
    macValue = sum((1-MACmeas).*(length(wMeas):-1:1)')/length(wSim);
    macValueUG = sum(1-MACmeas)/length(wSim);
    
    % combined
    combValue = (witt + macValue)/2;
    combValueUG = (wittUG + macValueUG)/2;
        
     
    method = options.objectiveFkt;
    if strcmp(method,'maxDeltaW')
        val = maxDeltaW;
    elseif strcmp(method,'witt')
        val = witt;
    elseif strcmp(method,'wittUG')
        val = wittUG;
    elseif strcmp(method,'quad')
        val = quad;
    elseif strcmp(method,'weightedQuad')
        val = weightedQuad;
    elseif strcmp(method,'mac')
        val = macValue;
    elseif strcmp(method,'macUG')
        val = macValueUG;
    elseif strcmp(method,'combined')
        val = combValue;
    elseif strcmp(method,'combinedUG')
        val = combValueUG;
    elseif strcmp(method,'sumOfAll')
        val = maxDeltaW + witt + quad + macValue;
    else
        val = NaN;
        warning('Wrong method specified')
    end
    
    
    if options.printMACmatch
        fprintf('UsedMethod: %s val: %10.5f; maxDeltaW: %10.5f; witt: %10.5f; wittUG: %10.5f; quad: %10.5f; weightedQuad: %10.5f; MAC: %10.5f; combined: %10.3f\n',...
            method,val,maxDeltaW,witt,wittUG,quad,weightedQuad,macValue,combValue)
    end
      
        
end

