function [] = F_showFRF(dataObj,fitSet)
%% Diese Funktion plottet die gemessenen und simulierten Nachgiebigkeitsfrequenzg�nge
% inputs:
% dataObj:      Datenobjekt der GUI-Modellabglaich
% fitSet:       fitSet f�r welches die simulierte FRF geplottet werden soll:
%                   -   leer: System ohne gefittete parameter
%                   -   'current': aktuell ausgew�hltes fitSet
%                   -   1,2,3..n : ntes fitSet


if nargin == 1  % System aus simulationstab ohne gefittete Parameter verwenden
    %Systemmatrizen �bernehmen
    T = dataObj.simulation.T;
    KGes = dataObj.simulation.MDKgekoppelt.K;
    DGesEq = dataObj.simulation.MDKgekoppelt.DGesEQ;
    MGes = dataObj.simulation.MDKgekoppelt.M;
    
    % Anregungs und Messrichtung �bernehmen
    try
        for n = 1:3 % f�r alle 3 Raumrichtungen
            inp(n) = find(dataObj.simulation.A == dataObj.simulation.exitationNode*10+n);
            outp(n) = inp(n);
        end
    catch
         msgbox('Knoten f�r anregung konnte nicht gefunden werden','Fehler','error')
         error('Knoten f�r anregung konnte nicht gefunden werden');
    end
    
elseif ~isnumeric(fitSet)
    if strcmp(fitSet,'current') % aktuelles fitSet verwenden
        %Systemmatrizen �bernehmen
        T = dataObj.fit.fittedSystem.T;
        KGes = dataObj.fit.fittedSystem.MDKgekoppelt.K;
        MGes = dataObj.fit.fittedSystem.MDKgekoppelt.M;
        DGesEq = dataObj.fit.fittedSystem.MDKgekoppelt.DGesEQ;
    
        % Anregungs und Messrichtung �bernehmen
        for n = 1:3 % f�r alle 3 Raumrichtungen
            inp(n) = find(dataObj.simulation.A == dataObj.fit.Objective.node*10+n);
            outp(n) = inp(n); % direcr frf: input = output
        end
    
    else
        msgbox('Falsches FitSet gew�hlt','Fehler','error');
    end
else    % �ber die variable fitSet spezifiziertes fitSet verwenden
    %Systemmatrizen �bernehmen
    T = dataObj.fitSet(fitSet).fittedSystem.T;
    KGes = dataObj(fitSet).fit.fittedSystem.MDKgekoppelt.K;
    MGes = dataObj(fitSet).fit.fittedSystem.MDKgekoppelt.M;
    DGesEq = dataObj(fitSet).fit.fittedSystem.MDKgekoppelt.DGesEQ;
    
    % Anregungs und Messrichtung �bernehmen
    for n = 1:3 % f�r alle 3 Raumrichtungen
        inp(n) = find(dataObj.simulation.A == dataObj.fit.Objective.node*10+n);
        outp(n) = inp(n);
    end
    
end

% wie oft integrieren f�r FRF
int = dataObj.messdatenX.logStruct.int;
    

% Modaltrafo der systemmatrizen
KMod = T'*KGes*T;
KMod = diag(diag(KMod));
DMod = T'*DGesEq*T;
DMod = diag(diag(DMod));
MMod = eye(size(KMod,1));



%% Messdaten Extrahieren
wMeasX = dataObj.messdatenX.wMeas;
hMeasX = dataObj.messdatenX.hMeas;
wMeasY = dataObj.messdatenY.wMeas;
hMeasY = dataObj.messdatenY.hMeas;
wMeasZ = dataObj.messdatenZ.wMeas;
hMeasZ = dataObj.messdatenZ.hMeas;

Ts = (wMeasX(2)-wMeasX(1))/2/pi;

%% Frequenzg�nge der Simulation berechnen
% Frequenzvektor f�r Simulierte FRF
% if ~isempty(wMeasX)
%     omega = wMeasX;
% elseif ~isempty(wMeasY)
%     omega = wMeasY;
% elseif ~isempty(wMeasZ)
%     omega = wMeasZ;
% else
%     omega = [dataObj.simulation.freqRange(1):0.5:dataObj.simulation.freqRange(2)]'*2*pi;
% end
omega = [dataObj.simulation.freqRange(1):Ts:dataObj.simulation.freqRange(2)]'*2*pi;

% xRichtung
Hsim = F_bode(T,MMod,KMod,DMod,inp(1),outp(1),omega)/1000;
HX = squeeze(Hsim(1,1,:));

%yRichtung
Hsim = F_bode(T,MMod,KMod,DMod,inp(2),outp(2),omega)/1000;
HY = squeeze(Hsim(1,1,:));

%zRichtung
Hsim = F_bode(T,MMod,KMod,DMod,inp(3),outp(3),omega)/1000;
HZ = squeeze(Hsim(1,1,:));

%% Grenzen und Faktor f�r Normierung berechnen
    aMinX = min([abs(HX);abs(hMeasX)]);
    aMaxX = max([abs(HX);abs(hMeasX)]);
    aNormX = abs(HX(find(omega >= 10*2*pi ,1,'first'))); % Faktor f�r normierung

    aMinY = min([abs(HY);abs(hMeasY)]);
    aMaxY = max([abs(HY);abs(hMeasY)]);
    aNormY = abs(HY(find(omega >= 10*2*pi ,1,'first')));

    aMinZ = min([abs(HZ);abs(hMeasZ)]);
    aMaxZ = max([abs(HZ);abs(hMeasZ)]);
    aNormZ = abs(HZ(find(omega >= 10*2*pi ,1,'first')));

%% Plotten

    fig = figure;
    figPos = [15,10,16,9.5];
    set(fig,'WindowStyle','normal')
    set(fig,'Units','normalized')
%     set(fig,'Position',figPos)
    set(fig,'PaperSize', fig.Position(3:4));


    ax1 = subplot(3,1,1);
    ax2 = subplot(3,1,2);
    ax3 = subplot(3,1,3);

    TitleFontSize = 12;
    LabelFontSize = 12;
    subplotHeigh = 2.1;
    subplotWidth = 13.5;
    subplotSpacing = 0.7;
    subplotX0 = 2.0;
    subplotY0 = 1.2;
    set(ax1,'Units','normalized')
    set(ax2,'Units','normalized')
    set(ax3,'Units','normalized')
    
    % Xplot =======================================================
    semilogy(omega/2/pi,abs(HX),'k--','LineWidth',1,'Parent',ax1);
    set(ax1,'NextPlot','add');
    semilogy(wMeasX/2/pi,abs(hMeasX),'k-','LineWidth',1,'Parent',ax1);
    set(ax1,'XLim',[10,max(omega/2/pi)],'XTick',[50:50:1500],'XTickLabel',{},'XGrid','on','YLim',[aMinX*0.5,aMaxX*2],'YGrid','on','YTick',logspace(-12,10,23),'YGrid','on','YMinorGrid','off','TickLabelInterpreter','latex','FontSize',12);
    set(ax1.Title,'String','Nachgiebigkeit in x-Richtung','Interpreter','latex','FontSize',TitleFontSize)
    set(ax1,'XColor',[0 0 0])
    set(ax1,'YColor',[0 0 0])
    set(ax1,'LabelFontSizeMultiplier',1)
    set(ax1,'TitleFontSizeMultiplier',1)

    % Yplot =======================================================
    semilogy(omega/2/pi,abs(HY),'k--','LineWidth',1,'Parent',ax2);
    set(ax2,'NextPlot','add');
    semilogy(wMeasY/2/pi,abs(hMeasY),'k-','LineWidth',1,'Parent',ax2);
    set(ax2,'XLim',[10,max(omega/2/pi)],'XTick',[50:50:1500],'XTickLabel',{},'XGrid','on','YLim',[aMinY*0.5,aMaxY*2],'YGrid','on','YTick',logspace(-12,10,23),'YGrid','on','YMinorGrid','off','TickLabelInterpreter','latex','FontSize',12);
    set(ax2.YLabel,'String','Nachgiebigkeit in $\frac{\mathrm{m}}{\mathrm{N}}$','Interpreter','latex','FontSize',LabelFontSize)
    set(ax2.Title,'String','Nachgiebigkeit in y-Richtung','Interpreter','latex','FontSize',TitleFontSize)
    set(ax2,'XColor',[0 0 0])
    set(ax2,'YColor',[0 0 0])
    set(ax2,'LabelFontSizeMultiplier',1)
    set(ax2,'TitleFontSizeMultiplier',1)

    % Zplot =======================================================
    semilogy(omega/2/pi,abs(HZ),'k--','LineWidth',1,'Parent',ax3);
    set(ax3,'NextPlot','add');
    semilogy(wMeasZ/2/pi,abs(hMeasZ),'k-','LineWidth',1,'Parent',ax3);
    set(ax3,'XLim',[10,max(omega/2/pi)],'XTick',[50:50:1500],'XGrid','on','YLim',[aMinY*0.5,aMaxY*2],'YGrid','on','YTick',logspace(-12,10,23),'YGrid','on','YMinorGrid','off','TickLabelInterpreter','latex','FontSize',12);
    set(ax3.XLabel,'String','Frequenz in Hz','Interpreter','latex','FontSize',LabelFontSize);
    set(ax3.Title,'String','Nachgiebigkeit in z-Richtung','Interpreter','latex','FontSize',TitleFontSize)
    set(ax3,'XColor',[0 0 0])
    set(ax3,'YColor',[0 0 0])
    set(ax3,'LabelFontSizeMultiplier',1)
    set(ax3,'TitleFontSizeMultiplier',1)

    l = legend('Simulation','Messung');
    set(l,'Interpreter','latex','FontSize',12,'Units','normalized');
    set(l,'Position',[ax3.Position(1)+ax3.Position(3)-l.Position(3),ax3.Position(2)+ax3.Position(4)-l.Position(4),l.Position(3),l.Position(4)]);
%% normiert

    fig = figure;
    set(fig,'WindowStyle','normal')
    set(fig,'Units','normalized')
%     set(fig,'Position',figPos)
    set(fig,'PaperSize', fig.Position(3:4));


    ax1 = subplot(3,1,1);
    ax2 = subplot(3,1,2);
    ax3 = subplot(3,1,3);

    set(ax1,'Units','normalized')
    set(ax2,'Units','normalized')
    set(ax3,'Units','normalized')
    
    % Xplot =======================================================
    semilogy(omega/2/pi,abs(HX)/aNormX,'k--','LineWidth',1,'Parent',ax1);
    set(ax1,'NextPlot','add');
    semilogy(wMeasX/2/pi,abs(hMeasX)/aNormX,'k-','LineWidth',1,'Parent',ax1);
    set(ax1,'XLim',[10,max(omega/2/pi)],'XTick',[50:50:1500],'XTickLabel',{},'XGrid','on','YLim',[aMinX*0.5,aMaxX*2]/aNormX,'YGrid','on','YTick',logspace(-12,10,23),'YGrid','on','YMinorGrid','off','TickLabelInterpreter','latex','FontSize',12);
    set(ax1.Title,'String','Nachgiebigkeit in x-Richtung','Interpreter','latex','FontSize',TitleFontSize)
    set(ax1,'XColor',[0 0 0])
    set(ax1,'YColor',[0 0 0])
    set(ax1,'LabelFontSizeMultiplier',1)
    set(ax1,'TitleFontSizeMultiplier',1)

    % Yplot =======================================================
    semilogy(omega/2/pi,abs(HY)/aNormY,'k--','LineWidth',1,'Parent',ax2);
    set(ax2,'NextPlot','add');
    semilogy(wMeasY/2/pi,abs(hMeasY)/aNormY,'k-','LineWidth',1,'Parent',ax2);
    set(ax2,'XLim',[10,max(omega/2/pi)],'XTick',[50:50:1500],'XTickLabel',{},'XGrid','on','YLim',[aMinY*0.5,aMaxY*2]/aNormY,'YGrid','on','YTick',logspace(-12,10,23),'YGrid','on','YMinorGrid','off','TickLabelInterpreter','latex','FontSize',12);
    set(ax2.YLabel,'String','Nachgiebigkeit normiert','Interpreter','latex','FontSize',LabelFontSize)
    set(ax2.Title,'String','Nachgiebigkeit in y-Richtung','Interpreter','latex','FontSize',TitleFontSize)
    set(ax2,'XColor',[0 0 0])
    set(ax2,'YColor',[0 0 0])
    set(ax2,'LabelFontSizeMultiplier',1)
    set(ax2,'TitleFontSizeMultiplier',1)

    % Zplot =======================================================
    semilogy(omega/2/pi,abs(HZ)/aNormZ,'k--','LineWidth',1,'Parent',ax3);
    set(ax3,'NextPlot','add');
    semilogy(wMeasZ/2/pi,abs(hMeasZ)/aNormZ,'k-','LineWidth',1,'Parent',ax3);
    set(ax3,'XLim',[10,max(omega/2/pi)],'XTick',[50:50:1500],'XGrid','on','YLim',[aMinY*0.5,aMaxY*2]/aNormZ,'YGrid','on','YTick',logspace(-12,10,23),'YGrid','on','YMinorGrid','off','TickLabelInterpreter','latex','FontSize',12);
    set(ax3.XLabel,'String','Frequenz in Hz','Interpreter','latex','FontSize',LabelFontSize);
    set(ax3.Title,'String','Nachgiebigkeit in z-Richtung','Interpreter','latex','FontSize',TitleFontSize)
    set(ax3,'XColor',[0 0 0])
    set(ax3,'YColor',[0 0 0])
    set(ax3,'LabelFontSizeMultiplier',1)
    set(ax3,'TitleFontSizeMultiplier',1)



    l = legend('Simulation','Messung');
    set(l,'Interpreter','latex','FontSize',12,'Units','normalized')
    set(l,'Position',[ax3.Position(1)+ax3.Position(3)-l.Position(3),ax3.Position(2)+ax3.Position(4)-l.Position(4),l.Position(3),l.Position(4)])
    
%% mit FRAC, CSF und LS
hMeasSum = zeros(size(hMeasX));
hSimSum = zeros(size(HX));

if ismember('x',dataObj.fit.Objective.dir)
    hMeasSum = hMeasSum + hMeasX;
    hSimSum = hSimSum + HX;
    wMeas = wMeasX;
end
if ismember('y',dataObj.fit.Objective.dir)
    hMeasSum = hMeasSum + hMeasY;
    hSimSum = hSimSum + HY;
    wMeas = wMeasY;
end
if ismember('z',dataObj.fit.Objective.dir)
    hMeasSum = hMeasSum + hMeasZ;
    hSimSum = hSimSum + HZ;
    wMeas = wMeasZ;
end



wLow = max([wMeas(1),...
            dataObj.fit.Objective.freqRange(1)*2*pi,...
            dataObj.simulation.freqRange(1)*2*pi]);      %evtl m�ssen Grenzen angepasst werden
wHigh = min([wMeas(end),...
             dataObj.fit.Objective.freqRange(2)*2*pi,...
             dataObj.simulation.freqRange(2)*2*pi]);

idxSim1 = max(2,find(omega >= wLow,1,'first'));
idxSim2 = max(2,find(omega >= wHigh,1,'first'));
omega = omega(idxSim1:idxSim2); %omega so zuschneiden, dass es innerhalb der definierten Grenzen liegt
hSimSum = hSimSum(idxSim1:idxSim2);   % Frequenzgang zuschneiden, dass er in den grenzenl liegt


idxMeas1 = max(2,find(wMeas >= wLow,1,'first'));
idxMeas2 = idxMeas1 + idxSim2 - idxSim1;
wMeas = wMeas(idxMeas1:idxMeas2);
hMeasSum = hMeasSum(idxMeas1:idxMeas2);

% Kennwerte Berechnen
FRAC = F_calculateFRAC(hMeasSum,hSimSum);
CSF= F_calculateCSF(hMeasSum,hSimSum);
LS= F_calculateLS(hMeasSum,hSimSum);
titleString = sprintf('FRAC: %4.3f; CSF: %4.3f; LS: %4.3f; in %s Richtung',FRAC,CSF,LS,dataObj.fit.Objective.dir);

    fig = figure;
    set(fig,'WindowStyle','normal')
    set(fig,'Units','normalized')
%     set(fig,'Position',figPos)
    set(fig,'PaperSize', fig.Position(3:4));


    ax1 = subplot(1,1,1);
    set(ax1,'Units','normalized')
    
    % Xplot =======================================================
    semilogy(omega/2/pi,abs(hSimSum),'k--','LineWidth',1,'Parent',ax1);
    set(ax1,'NextPlot','add');
    semilogy(wMeas/2/pi,abs(hMeasSum),'k-','LineWidth',1,'Parent',ax1);
    set(ax1,'XLim',[omega(1)/2/pi,omega(end)/2/pi],'XGrid','on','YGrid','on','YTick',logspace(-12,10,23),'YGrid','on','YMinorGrid','off','TickLabelInterpreter','latex','FontSize',12);
    set(ax1.Title,'String',titleString,'Interpreter','latex','FontSize',TitleFontSize)
    set(ax1.XLabel,'String','Frequenz in Hz','Interpreter','Latex')
    set(ax1.YLabel,'String','nachgiebigkeit in $$\frac{m}{N}$$','Interpreter','Latex')
    set(ax1,'XColor',[0 0 0])
    set(ax1,'YColor',[0 0 0])
    set(ax1,'LabelFontSizeMultiplier',1)
    set(ax1,'TitleFontSizeMultiplier',1)


    l = legend('Simulation','Messung');
    set(l,'Interpreter','latex','FontSize',12,'Units','normalized')
    set(l,'Position',[ax1.Position(1)+ax1.Position(3)-l.Position(3),ax1.Position(2)+ax1.Position(4)-l.Position(4),l.Position(3),l.Position(4)])
    



end

function [FRAC] = F_calculateFRAC(Hmeas,Hsim)
    FRAC = abs(transpose(Hmeas)*conj(Hsim))^2/((transpose(Hmeas)*conj(Hmeas))*(transpose(Hsim)*conj(Hsim)));
end

function [CSF] = F_calculateCSF(Hmeas,Hsim)
    CSF = 2*abs(ctranspose(Hmeas)*Hsim)/((ctranspose(Hmeas)*Hmeas)+(ctranspose(Hsim)*Hsim));
end

function [LS] = F_calculateLS(Hmeas,Hsim)
    LS = (transpose(Hsim-Hmeas)*conj(Hsim-Hmeas))/(transpose(Hmeas)*conj(Hmeas));
end












