function [] = GUI_showFittedParas(guiHandle,dataObj)

hFig = 10;
figX = guiHandle.fig.Position(1) + guiHandle.fig.Position(3)+0.1;
figY = guiHandle.fig.Position(2) + guiHandle.fig.Position(4)-hFig;

GUI.fig = figure;
set(GUI.fig,...
        'Units','centimeters',...
        'Position',[figX,figY,18,hFig],...
        'Name', 'GUI Modellabgleich',...
        'MenuBar','none',...
        'NumberTitle','off',...
        'CloseRequestFcn',@CB_figCloseRequest)
        %'WindowStyle','modal',...

GUI.UITpara = uitable;
colWidth = 100;
set(GUI.UITpara,...
        'Parent',GUI.fig,...
        'Units','centimeters',...
        'ColumnWidth',{colWidth,colWidth,colWidth},...
        'ColumnEditable',false,...
        'Data',{},...
        'Position',[0,0,18,10],...
        'CellEditCallback',@CB_cellEdit);
    
rowName = cell(max(max(dataObj.fit.paraGroupsCurrent.groupsPrototype)),1);
for n = 1:length(dataObj.fit.paraOpt)
    % Name der Parametergruppe extrahieren
    [idx2,idx1] = F_findMat(dataObj.fit.paraGroupsCurrent.groupsPrototype,n);
    groupName = dataObj.fit.paraGroupsCurrent.groups(idx1).ID;
    paraName = dataObj.para.titleBar{5+idx2};
    rowName{n,1} = sprintf('%s : %s',groupName,paraName);
end
set(GUI.UITpara,'ColumnName',{'min','gefittet','max'},...
                'RowName',rowName);
            
GUI.UITpara.Data = [dataObj.fit.paraMin,dataObj.fit.paraFit,dataObj.fit.paraMax];

uiwait(guiHandle.fig)

%% Callbacks 
    
    function CB_figCloseRequest(~,~)
        delete(GUI.fig)
        uiresume(guiHandle.fig)
    end
    
end

