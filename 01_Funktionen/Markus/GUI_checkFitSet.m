function [] = GUI_checkFitSet(dataObj)
%% Diese funktion zeigt alle einstellungen des Aktuellen fitSets an

windowSize = [10,10,15,18];

fig = figure;
set(fig,...
    'Units','centimeters',...
    'Position',windowSize,...
    'Name','control fit-Set',...
    'Menubar','none',...
    'NumberTitle','off');

edit = uicontrol;
set(edit,...
    'Style','edit',...
    'Parent',fig,...
    'Units','normalized',...
    'Position',[0.05,0.05,0.9,0.9],...
    'HorizontalAlignment','left',...
    'Max',2,...
    'FontName','Monospeced');
% modifikationen im Java Objekt, um horizontales scrollen zu ermöglichen
jScrollPane = findjobj(edit);
jViewPort = jScrollPane.getViewport;
jEditbox = jViewPort.getComponent(0);
jEditbox.setWrapping(false);  % do *NOT* use set(...)!!!
newPolicy = 32;%jScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED; never: 31, as needed: 30 always: 32




fit = dataObj.fit;
dataString = '';

dataString = sprintf('%s ###################################################\n',dataString);
dataString = sprintf('%s  Inhalt des Aktuellen Fit-Sets: %s\n',dataString,fit.ID);
dataString = sprintf('%s ###################################################\n\n',dataString);


dataString = sprintf('%s %20s %15s %15s %15s | %15s\n',dataString,...
                                                       'paraName',...
                                                       'min',...
                                                       'nominal',...
                                                       'max',...
                                                       'fitted');
dataString = sprintf('%s ------------------------------------------------------------------------------------------\n',dataString);                                                   
if ~isempty(fit.paraOpt)
    if isempty(fit.paraFit)
        fit.paraFit = ones(size(fit.paraOpt))*99999999999.99;
    end
    for n = 1:length(fit.paraOpt)
        % Name der Parametergruppe extrahieren
        [idx2,idx1] = F_findMat(dataObj.fit.paraGroupsCurrent.groupsPrototype,n);
        groupName = dataObj.fit.paraGroupsCurrent.groups(idx1).ID;
        paraName = dataObj.para.titleBar{5+idx2};
        parameterName = sprintf('%s : %s',groupName,paraName);

        dataString = sprintf('%s %-20s %15.2f %15.2f %15.2f | %15.2f\n',dataString,...
                                             parameterName,...
                                             fit.paraMin(n),...
                                             fit.paraOpt(n),...
                                             fit.paraMax(n),...
                                             fit.paraFit(n));
    end
end


dataString = sprintf('%s\n ~~~~~~~~~ Optimizer ~~~~~~~~~\n',dataString);
if strcmp(fit.Optimizer.ID,'PSO')
    dataString = sprintf('%s Optimizer    : %s\n',dataString,'Particleswarm Optimisation');
    dataString = sprintf('%s nParticles   : %i\n',dataString,fit.Optimizer.nPartPSO);
    dataString = sprintf('%s nIter        : %i\n',dataString,fit.Optimizer.nIterPSO);
    dataString = sprintf('%s maxStallIter : %i\n',dataString,fit.Optimizer.maxStallIterPSO);
elseif strcmp(fit.Optimizer.ID,'GA')
    dataString = sprintf('%s Optimizer    : %s\n',dataString,'Genetic Algorithm');
    dataString = sprintf('%s nIndividui   : %i\n',dataString,fit.Optimizer.nIndGA);
    dataString = sprintf('%s nGenerations : %i\n',dataString,fit.Optimizer.nGenGA);
    dataString = sprintf('%s maxStallIter : %i\n',dataString,fit.Optimizer.maxStallIterGA);
elseif strcmp(fit.Optimizer.ID,'SQP')
    dataString = sprintf('%s Optimizer    : %s\n',dataString,'Sequantial Quadratic Programming');
    dataString = sprintf('%s Optimality Tolerance: %f\n',dataString,fit.Optimizer.optTolSQP);
    dataString = sprintf('%s stepSize     : %i\n',dataString,fit.Optimizer.stepSizeSQP);
    dataString = sprintf('%s maxIterSQP   : %i\n',dataString,fit.Optimizer.maxIterSQP);
else
    dataString = sprintf('%s Optimizer    : %s\n',dataString,fit.Optimizer.ID);
end

dataString = sprintf('%s\n ~~~~~~~~~ Objective ~~~~~~~~~\n',dataString);
dataString = sprintf('%s Objective: %s\n',dataString,fit.Objective.ID);
dataString = sprintf('%s ObjectiveFcn: %s\n\n',dataString,fit.Objective.function);
if strcmp(fit.Objective.function,'damping')
    dataString = sprintf('%s Node      : %i\n',dataString,fit.Objective.node);  
    dataString = sprintf('%s Direction : %s\n\n',dataString,fit.Objective.dir);    
end


if fit.Objective.idx <= 8   % matching von Moden
    if fit.Objective.matchStrategy <= 1 %matchen nach MAC
        dataString = sprintf('%s Mode Matching Method: %s\n',dataString,'MAC');
        dataString = sprintf('%s Matching Modes: [%i - %i]\n',dataString,fit.Objective.matchModesRange);
        for n = 1:length(fit.Objective.modes)
            dataString = sprintf('%s %2i : %4.1f Hz \n',dataString,...
                                                     fit.Objective.modes(n),...
                                                     dataObj.UNV.EVmeas{fit.Objective.modes(n)}.freq);
        end        
    else
        dataString = sprintf('%s Mode Matching Method: %s\n',dataString,'Frequency');
        dataString = sprintf('%s %15s %15s\n',dataString,'measured','simulated');
        dataString = sprintf('%s -------------------------------\n',dataString);                                                   
        for n = 1:size(fit.Objective.matchFreq,1)
             dataString = sprintf('%s %15.2f Hz %15.2f Hz\n',dataString,fit.Objective.matchFreq(n,:));
        end
    end
else
    dataString = sprintf('%s Freq Range: [%4i Hz - %4i Hz]\n\n',dataString,fit.Objective.freqRange);
end


set(edit,...
    'String',dataString,...
    'FontName','Monospaced');
pause(0.1)
set(jScrollPane,'HorizontalScrollBarPolicy',newPolicy);

