function [] = GUI_groupPara(guiHandle,dataObj)
%% parameter
pBx = 4;
pBy = 2;
dpBx = 0.2;
dpBy = 0.2;
okColor = [100 255 100]/255;
notOkColor = [255 100 100]/255;
pbColor = [0.9400 0.9400 0.9400];

%% Datenstrukturen
groups = struct('ID',[],...
                'membersID',[],...    % members aus paras Listbox
                'memberIdx',[]);    % Zeilebnindex in para
            
paras = struct('ID',[],...
               'idx',[]);           % zeilenindex in para
           
selectedParaIdx = 1;               % Zeilenindex des ausgewählten Parameters in der paras-Listbox
selectedGroupIdx = [];              % Zeilenindex des ausgewählten Parameters in der groups-Listbox
selectedMemberIdx = [];             % Zeilenindex des ausgewählten Parameters in der members-Listbox

nParas = length(dataObj.para.paraName);



%% GUI elemente initialisieren
hFig = 5*dpBy+2*pBy/2+4*pBy;
figX = guiHandle.fig.Position(1) + guiHandle.fig.Position(3)+0.1;
figY = guiHandle.fig.Position(2) + guiHandle.fig.Position(4)-hFig;
GUI.fig = figure;
set(GUI.fig,...
        'Units','centimeters',...
        'Position',[figX, figY, 5*dpBx+4*pBx+pBy/2, hFig],...
        'Name', 'GUI Modellabgleich',...
        'MenuBar','none',...
        'NumberTitle','off',...
        'WindowStyle','modal');
    
GUI.LBgroups = uicontrol;
set(GUI.LBgroups,...
        'Style','ListBox',...
        'Parent',GUI.fig,...
        'Units','centimeters',...
        'Position',[dpBx,5*dpBy+3*pBy+0.5,dpBx+2*pBx,2*pBy-0.5-dpBy],...
        'CallBack',@CB_LBgroups);
    
GUI.EgroupName = uicontrol;
set(GUI.EgroupName,...
        'Style','edit',...
        'Parent',GUI.fig,...
        'Units','centimeters',...
        'Position',[dpBx,4*dpBy+3*pBy,dpBx+2*pBx,0.5],...
        'String','Gruppenname eingeben...');
    
GUI.LBgroupMembers = uicontrol;
set(GUI.LBgroupMembers,...
        'Style','ListBox',...
        'Parent',GUI.fig,...
        'Units','centimeters',...
        'Position',[dpBx,2*dpBy+pBy/2,dpBx+2*pBx,pBx],...
        'CallBack',@CB_LBmembers);
    
GUI.PBaddGroup = uicontrol;
set(GUI.PBaddGroup,...
        'Style','PushButton',...
        'Parent',GUI.fig,...
        'Units','centimeters',...
        'Position',[dpBx,3*dpBy+2.5*pBy,pBx,pBy/2],...
        'String','<HTML><div align = "center">Parametergruppe<br>Hinzufügen</div></HTML>',...
        'CallBack',@CB_addGroup);
    
GUI.PBdelGroup = uicontrol;
set(GUI.PBdelGroup,...
        'Style','PushButton',...
        'Parent',GUI.fig,...
        'Units','centimeters',...
        'Position',[2*dpBx+pBx,3*dpBy+2.5*pBy,pBx,pBy/2],...
        'String','<HTML><div align = "center">Parametergruppe<br>Entfernen</div></HTML>',...
        'CallBack',@CB_delGroup);
    
GUI.LBparas = uicontrol;
set(GUI.LBparas,...
        'Style','ListBox',...
        'Parent',GUI.fig,...
        'Units','centimeters',...
        'Position',[4*dpBx+2*pBx+pBy/2,2*dpBy+pBy/2,2*pBx,4.5*pBy+2*dpBy],...
        'CallBack',@CB_LBparas);
        
GUI.PBaddPara = uicontrol;
set(GUI.PBaddPara,...
        'Style','PushButton',...
        'Parent',GUI.fig,...
        'Units','centimeters',...
        'Position',[3*dpBx+2*pBx,(2.5)*dpBy+1.5*pBy,pBy/2,pBy/2],...
        'String','<html><&larr<html>',...
        'CallBack',@CB_addPara);
        
GUI.PBdelPara = uicontrol;
set(GUI.PBdelPara,...
        'Style','PushButton',...
        'Parent',GUI.fig,...
        'Units','centimeters',...
        'Position',[3*dpBx+2*pBx,2*pBy/2+dpBy/2,pBy/2,pBy/2],...
        'String','<html><&rarr<html>',...
        'CallBack',@CB_delPara);

    
    
    
GUI.PBaddGroup = uicontrol;
set(GUI.PBaddGroup,...
        'Style','PushButton',...
        'Parent',GUI.fig,...
        'Units','centimeters',...
        'Position',[4*dpBx+2*pBx+pBy/2,dpBy,pBx-dpBx/2,pBy/2],...
        'String','Abbrechen',...
        'CallBack',@CB_abort);
    
GUI.PBdelGroup = uicontrol;
set(GUI.PBdelGroup,...
        'Style','PushButton',...
        'Parent',GUI.fig,...
        'Units','centimeters',...
        'Position',[4.5*dpBx+3*pBx+pBy/2,dpBy,pBx-dpBx/2,pBy/2],...
        'String','OK',...
        'CallBack',@CB_ok);
    
%% Parameter in paras-Listbox schreiben
    if isempty(dataObj.fit.paraGroupsCurrent.groups) ||...
       isempty(dataObj.fit.paraGroupsCurrent.groups(1).ID) % wenn noch keine Gruppierung vorgenommen wurde, paras initialisieren
        for n = 1:length(dataObj.para.paraName)        
            GUI.LBparas.String{n,1} = sprintf('%s : %s',dataObj.para.paraKomp{n},dataObj.para.paraName{n});       
            paras(n).ID = sprintf('%s : %s',dataObj.para.paraKomp{n},dataObj.para.paraName{n});
            paras(n).idx = n;
        end
    else    % wenn bereits eine initialisierung vorgenommen wurde, übernehmen
        groups = dataObj.fit.paraGroupsCurrent.groups;
        paras = dataObj.fit.paraGroupsCurrent.paras;
    end
    % Listboxes updaten
    F_updateGroups();
    selectedGroupIdx = 1;
    F_updateMembers();
    F_updateParas();
    
%% HauptGui pausieren
%     uiwait(guiHandle.fig);
%% Callbacks
    function CB_LBgroups(src,eventData)
        selectedGroupIdx = src.Value;
        F_updateMembers();
        if ~isempty(selectedGroupIdx)
            if ~isempty(groups(selectedGroupIdx).membersID)
                for n = 1:length(groups(selectedGroupIdx).membersID)
%                     GUI.LBmembers(n,:) = groups(selectedGroupIdx).membersID(n);
                end
            end
        end
    end

    function CB_LBparas(src,eventData)
        selectedParaIdx = src.Value;
    end

    function CB_LBmembers(src,eventData)
        selectedMemberIdx = src.Value;
    end

    function CB_addGroup(~,~)
        if isempty(groups) || isempty(groups(1).ID)
            nGroup = 1;
        else
            nGroup = length(groups) + 1;
        end
        selectedGroupIdx = nGroup;
        selectedMemberIdx = 1;
        GUI.LBgroups.String{nGroup,1} = sprintf('%s',GUI.EgroupName.String);
        GUI.LBgroups.Value = nGroup;
        groups(nGroup).ID = GUI.EgroupName.String;
        F_updateMembers;
    end

    function CB_delGroup(~,~)
        if ~isempty(groups)
            for n = 1:length(groups(selectedGroupIdx).memberIdx)     % nach und nach alle members aus der Gruppe löschen und zurück in paras schreiben      
                paras(end+1).ID = groups(selectedGroupIdx).membersID{n};
                paras(end).idx = groups(selectedGroupIdx).memberIdx(n);
            end
            groups(selectedGroupIdx) = [];
            F_updateGroups();
            F_updateMembers();
            F_updateParasStruct();
            F_updateParas();
        end
    end

    function CB_addPara(~,~)
        if ~isempty(selectedGroupIdx) && ~isempty(paras)
            ID = paras(selectedParaIdx).ID;
            memberIdx = paras(selectedParaIdx).idx;   % in den ersten 4 chars steht der Zeilenindex des Parameters 
            groups(selectedGroupIdx).membersID{end+1,1} = ID;
            groups(selectedGroupIdx).memberIdx(end+1,1) = memberIdx;
            paras(selectedParaIdx) = [];
            F_updateMembers();
            F_updateParas();
        end
    end

    function CB_delPara(~,~)
        if ~isempty(groups(selectedGroupIdx).memberIdx)
            paras(end+1).ID = groups(selectedGroupIdx).membersID{selectedMemberIdx};
            paras(end).idx = groups(selectedGroupIdx).memberIdx(selectedMemberIdx);
            groups(selectedGroupIdx).membersID(selectedMemberIdx) = [];
            groups(selectedGroupIdx).memberIdx(selectedMemberIdx) = [];
            if isempty(groups(selectedGroupIdx).memberIdx)
                groups(selectedGroupIdx).membersID = [];
                groups(selectedGroupIdx).memberIdx = [];
            end
            F_updateParasStruct();
        end
    end

    function CB_abort(~,~)
        delete(GUI.fig);
    end

    function CB_ok(~,~)
        dataObj.fit.paraGroupsCurrent.groups = groups;
        dataObj.fit.paraGroupsCurrent.paras = paras;
        delete(GUI.fig);
    end

    %% andere Funktionen
    function F_updateGroups()
        GUI.LBgroups.String = [];
        for n = 1:length(groups)
            GUI.LBgroups.String{n,1} = groups(n).ID;
        end
        if n < GUI.LBgroups.Value
            GUI.LBgroups.Value = n;
            selectedGroupIdx = n;
        end
    end

    function F_updateMembers()  % Groupmembers Listbox updaten
        GUI.LBgroupMembers.String = [];
        if ~isempty(groups) && ~isempty(groups(selectedGroupIdx).memberIdx)
            for n = 1:length(groups(selectedGroupIdx).memberIdx)
                GUI.LBgroupMembers.String{n,1} = groups(selectedGroupIdx).membersID{n,:};
            end
            if GUI.LBgroupMembers.Value > n
                GUI.LBgroupMembers.Value = n;
                selectedMemberIdx = n;
            end
        end
    end

    function F_updateParas()   % paras Listbox updaten
        GUI.LBparas.String = [];
        for n = 1:length(paras)
            GUI.LBparas.String{n,1} = sprintf('%s',paras(n).ID);       
        end
    end

    function F_updateParasStruct()  % paras Struct in die anfangsreihenfolge bringen
        paraTemp.ID = [];
        paraTemp.idx = [];
        paraIdx = [];
        for n = 1:length(paras)
            paraIdx(n) = paras(n).idx;
        end
            n = 1;
        for m = 1:nParas+1
            idx = find(paraIdx == m);
            if ~isempty(idx)
                paraTemp(n).ID = paras(idx).ID;
                paraTemp(n).idx = paras(idx).idx;
                n = n+1;
            end
        end
        paras = paraTemp;
        F_updateParas();
        F_updateMembers();
    end


end

