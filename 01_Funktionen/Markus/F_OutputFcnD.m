function [stop] = F_OutputFcnD(optimValues,state)
%% Datei kann in jeder iteration des Partikelschwarmes aufgerufen werden um logdaten herauszuschreiben
    persistent fid;
    persistent formatSpec;
    
    stop = false;
    if strcmp(state,'init')
        filename = 'ParticlesLog(damping)';
        path = '03_Fit\temp\';
        
        formatSpec = '%10i: \t';
        for n = 1:size(optimValues.swarm,2)
            formatSpec = [formatSpec,' , %15.2f'];
        end
        formatSpec = [formatSpec,'; FVAL: \t %10.5f\n'];
                
        fid = fopen([path,filename,datestr(now,30),'.txt'],'w+');
        fprintf(fid,'ITERATION init\n');
        fprintf(fid,'PARTICLES\n');
        for n = 1:size(optimValues.swarm,1)
            fprintf(fid,formatSpec,n,optimValues.swarm(n,:),optimValues.swarmfvals(n));
        end
        fprintf(fid,'BEST \n');
        fprintf(fid,formatSpec,0,optimValues.bestx,optimValues.bestfval);
        fprintf(fid,'\n\n\n');
        
    elseif strcmp(state,'iter')
        fprintf(fid,'ITERATION %i\n',optimValues.iteration);
        fprintf(fid,'PARTICLES\n');
        for n = 1:size(optimValues.swarm,1)
            fprintf(fid,formatSpec,n,optimValues.swarm(n,:),optimValues.swarmfvals(n));
        end
        fprintf(fid,'BEST \n');
        fprintf(fid,formatSpec,0,optimValues.bestx,optimValues.bestfval);
        fprintf(fid,'\n\n\n');
    else
        fprintf(fid,'FINISHED\n',optimValues.iteration);
        
        fVal = optimValues.swarmfvals;
        sortFVal = sort(fVal);
        
        mean = sum(optimValues.swarm)/size(optimValues.swarm,1);
        maxErr = max(optimValues.swarm - mean);
        
        try
        thres90 = sortFVal(floor(length(fVal)*0.9));
        mean90 = sum(optimValues.swarm(fVal <= thres90,:))/size(optimValues.swarm(fVal <= thres90),1);
        maxErr90 = max(optimValues.swarm(fVal <= thres90,:)-mean90);
        
        thres50 = sortFVal(floor(length(fVal)*0.5));
        mean50 = sum(optimValues.swarm(fVal <= thres50,:))/size(optimValues.swarm(fVal <= thres50),1);
        maxErr50 = max(optimValues.swarm(fVal <= thres50,:)-mean90);
        
        thres10 = sortFVal(floor(length(fVal)*0.1));
        mean10 = sum(optimValues.swarm(fVal <= thres10,:))/size(optimValues.swarm(fVal <= thres10),1);
        maxErr10 = max(optimValues.swarm(fVal <= thres10,:)-mean10);
        
        fprintf(fid,'MEAN:\n');
        fprintf(fid,formatSpec,100,mean,NaN);
        fprintf(fid,'MAXERR:\n');
        fprintf(fid,formatSpec,100,maxErr,NaN);
        
        fprintf(fid,'MEAN:\n');
        fprintf(fid,formatSpec,90,mean90,NaN);
        fprintf(fid,'MAXERR:\n');
        fprintf(fid,formatSpec,90,maxErr90,NaN);
        
        fprintf(fid,'MEAN:\n');
        fprintf(fid,formatSpec,50,mean50,NaN);
        fprintf(fid,'MAXERR:\n');
        fprintf(fid,formatSpec,50,maxErr50,NaN);
        
        fprintf(fid,'MEAN:\n');
        fprintf(fid,formatSpec,10,mean10,NaN);
        fprintf(fid,'MAXERR:\n');
        fprintf(fid,formatSpec,10,maxErr10,NaN);
        
        
        fprintf(fid,'TOCOPY: \n')
        str = '';
        for n = 1:size(optimValues.swarm,2)
            str = [str,'%f '];
        end
        fprintf(fid,['mean100 = [',str,'];\n'],mean);
        fprintf(fid,['maxErr100 = [',str,'];\n'],maxErr);
        fprintf(fid,['mean90 = [',str,'];\n'],mean90);
        fprintf(fid,['maxErr90 = [',str,'];\n'],maxErr90);
        fprintf(fid,['mean50 = [',str,'];\n'],mean50);
        fprintf(fid,['maxErr50 = [',str,'];\n'],maxErr50);
        fprintf(fid,['mean10 = [',str,'];\n'],mean10);
        fprintf(fid,['maxErr10 = [',str,'];\n'],maxErr10);
        
        fprintf(fid,['',...
        'figure\n'...
        'bar(mean100,',char(39),'b',char(39),')\n',...
        'hold on\n'...
        'errorbar(1:9,mean100,maxErr100,',char(39),'b',char(39),')\n',...
        'bar(mean90,',char(39),'r',char(39),')\n',...
        'errorbar(1:9,mean90,maxErr90,',char(39),'r',char(39),')\n',...
        'bar(mean50,',char(39),'g',char(39),')\n',...
        'errorbar(1:9,mean50,maxErr50,',char(39),'g',char(39),')\n',...
        'bar(mean10,',char(39),'y',char(39),')\n',...
        'errorbar(1:9,mean10,maxErr10,',char(39),'y',char(39),')\n',...       
        'legend(',char(39),'mean100',char(39),',',char(39),...
        'maxErr100',char(39),',',char(39),'mean90',char(39),...
        ',',char(39),'maxErr90',char(39),',',char(39),...
        'mean50',char(39),',',char(39),'maxErr50',char(39),...
        ',',char(39),'mean10',char(39),',',char(39),'maxErr10',char(39),');'])
        catch
            fprintf(fid,'FAILED calculating mean\n')
        end
        
        fclose(fid);
    end
end

