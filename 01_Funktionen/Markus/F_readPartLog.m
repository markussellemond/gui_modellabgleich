function [ iteration , nIter, nPart] = F_readPartLog( filename )
    %% Diese funktion kann verwendet werden, um logdate, welche w�rhend der PSO geschrieben wurden wieder zu lesen
    fid = fopen(filename);
    
    fseek(fid, 0,'eof');
    filelength = ftell(fid);
    fseek(fid, 0,'bof');
    
    nIter = 0;
    nPart = 0;
    nVars = [];
    
    part = false;
    best = false;
    formatSpec = [];
    
    while 1
        tline = fgetl(fid);
        if tline == -1;         % end of file erreicht
            fclose(fid);
            return;
        end
        
        if strfind(tline,'ITERATION') %neue iteration Beginnt
            if strfind(tline,'init')
                nIter = 0
                fprintf('Reading nIter init\n');
            else
                nIter = str2num(tline(11:end));
                fprintf('Reading nIter %i\n',nIter);
            end
            iteration{nIter + 1}.nIter = nIter;
        end
        
        if strfind(tline,'BEST')       % bestes Ergebnis der Iteration
            best = true;
            part = false;
        elseif best == true
            best = false;
            
            particle = sscanf(tline,formatSpec)';
            fVal = particle(end);
            particle = particle(2:end-1);
            
            iteration{nIter+1}.best.particles(1,:) = particle;
            iteration{nIter+1}.best.fVal(1,:) = fVal;
            
            [mean100,mean90,mean80,mean50,mean20,mean10] = F_mean(iteration{nIter+1}.particles,iteration{nIter+1}.fVal);
            iteration{nIter+1}.mean100 = mean100;
            iteration{nIter+1}.mean90 = mean90;
            iteration{nIter+1}.mean80 = mean80;
            iteration{nIter+1}.mean50 = mean50;
            iteration{nIter+1}.mean20 = mean20;
            iteration{nIter+1}.mean10 = mean10;
        end
        
        if strfind(tline,'PARTICLES')  % wenn partikelinformationen kommen
            part = true;
        elseif part == true             % Partikelinfos
            if isempty(nVars)
                nVars = length(strfind(tline,',')); % eizelne Werte der Partikel sind mit kommata getrennt
                formatSpec = '%i: \t';
                for n = 1:nVars
                    formatSpec = [formatSpec,' , %f'];
                end
                formatSpec = [formatSpec,'; FVAL: \t %f\n'];
            end
            particle = sscanf(tline,formatSpec)';
            nPart = particle(1);
            fVal = particle(end);
            particle = particle(2:end-1);
            iteration{nIter+1}.particles(nPart,:) = particle;
            iteration{nIter+1}.fVal(nPart,1) = fVal;
        end
        if strfind(tline,'FINISHED')  % wenn partikelinformationen kommen
            fclose(fid);
            return;
        end
    end
end

function [mean100,mean90,mean80,mean50,mean20,mean10] = F_mean(particles,fVal)

    mean100 = mean(particles);
    
    fValSort = sort(fVal);
    fVal90 = fValSort(floor(length(fVal)*0.9));
    fVal80 = fValSort(floor(length(fVal)*0.8));
    fVal50 = fValSort(floor(length(fVal)*0.5));
    fVal20 = fValSort(floor(length(fVal)*0.2));
    fVal10 = fValSort(floor(length(fVal)*0.1));
    
    mean90 = mean(particles(fVal <= fVal90,:));
    mean80 = mean(particles(fVal <= fVal90,:));
    mean50 = mean(particles(fVal <= fVal90,:));
    mean20 = mean(particles(fVal <= fVal90,:));
    mean10 = mean(particles(fVal <= fVal90,:));
end













