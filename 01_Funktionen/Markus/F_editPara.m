function [] = F_editPara(guiHandle,dataObj)

GUI.fig = figure;
set(GUI.fig,...
        'Units','centimeters',...
        'Position',[10,10,30,18],...
        'Name', 'GUI Modellabgleich',...
        'MenuBar','none',...
        'NumberTitle','off',...
        'CloseRequestFcn',@CB_figCloseRequest)

GUI.UITpara = uitable;
set(GUI.UITpara,...
        'Parent',GUI.fig,...
        'Units','centimeters',...
        'ColumnWidth','auto',...
        'ColumnEditable',true,...
        'Data',{},...
        'Position',[0,0,30,18],...
        'CellEditCallback',@CB_cellEdit);
    
rowName = cell(length(dataObj.para.paraKomp),1);
for n = 1:length(dataObj.para.paraKomp)
    rowName{n,1} = [dataObj.para.paraKomp{n},':',dataObj.para.paraName{n}];
end
set(GUI.UITpara,'ColumnName',dataObj.para.titleBar(3:end),...
                'RowName',rowName);
GUI.UITpara.Data(:,1) = dataObj.para.paraInnerDof;
GUI.UITpara.Data(:,2:3) = num2cell(dataObj.para.paraNode);
GUI.UITpara.Data(:,4:size(dataObj.para.para,2)+3) = num2cell(dataObj.para.para);

uiwait(guiHandle.fig)

%% Callbacks 
    function CB_cellEdit(scr,eventData)
        idx1 = eventData.Indices(1);
        idx2 = eventData.Indices(2);
        if idx2 > 3 % para werden editiert
            idx2 = idx2-3;
            dataObj.para.para(idx1,idx2) = eventData.NewData;
        elseif idx2 > 1 % nodes werden editiert
            idx2 = idx2 - 1;
            dataObj.para.paraNode(idx1,idx2) = eventData.NewData;
        end
            
    end
    
    function CB_figCloseRequest(~,~)
        delete(GUI.fig)
        uiresume(guiHandle.fig)
    end
    
end

