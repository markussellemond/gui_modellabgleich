function [] = GUI_defineMatchFreq(guiHandle,dataObj)

hFig = 10;
figX = guiHandle.fig.Position(1) + guiHandle.fig.Position(3)+0.1;
figY = guiHandle.fig.Position(2) + guiHandle.fig.Position(4)-hFig;

GUI.fig = figure;
set(GUI.fig,...
        'Units','centimeters',...
        'Position',[figX,figY,10,hFig],...
        'Name', 'GUI Modellabgleich',...
        'MenuBar','none',...
        'NumberTitle','off',...
        'CloseRequestFcn',@CB_figCloseRequest)
        %'WindowStyle','modal',...

GUI.UITpara = uitable;
colWidth = 150;
set(GUI.UITpara,...
        'Parent',GUI.fig,...
        'Units','centimeters',...
        'ColumnWidth',{colWidth,colWidth},...
        'ColumnEditable',true,...
        'Data',{[],[]},...
        'Position',[0,0,10,10],...
        'CellEditCallback',@CB_cellEdit);
    

set(GUI.UITpara,'ColumnName',{'Frequenz Messung','Frequenz Simulation'});
            
for n = 1:size(dataObj.fit.Objective.matchFreq,1)
    GUI.UITpara.Data{n,1} = num2str(dataObj.fit.Objective.matchFreq(n,1));
    GUI.UITpara.Data{n,2} = num2str(dataObj.fit.Objective.matchFreq(n,2));
end
GUI.UITpara.Data{end+1,1} = '';
GUI.UITpara.Data{end,2} = '';
    

% uiwait(guiHandle.fig)

%% Callbacks 
    function CB_cellEdit(scr,eventData)
        idx1 = eventData.Indices(1);
        idx2 = eventData.Indices(2);
        if isempty(GUI.UITpara.Data{idx1,1}) && isempty(GUI.UITpara.Data{idx1,2}) % Zeile ist leer: Zeile L�schen
            GUI.UITpara.Data(idx1,:) = [];
            dataObj.fit.Objective.matchFreq(idx1,:) = [];
        else
            if ~isempty(GUI.UITpara.Data{idx1,idx2})    % nur wenn in der Zelle etwas drinnen steht matchFreq updaten
                dataObj.fit.Objective.matchFreq(idx1,idx2) = str2num(GUI.UITpara.Data{idx1,idx2});
            end
            if idx1 == size(GUI.UITpara.Data,1)
                GUI.UITpara.Data{end+1,1} = '';
                GUI.UITpara.Data{end,2} = '';
            end
        end
    end
    
    function CB_figCloseRequest(~,~)
        delete(GUI.fig)
        uiresume(guiHandle.fig)
    end
    
end

