function [EVmeas,Nodes,usedNodes,components,fileName] = F_loadUNV(UNV_File)
fprintf(' ---- opening Measurement Data ----')

path_sim=[cd '\01_Daten_FE'];
model_cb=['03_CB'];
path_meas=['04_Messung'];

if nargin == 0
    [UNV_File, UNV_Path] = uigetfile({'*.unv','Universal File (*.unv)'},...
            'Select Coordinate Modal Data File',path_sim);
    UNV_content = F_readuff([UNV_Path,UNV_File]);
    fileName = UNV_File;
elseif strcmp(UNV_File(end-2:end),'unv') % ein UNV-File wird �bergeben
    try
        UNV_content = F_readuff(UNV_File);
    catch
        warning('Datei konnte nicht ge�ffnet werden. Sie muss im Matlab Path liegen')
    end
else % wenn keine konkrete UNV datei �bergeben wird, dann ist es ein Pfad, in dem Gesucht werden soll
    path_sim = [UNV_File,'\'];
    [UNV_File, UNV_Path] = uigetfile({'*.unv','Universal File (*.unv)'},...
            'Select Coordinate Modal Data File',path_sim);
    UNV_content = F_readuff([UNV_Path,UNV_File]);
    fileName = UNV_File;
end
fprintf(' done\n')



%% Get UNV coordinates
fprintf(' ---- reading UNV coordinates ----')
ConvertExptoSimUnitFactor = 1;
%Messkoordinaten aus UNV-File herausholen
entry = 1;
%entry mit Messkoordinaten bestimmen
while entry <= length(UNV_content(1,:))
    if UNV_content{1,entry}.dsType == 2411  %2411 beinhaltet die Messkoordinaten
        break
    else
        entry = entry + 1;
    end
end
Nodes = [UNV_content{1,entry}.nodeN,...
    UNV_content{1,entry}.x*ConvertExptoSimUnitFactor,...
    UNV_content{1,entry}.y*ConvertExptoSimUnitFactor,...
    UNV_content{1,entry}.z*ConvertExptoSimUnitFactor,...
    zeros(length(UNV_content{1,entry}.nodeN(:,1)),1)];
Nodes = sortrows(Nodes,5);
fprintf(' done \n')



%% Get UNV Eigenvectors
fprintf(' ---- reading UNV Eigenvectors ----')
clear EVmeas

modeNodes = [];
EVmeas{1}.Nodes = [];
EVmeas{1}.ID = [];
EVmeas{1}.num = [];
EVmeas{1}.freq = [];
n = 1;
for m = 1:length(UNV_content(1,:))
    if UNV_content{1,m}.dsType == 55
        modeNodes(n) = m;
        EVmeas{n}.num = n;
        EVmeas{n}.Nodes(:,:) = [UNV_content{1,m}.nodeNum,UNV_content{1,m}.r1,UNV_content{1,m}.r2,UNV_content{1,m}.r3];
        EVmeas{n}.ID = UNV_content{1,m}.IDs;
        if isempty(strfind(UNV_content{1,m}.d1,'Residuals'))
            EVmeas{n}.freq = UNV_content{1,m}.modeFreq;
        else
            EVmeas{n}.freq = UNV_content{1,m}.freq;
            EVmeas{n}.ID = [EVmeas{n}.ID,' (Residual)'];
        end
        n = n+1;
    end
end

% fehlende nicht gemessene Knoten hinzuf�gen
for m = 1:size(EVmeas,2)
    EVmeas{m}.Nodes(end+1:length(Nodes),:) = 0;
    usedNodes = [];
    for n = 1:size(EVmeas{m}.Nodes,1)
        usedNodes(end+1,1) = n;   %Vektor der Knoten, die effektiv verwendet werden
        if isempty(find(EVmeas{m}.Nodes(:,1) == n)) %wenn dieser Knoten nicht in Eigenvektor vorhanden ist
            EVmeas{m}.Nodes(find(EVmeas{m}.Nodes(:,1) == 0,1,'first'),1) = n; %knoten k�nstlich hinzuf�gen
            %Nodes(n,[2:4]) = 0;
            usedNodes(end,:) = []; %wenn knoten nicht benutzt wird, letzten EIntrag l�schen
        end
    end
end


Nodes(usedNodes,5) = 1;

% eigenvektoren nach knoten Sortieren
for m = 1:size(EVmeas,2)
    EVmeas{m}.Nodes(:,:) = sortrows(EVmeas{m}.Nodes(:,:),1);
end

fprintf('done\n')

%% get UNV Komponents
fprintf(' ---- reading Node Names and Kompoents ----')
exitFlag = false;
components = struct('ID',[],'nodes',[],'nodeNames',[],'lines',[]);
nComp = 1;
MAP_File = [UNV_File(1:end-3),'map'];
comps = {}; %lokale variable zum mitz�hlen der Komponenten
fID = fopen([UNV_Path,MAP_File]);
if exist([UNV_Path,MAP_File])
    while ~exitFlag
        textLine = fgetl(fID);
        if textLine == -1
            exitFlag = true;
        elseif ~ismember('#',textLine)
            nodeNum = str2num(textLine(1:11));  % im ersten Teil findet sich die Knotennummer
            nodeName = textLine(12:end);        % in zweiten Teil findet sich der Name des Knotens
            nodeName(nodeName == 32) = [];      % alle Leerzeichen entfernen 
            compName = nodeName(1:find(nodeName == ':')-1); % im ersten Teil steht der Komponentenname  
            
            nComp = find(strcmp(comps,compName));
            if isempty(nComp)   % wenn die komponente noch nicht initialisiert wurde
                components(length(comps)+1).ID = compName;
                comps{length(comps)+1} = compName;  
                nComp = length(comps);
            end
            components(nComp).nodes(end+1) = nodeNum;
            components(nComp).nodeNames{end+1} = nodeName;  
        end
    end   
    fclose(fID);
else
    components(1).ID = 'masterComponent';
    components(1).nodes = Nodes(:,1);
    for n = 1:length(Nodes)
        components(1).nodeNames{n} = sprintf('P%i',n);
    end
end

fprintf('done\n')


%% Get UNV Lines
fprintf(' ---- reading UNV lines ----')
entry = 1;
%entry mit Messkoordinaten bestimmen
while entry <= length(UNV_content(1,:))
    if UNV_content{1,entry}.dsType == 82  %82 beinhaltet die Linien undKomponenten des Messmodells
        compName = UNV_content{1,entry}.ID;
        nComp = find(strcmp(comps,compName));
        if ~isempty(nComp)
            components(nComp).lines = UNV_content{1,entry}.lines;
        elseif strcmp(components(1).ID,'masterComponent')
            components(1).lines = [components(1).lines;UNV_content{1,entry}.lines];
        end
    end
    entry = entry + 1;
end

fprintf(' done\n')

end

