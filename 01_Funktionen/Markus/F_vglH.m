function [outputArg1,outputArg2] = F_vglH(MDK,node,dir,Hmeas,wMeas,useRed)
%F_vglH Diese Funktion vergleicht den Simulierten und den Gemessenen
%Frequenzgang miteinander (direct FRF)
%INPUT: 
%   MDK: bereits gekoppelte Massen, Steifigkeits, und D�mpfungsmatritzen
%   node: Knotennummer in der Match-Matrix
%   dir: Richtung der Anregung und Messung als string 'x','y','z'
%   Hmeas:  komplexer Frequenzgang
%   wMeas: frequenzvektor passend zu H
%   useRed: boolean um f�r reduziertes/nicht reduziertes Modell

    [EV,EW] = eig(MDK.K,MDK.M);
    EV = EV * sqrt(inv(diag(diag(EV'*MDK.M*EV)))); %Massen normieren
    wGes = diag(sqrt(abs(EW)));
    [wSortGes,I] = sort(wGes);
    EVsort = EV(:,I);
    
    % ein mal modaltransformieren -> Koppeln -> r�cktransformieren
    D01 = EV'*MDK.D*EV*diag(1./wGes); %hysteretisch
    B01 = EV'*MDK.B*EV;               %viskos
    D_Eq = MDK.M*EV*(B01+D01)*EV'*MDK.M';
    
    
%% anregungsKnoten suchen
    dir = 0;
    node = match(exitationNode,4);

    if strcmp(exitationDirection,'x')
        dir = 0;
    elseif strcmp(exitationDirection,'y')
        dir = 1;
    elseif strcmp(exitationDirection,'z')
        dir = 2;
    else
        error('Anregungsrichtung nicht ermittelbar');
    end
%% ModalTrafo der Matritzen
    if useRed
        T = EVsort(:,1:50);

        KMod = T'*MDK.K*T;
        KMod = diag(diag(KMod));

        DMod = T'*D_Eq*T;

        MMod = eye(size(KMod,1));        

        invM = eye(size(MMod,1));%1\MDK.M;  %%ACHTUNG!!!! macht gravierenden unterschied ob invM mit inv(M) oder 1\M berechnet wird. inv() besser
        len = size(MMod,1);

        AMatRed = [zeros(size(MMod)),eye(size(MMod,1));-invM*KMod, -invM*DMod];

        BMatRed = [zeros(size(T'));invM*T'];
        BMatRed = BMatRed(:,node+dir);

        CMatRed = [T,zeros(size(T))]*1/1000;
        CMatRed = CMatRed(node+dir,:);

        DMatRed = 0;

        sysRed = ss(AMatRed,BMatRed,CMatRed,DMatRed);
    else
        invM = inv(MDK.M);%1\MDK.M;  %%ACHTUNG!!!! macht gravierenden unterschied ob invM mit inv(M) oder 1\M berechnet wird. inv() besser
        len = size(MDK.M,1);

        AMat = [zeros(size(MDK.M)),eye(size(MDK.M));-invM*MDK.K,-invM*D_Eq];
        %BMat = [zeros(len,1);zeros(len,1)];
        BMatGes = [zeros(size(MDK.M));invM];
        CMat = [zeros(1,len),zeros(1,len)];
        DMat = 0;

        %BMat(len + node + dir) = 1;
        BMat = BMatGes(:,node + dir);
        CMat(node + dir) = 1/1000;

        sys = ss(AMat,BMat,CMat,DMat);
    end
%% Frequenzgang berechnen    
    if useRed
        [mag,phase,wout] = bode(sysRed,wMeas);
        mag = squeeze(mag);
        phase = squeeze(phase)/180*pi;

        HsimRed = mag.*exp(1i*phase); % in komplexen Frequenzgang umwandeln
        Hsim = HsimRed;
    else
        [mag,phase,wout] = bode(sys,wMeas);
        mag = squeeze(mag);
        phase = squeeze(phase)/180*pi;

        Hsim = mag.*exp(i*phase); % in komplexen Frequenzgang umwandeln
    end
 
%% plotten
    redString = {'full','red'};
    if nargin == 25
        figure
        p1 = semilogy(wout/2/pi,abs(Hmeas))
        hold on
        p2 = semilogy(wout/2/pi,abs(Hsim))
        xlabel('Frequenz in Hz')
        ylabel('Nachgiebigkeit in m/N')
        title('Vergleich der Nachgiebigkeitsfrequenzg�nge')
        ax = gca;
        for n = 1:length(EVmeas)
            plot([1,1]*EVmeas{n}.freq,get(gca,'yLim'),'k-')
        end
        legend([p1,p2],'Hmeas',['Hsim ',redString{useRed+1}]);
    else
        if isempty(get(figHandle,'Children'))
            ax = subplot(1,1,1);
            semilogy(wout/2/pi,abs(Hmeas),'Parent',ax,'DisplayName','Hmeas');
            set(ax,'NextPlot','add');
        else
            ax = get(figHandle,'Children');
        end
        nameString = sprintf('Hsim (%s), %s, %s: %.3f', redString{useRed+1});
        semilogy(wout/2/pi,abs(Hsim),'Parent',ax,'DisplayName',nameString);
    end
        
end

