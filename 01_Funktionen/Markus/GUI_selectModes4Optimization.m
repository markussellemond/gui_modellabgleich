function [] = GUI_selectModes4Optimization(guiHandle,dataObj)

hFig = 15;
figX = guiHandle.fig.Position(1) + guiHandle.fig.Position(3)+0.1;
figY = guiHandle.fig.Position(2) + guiHandle.fig.Position(4)-hFig;

GUI.fig = figure;
set(GUI.fig,...
        'Units','centimeters',...
        'Position',[figX,figY,6.5,hFig],...
        'Name', 'GUI Modellabgleich',...
        'MenuBar','none',...
        'NumberTitle','off',...
        'CloseRequestFcn',@CB_figCloseRequest)%'WindowStyle','modal',...
        

GUI.UITmodes = uitable;
colWidth = 90;
set(GUI.UITmodes,...
        'Parent',GUI.fig,...
        'Units','centimeters',...
        'ColumnWidth',{colWidth,colWidth},...
        'ColumnEditable',false,...
        'Position',[0,0,6.5,hFig],...
        'CellSelectionCallback',@CB_cellSelect);
    
set(GUI.UITmodes,'ColumnName',{'Mode aus UNV','Verwenden'});  

for n = 1:length(dataObj.UNV.EVmeas)
    GUI.UITmodes.Data{n,1} = sprintf('%6.2f Hz' ,dataObj.UNV.EVmeas{n}.freq);
end

for n = 1:length(dataObj.fit.Objective.modes)
    GUI.UITmodes.Data{dataObj.fit.Objective.modes(n),2} = 'fit';
end

uiwait(guiHandle.fig)

%% Callbacks 
    function CB_cellSelect(scr,eventData)
        if ~isempty(eventData.Indices)
            idx1 = eventData.Indices(1);
            modeN = idx1;
            if ismember(modeN,dataObj.fit.Objective.modes) % Mode ist schon tiel der Moden f�r Optimierung
                dataObj.fit.Objective.modes(dataObj.fit.Objective.modes == modeN) = [];
                GUI.UITmodes.Data{idx1,2} = '';
            else
                dataObj.fit.Objective.modes(end+1) = modeN;
                dataObj.fit.Objective.modes = sort(dataObj.fit.Objective.modes);
                GUI.UITmodes.Data{idx1,2} = 'fit';
            end
        end  
    end
    
    function CB_figCloseRequest(~,~)
        delete(GUI.fig)
        uiresume(guiHandle.fig)
    end
    
end

