function [outputArg1,outputArg2] = F_fitK(MDK,paraOptiMask,para,paraName,ASET,komp,EVmeas,match,options)
%% INTPUTS:
%   MDK: Struct mit Ungekoppelten Massen-, Dämpfungs- und Steifigkeitsmatritzen
%   paraOptiMask: logical-Matrix mit size(para). Parameter mit 1 werden optimiert
%   para:   para-Matrix mit steifigkeits und Dämpfungsparametern
%   paraName: Name, der Elemente in para
%   ASET: Matrix mit den globalen Knotennamen im FEM-Modell (zuordnung der zu koppelnden Freiheitsgrade und Zustände)
%   komp: zu koppelnde Komponenten. (kann später interessant werden)
%   EVmeas: gemessene Eigenvektoren
%   match: match-Matrix die eine Zuordnung von Einträgen in gemessenen (erste Spalte) und simulierten (zweite & dritte Spalte) Eigenvektoren
%   options: Struct mit Optionen



end

