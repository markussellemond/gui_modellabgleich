function [EVsim,T] = F_getEVsim(K,M,nodes)
% EVsim: struct mit Eigenvektoren und Eigenfrequenzen
% T:     Transformationsmatrix f�r Modaltrafo
    [EV,EW]=eig(K,M);
    EV=EV*sqrt(inv(diag(diag(EV'*M*EV))));%massen-normalisieren
    freq=diag(sqrt(abs(EW))./2./pi);
    
    [~,I] = sort(freq);
    freq = freq(I);
    EV = EV(:,I);
    T = EV;
    
    usedNodes = nodes(:,5) ~= 0;  % knoten mit vollwertigen informationen aussuchen

    for m = 1:length(EV)
        EVsim{m}.Nodes = zeros(size(nodes,1),3);
        EVsim{m}.Nodes(usedNodes,1:3) = [EV(nodes(usedNodes,5),m),...
                                         EV(nodes(usedNodes,6),m),...
                                         EV(nodes(usedNodes,7),m)];
        EVsim{m}.EW = EW(m,m);
        EVsim{m}.freq = freq(m);
        EVsim{m}.w = freq(m)*2*pi;
    end
end

