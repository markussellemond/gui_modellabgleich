function [] = F_plotFRF(dataObj)

if ~isempty(dataObj.messdatenX.logStruct.int)   % eine Mewssung suchen, mit vollst�ndigen informationen
    int = dataObj.messdatenX.logStruct.int;
elseif ~isempty(dataObj.messdatenY.logStruct.int)
    int = dataObj.messdatenY.logStruct.int;
elseif ~isempty(dataObj.messdatenZ.logStruct.int)
    int = dataObj.messdatenZ.logStruct.int;
else
    int = 2;
end

%% integration ermitteln und label-Strings definieren
switch int
    case 0
        titleString = 'Akzeleranz';
        yLabelString = 'Akzeleranz in $$\frac{m}{Ns^2}$$';
    case 1
        titleString = 'Mobilitaet';
        yLabelString = 'Mobilitaet in $$\frac{m}{Ns}$$';
    case 2
        titleString = 'Nachgiebigkeit';
        yLabelString = 'Nachgiebigkeit in $$\frac{m}{N}$$';
end


%% figure eretellen
fig = figure;
figPos = [15,10,16,9.5];
set(fig,'WindowStyle','normal')
set(fig,'Units','centimeters')
set(fig,'Position',figPos)
set(fig,'PaperSize', fig.Position(3:4));

% Achsen initialisieren
ax1 = subplot(3,1,1);
ax2 = subplot(3,1,2);
ax3 = subplot(3,1,3);

TitleFontSize = 12;
LabelFontSize = 12;
subplotHeigh = 2.1;
subplotWidth = 13.5;
subplotSpacing = 0.7;
subplotX0 = 2.0;
subplotY0 = 1.2;
set(ax1,'Units','centimeters')
set(ax1,'Position',[subplotX0,subplotY0 + 2*subplotSpacing + 2*subplotHeigh,subplotWidth,subplotHeigh])
set(ax2,'Units','centimeters')
set(ax2,'Position',[subplotX0,subplotY0 + subplotSpacing + subplotHeigh,subplotWidth,subplotHeigh])
set(ax3,'Units','centimeters')
set(ax3,'Position',[subplotX0,subplotY0,subplotWidth,subplotHeigh])

% xRichtung
wMeasX = dataObj.messdatenX.wMeas;
HmeasX = dataObj.messdatenX.hMeas;
if isempty(HmeasX)
    aMinX = 1;
    aMaxX = 1;
    aNormX = 1;
else
    idx10X = find(wMeasX >= 10*2*pi,1,'first');
    aNormX = abs(HmeasX(idx10X));   % faktor f�r normierung bei 10 Hz
    idxMinX = find(wMeasX >= dataObj.messdatenX.logStruct.freqRange(1)*2*pi,1,'first');
    idxMaxX = find(wMeasX >= dataObj.messdatenX.logStruct.freqRange(2)*2*pi,1,'first');
        if isempty(idxMaxX)
            idxMaxX = length(wMeasX);
        end
    HmeasX = HmeasX(idxMinX:idxMaxX);
    wMeasX = wMeasX(idxMinX:idxMaxX);
    aMinX = min(abs(HmeasX));
    aMaxX = max(abs(HmeasX));
end


semilogy(wMeasX/2/pi,abs(HmeasX),'k-','LineWidth',1,'Parent',ax1);
set(ax1,'XLim',dataObj.messdatenX.logStruct.freqRange,'XTick',[50:50:5000],'XTickLabel',{},'XGrid','on','YLim',[aMinX*0.5,aMaxX*2],'YGrid','on','YTick',logspace(-12,10,23),'YGrid','on','YMinorGrid','off','TickLabelInterpreter','latex','FontSize',12);
set(ax1.Title,'String',sprintf('%s in x-Richtung',titleString),'Interpreter','latex','FontSize',TitleFontSize)
set(ax1,'XColor',[0 0 0])
set(ax1,'YColor',[0 0 0])
set(ax1,'LabelFontSizeMultiplier',1)
set(ax1,'TitleFontSizeMultiplier',1)

% yRichtung
wMeasY = dataObj.messdatenY.wMeas;
HmeasY = dataObj.messdatenY.hMeas;
if isempty(HmeasY)
    aMinY = 1;
    aMaxY = 1;
    aNormY = 1;
else
    idx10Y = find(wMeasY >= 10*2*pi,1,'first');
    aNormY = abs(HmeasY(idx10Y));   % faktor f�r normierung bei 10 Hz
    idxMinY = find(wMeasY >= dataObj.messdatenY.logStruct.freqRange(1)*2*pi,1,'first');
    idxMaxY = find(wMeasY >= dataObj.messdatenY.logStruct.freqRange(2)*2*pi,1,'first');
    if isempty(idxMaxY)
        idxMaxY = length(wMeasY);
    end
    HmeasY = HmeasY(idxMinY:idxMaxY);
    wMeasY = wMeasY(idxMinY:idxMaxY);
    aMinY = min(abs(HmeasY));
    aMaxY = max(abs(HmeasY));
end

semilogy(wMeasY/2/pi,abs(HmeasY),'k-','LineWidth',1,'Parent',ax2);
set(ax2,'XLim',dataObj.messdatenY.logStruct.freqRange,'XTick',[50:50:500],'XTickLabel',{},'XGrid','on','YLim',[aMinY*0.5,aMaxY*2],'YGrid','on','YTick',logspace(-12,10,23),'YGrid','on','YMinorGrid','off','TickLabelInterpreter','latex','FontSize',12);
set(ax2.YLabel,'String',sprintf('%s',yLabelString),'Interpreter','latex','FontSize',LabelFontSize)
set(ax2.Title,'String',sprintf('%s in y-Richtung',titleString),'Interpreter','latex','FontSize',TitleFontSize)
set(ax2,'XColor',[0 0 0])
set(ax2,'YColor',[0 0 0])
set(ax2,'LabelFontSizeMultiplier',1)
set(ax2,'TitleFontSizeMultiplier',1)

% zRichtung
wMeasZ = dataObj.messdatenZ.wMeas;
HmeasZ = dataObj.messdatenZ.hMeas;
if isempty(HmeasZ)
    aMinZ = 1;
    aMaxZ = 1;
    aNormZ = 1;
else
    idx10Z = find(wMeasZ >= 10*2*pi,1,'first');
    aNormZ = abs(HmeasZ(idx10Z));   % faktor f�r normierung bei 10 Hz
    idxMinZ = find(wMeasZ >= dataObj.messdatenZ.logStruct.freqRange(1)*2*pi,1,'first');
    idxMaxZ = find(wMeasZ >= dataObj.messdatenZ.logStruct.freqRange(2)*2*pi,1,'first');
        if isempty(idxMaxZ)
            idxMaxZ = length(wMeasZ);
        end
    HmeasZ = HmeasZ(idxMinZ:idxMaxZ);
    wMeasZ = wMeasZ(idxMinZ:idxMaxZ);
    aMinZ = min(abs(HmeasZ));
    aMaxZ = max(abs(HmeasZ));
end

semilogy(wMeasZ/2/pi,abs(HmeasZ),'k-','LineWidth',1,'Parent',ax3);
set(ax3,'XLim',dataObj.messdatenZ.logStruct.freqRange,'XTick',[50:50:5000],'XGrid','on','YLim',[aMinZ*0.5,aMaxZ*2],'YGrid','on','YTick',logspace(-12,10,23),'YGrid','on','YMinorGrid','off','TickLabelInterpreter','latex','FontSize',12);
set(ax3.XLabel,'String','Frequenz in Hz','Interpreter','latex','FontSize',LabelFontSize);
set(ax3.Title,'String',sprintf('%s in z-Richtung',titleString),'Interpreter','latex','FontSize',TitleFontSize)
set(ax3,'XColor',[0 0 0])
set(ax3,'YColor',[0 0 0])
set(ax3,'LabelFontSizeMultiplier',1)
set(ax3,'TitleFontSizeMultiplier',1)

%% normiert
yLabelString(strfind(yLabelString,'in'):end) = [];
yLabelString = [yLabelString,'normiert'];   % vom yLabelString Einheit mit normiert ersetzen

% fiugre erstellen
fig = figure;
figPos = [15,10,16,9.5];
set(fig,'WindowStyle','normal')
set(fig,'Units','centimeters')
set(fig,'Position',figPos)
set(fig,'PaperSize', fig.Position(3:4));

% Axes erstellen
ax1 = subplot(3,1,1);
ax2 = subplot(3,1,2);
ax3 = subplot(3,1,3);

TitleFontSize = 12;
LabelFontSize = 12;
subplotHeigh = 2.1;
subplotWidth = 13.5;
subplotSpacing = 0.7;
subplotX0 = 2.0;
subplotY0 = 1.2;
set(ax1,'Units','centimeters')
set(ax1,'Position',[subplotX0,subplotY0 + 2*subplotSpacing + 2*subplotHeigh,subplotWidth,subplotHeigh])
set(ax2,'Units','centimeters')
set(ax2,'Position',[subplotX0,subplotY0 + subplotSpacing + subplotHeigh,subplotWidth,subplotHeigh])
set(ax3,'Units','centimeters')
set(ax3,'Position',[subplotX0,subplotY0,subplotWidth,subplotHeigh])

% xRichtung
semilogy(wMeasX/2/pi,abs(HmeasX)/aNormX,'k-','LineWidth',1,'Parent',ax1);
set(ax1,'XLim',dataObj.messdatenX.logStruct.freqRange,'XTick',[50:50:5000],'XTickLabel',{},'XGrid','on','YLim',[aMinX*0.5,aMaxX*2]/aNormX,'YGrid','on','YTick',logspace(-12,10,23),'YGrid','on','YMinorGrid','off','TickLabelInterpreter','latex','FontSize',12);
set(ax1.Title,'String',sprintf('%s in x-Richtung',titleString),'Interpreter','latex','FontSize',TitleFontSize)
set(ax1,'XColor',[0 0 0])
set(ax1,'YColor',[0 0 0])
set(ax1,'LabelFontSizeMultiplier',1)
set(ax1,'TitleFontSizeMultiplier',1)

% yRichtung
semilogy(wMeasY/2/pi,abs(HmeasY)/aNormY,'k-','LineWidth',1,'Parent',ax2);
set(ax2,'XLim',dataObj.messdatenY.logStruct.freqRange,'XTick',[50:50:500],'XTickLabel',{},'XGrid','on','YLim',[aMinY*0.5,aMaxY*2]/aNormY,'YGrid','on','YTick',logspace(-12,10,23),'YGrid','on','YMinorGrid','off','TickLabelInterpreter','latex','FontSize',12);
set(ax2.YLabel,'String',sprintf('%s',yLabelString),'Interpreter','latex','FontSize',LabelFontSize)
set(ax2.Title,'String',sprintf('%s in y-Richtung',titleString),'Interpreter','latex','FontSize',TitleFontSize)
set(ax2,'XColor',[0 0 0])
set(ax2,'YColor',[0 0 0])
set(ax2,'LabelFontSizeMultiplier',1)
set(ax2,'TitleFontSizeMultiplier',1)

%zRichtung
semilogy(wMeasZ/2/pi,abs(HmeasZ)/aNormZ,'k-','LineWidth',1,'Parent',ax3);
set(ax3,'XLim',dataObj.messdatenZ.logStruct.freqRange,'XTick',[50:50:5000],'XGrid','on','YLim',[aMinZ*0.5,aMaxZ*2]/aNormZ,'YGrid','on','YTick',logspace(-12,10,23),'YGrid','on','YMinorGrid','off','TickLabelInterpreter','latex','FontSize',12);
set(ax3.XLabel,'String','Frequenz in Hz','Interpreter','latex','FontSize',LabelFontSize);
set(ax3.Title,'String',sprintf('%s in z-Richtung',titleString),'Interpreter','latex','FontSize',TitleFontSize)
set(ax3,'XColor',[0 0 0])
set(ax3,'YColor',[0 0 0])
set(ax3,'LabelFontSizeMultiplier',1)
set(ax3,'TitleFontSizeMultiplier',1)

end

