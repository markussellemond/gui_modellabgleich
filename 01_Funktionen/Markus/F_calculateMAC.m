function [MAC] = F_calculateMAC(Phi1,Phi2)
    if size(Phi1,2) == 3
        Phi1 = [Phi1(:,1); Phi1(:,2); Phi1(:,3)];
    end
    if size(Phi2,2) == 3
        Phi2 = [Phi2(:,1); Phi2(:,2); Phi2(:,3)];
    end
    
    MAC = abs(Phi1'*Phi2)^2/((Phi1'*Phi1)*(Phi2'*Phi2));
end


