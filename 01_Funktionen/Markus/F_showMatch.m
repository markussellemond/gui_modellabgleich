function [] = F_showMatch(dataObj)



    nodesSim = dataObj.simulation.nodesGes;
    nodesMes = dataObj.UNV.nodes;
    match = dataObj.match.match;
    blackList = dataObj.match.blackList;
    
    
    fig = figure;
    set(fig,'Units','centimeters','Position',[0,0,20,20]);
    %% plotten             
    ax1 = gca;
    set(ax1,'nextPlot','add','View',[45 45]);
    
    blColor = [1 0 0];
    matchColor = [0 1 0];
    noMatchColor = [1 1 1]*0.5;
    
    % Knoten der Simulation Plotten
    for n = 1:length(nodesSim)
        if ismember(nodesSim(n,1),match(:,3))   % Knoten geh�rt zu den gematchten Knoten
            plot3(nodesSim(n,2),nodesSim(n,3),nodesSim(n,4),'Marker','o','LineStyle','none','Color',matchColor)
        elseif ismember(nodesSim(n,1),blackList) % Knoten steht auf der Blacklist und darf nicht gematcht werden
            plot3(nodesSim(n,2),nodesSim(n,3),nodesSim(n,4),'Marker','o','LineStyle','none','Color',blColor)
        elseif nodesSim(n,5) ~= 0               % Knoten konnte nicht gematcht werden, weil kein geeigneter partner gefunden wurde (und er ist kein Toter/ unvollst�ndiger Knoten)
            plot3(nodesSim(n,2),nodesSim(n,3),nodesSim(n,4),'Marker','o','LineStyle','none','Color',noMatchColor)
        end            
    end
    
    % Knoten der Messung Plotten
    for n = 1:length(nodesMes)
        if ismember(nodesMes(n,1),match(:,1))   % Knoten geh�rt zu den gematchten Knoten
            plot3(nodesMes(n,2),nodesMes(n,3),nodesMes(n,4),'Marker','*','LineStyle','none','Color',matchColor)
        elseif nodesMes(n,5) ~= 0
            plot3(nodesMes(n,2),nodesMes(n,3),nodesMes(n,4),'Marker','*','LineStyle','none','Color',noMatchColor)
        end            
    end
    
    % Verbildungslinien der Matches plotten
    for n = 1:size(match,1)
        xCoords = [nodesSim(match(n,2),2),nodesMes(match(n,1),2)];
        yCoords = [nodesSim(match(n,2),3),nodesMes(match(n,1),3)];
        zCoords = [nodesSim(match(n,2),4),nodesMes(match(n,1),4)];
        plot3(xCoords,yCoords,zCoords,'r-')
    end

    
    %% Kosy plotten
    xAchse = plot3([0,500],[0,0],[0,0],'g-','DisplayName','x-Achse');
    yAchse = plot3([0,0],[0,500],[0,0],'b-','DisplayName','y-Achse');
    zAchse = plot3([0,0],[0,0],[0,500],'r-','DisplayName','z-Achse');

    xMin = min(nodesMes(:,2))-500;
    xMax = max(nodesMes(:,2))+500;
    
    yMin = min(nodesMes(:,3))-500;
    yMax = max(nodesMes(:,3))+500;
    
    zMin = min(nodesMes(:,4))-500;
    zMax = max(nodesMes(:,4))+500;
    
    axis equal
    set(ax1,'XLim',[xMin,xMax],'YLim',[yMin,yMax],'ZLim',[zMin,zMax])

    %% legende Erzeugen
    p1 = plot3(inf,inf,inf,'Marker','o','LineStyle','none',...
        'Color',matchColor,'Visible','on','DisplayName','Gematchte Knoten');
    p2 = plot3(inf,inf,inf,'Marker','o','LineStyle','none',...
        'Color',noMatchColor,'Visible','on','DisplayName','Nicht Gematchte Knoten');
    p3 = plot3(inf,inf,inf,'Marker','o','LineStyle','none',....
        'Color',blColor,'Visible','on','DisplayName','Knoten der Blacklist');
    p4 = plot3(inf,inf,inf,'Marker','o','LineStyle','none',...
        'Color',[0 0 0],'Visible','on','DisplayName','Knoten des Simulationsmodells');
    p5 = plot3(inf,inf,inf,'Marker','*','LineStyle','none',...
        'Color',[0 0 0],'Visible','on','DisplayName','Knoten des Messmodells');
    
    l = legend([p1,p2,p3,p4,p5,xAchse,yAchse,zAchse]);

end

