function [outputArg1,outputArg2] = F_EVplotUNV(EVmeas,Nodes,components,savePath)
%% INPUT:
%   EV: Struct mit Informationen zu den Eigenvektoren
%   Nodes: Knoten des Messmodells
%   usedNodes: Knoten des Messmodells, die wirklich gemessen wurden
%   savePath:   Pfad, unter dem die gif gespeichert werden soll
    
    if nargin == 3
        savePath = '';
    end
    fileName = [savePath,'\exportedGif.gif'];
    capture = false;
    
    loc = logical(Nodes(:,5));
    
    modeN = 1;
    measEV = EVmeas{modeN}.Nodes(:,2:4);
    
    fig = figure;
    
    eigNr=uicontrol(fig,'Style','edit',...
                    'String','1',...
                    'Units','normalized',...
                    'Position',[.2,.13,.05,.04],...
                    'Callback',@CB_switchEigenmodes);
    eigNrSlider=uicontrol(fig,'Style','slider',...
                    'Units','normalized',...
                    'Position',[.25,.13,.06,.04],...
                    'Min',1,'Max',length(EVmeas),...
                    'SliderStep',[1 1]/(length(EVmeas)-1),...
                    'Value',1,...
                    'Callback',@CB_eigNrSlider);
    multiSlider=uicontrol(fig,'Style','slider',...
                    'Units','normalized',...
                    'Position',[.05,.15,.03,.7],...
                    'Min',0,'Max',1000,...
                    'SliderStep',[1 1]/(100),...
                    'Value',300,...
                    'Callback',@CB_multiSlider);
    eigNrStr=uicontrol(fig,'Style','text',...
                    'String','Mode No:',...
                    'Units','normalized',...
                    'HorizontalAlignment','left',...
                    'Position',[.1,.12,.1,.04]);
                
    linesChkBx = uicontrol(fig,'Style','checkbox',...
                    'Units','normalized',...
                    'Position',[0.25,0.09,0.04,0.04],...
                    'Value',1,...
                    'Callback',@CB_linesChkBx);                
    linesStr=uicontrol(fig,'Style','text',...
                    'String','Lines:',...
                    'Units','normalized',...
                    'HorizontalAlignment','left',...
                    'Position',[0.1,0.08,0.14,0.0476]);
                                     
    nodeNameChkBx = uicontrol(fig,'Style','checkbox',...
                    'Units','normalized',...
                    'Position',[0.25,0.05,0.04,0.04],...
                    'Value',1,...
                    'Callback',@CB_nodeNameChkBx);                
    nodeNameStr=uicontrol(fig,'Style','text',...
                    'String','Node Names:',...
                    'Units','normalized',...
                    'HorizontalAlignment','left',...
                    'Position',[0.1,0.04,0.14,0.0476]);
                                            
    nodeNumberChkBx = uicontrol(fig,'Style','checkbox',...
                    'Units','normalized',...
                    'Position',[0.25,0.01,0.04,0.04],...
                    'Value',0,...
                    'Callback',@CB_nodeNumberChkBx);                
    nodeNumberStr=uicontrol(fig,'Style','text',...
                    'String','Node Numbers:',...
                    'Units','normalized',...
                    'HorizontalAlignment','left',...
                    'Position',[0.1,0.0,0.14,0.0476]);
                                            
    statMdlChkBx = uicontrol(fig,'Style','checkbox',...
                    'Units','normalized',...
                    'Position',[0.5,0.01,0.04,0.04],...
                    'Value',1,...
                    'Callback',@CB_statMdlChkBx);                
    statMdlStr =uicontrol(fig,'Style','text',...
                    'String','Static Model:',...
                    'Units','normalized',...
                    'HorizontalAlignment','left',...
                    'Position',[0.35,0.0,0.14,0.0476]);
                                
    captureGif = uicontrol(fig,'Style','pushbutton',...
                    'Units','normalized',...
                    'Position',[0.9,0.028,0.0476,0.0476],...
                    'Callback',@CB_captureGif);
                
                
    

    maxAmpl = 300; %multiplier f�r die Amplitude der Normierten Eigenvektoren
    multi = sin(linspace(0,2*pi,30)) * maxAmpl/max(max(abs(measEV)));
    n = 1;
    
    %% Knoten nach Substrukturen getrennt Plotten
    compColor = [0 0 0;
                 1 0 0;
                 0 1 0;
                 0 0 1;
                 1 0 1;
                 0 1 1];
    
    for n = 1:length(components)
        locTemp = logical(zeros(size(loc)));
        locTemp(components(n).nodes) = loc(components(n).nodes);
        compPlot(n) =  plot3(Nodes(locTemp,2),...
                             Nodes(locTemp,3),...
                             Nodes(locTemp,4),...
                             'LineStyle','none','Marker','o','Color',compColor(n,:));
        hold on
    end
   
    %% Knotennummern und Knotennamen plotten
    tNumbers = text(Nodes(loc,2),Nodes(loc,3),Nodes(loc,4),num2str(Nodes(loc,1)),'Visible','off'); %Knotennummern wie in Matlab
    tNames = [];
    for n = 1:length(components)
        locTemp = logical(zeros(size(loc)));
        locTemp(components(n).nodes) = loc(components(n).nodes);
        tNames = [tNames;
                  text(Nodes(locTemp,2),Nodes(locTemp,3),Nodes(locTemp,4),...
                    components(n).nodeNames(loc(components(n).nodes))','Color',compColor(n,:))];   %Knotennamen aus LMS 
    end
    
    %% Knoten f�r Eigenvektoren nach Substrukturen getrennt plotten
    for m = 1:length(components)
        locTemp = logical(zeros(size(loc)));
        locTemp(components(n).nodes) = loc(components(n).nodes);
        pEV(m) = plot3(Nodes(locTemp,2),Nodes(locTemp,3),Nodes(locTemp,4),...
                   'LineStyle','none','Marker','o','Color',compColor(m,:));
    end
    
    %% Linien zwischen den Knoten plotten
    compLines = {}; 
    for m = 1:length(components)
        n = 1;
        while n <= length(components(m).lines)
            if components(m).lines(n) ~= 0
                idxStart = n;
                idxEnd = n -2 + find(components(m).lines(n:end) == 0,1,'first');
                n = idxEnd + 2;
                compLines{end+1} = components(m).lines(idxStart:idxEnd);
            else
                n = n+1;
            end
        end
    end
   
    for n = 1:length(compLines)
        locTemp = compLines{n};
        linesStat(n) = plot3(Nodes(compLines{n},2),Nodes(compLines{n},3),Nodes(compLines{n},4),'k-');
        linesEV(n) = plot3(Nodes(compLines{n},2),Nodes(compLines{n},3),Nodes(compLines{n},4),'k-');
    end
    
    %% KoSy plotten
    plot3([0,500],[0,0],[0,0],'g-');
    plot3([0,0],[0,500],[0,0],'b-');
    plot3([0,0],[0,0],[0,500],'r-');

    %% axis initialisieren
    ax = gca;
    axis equal
    set(ax,'XLim',[min(Nodes(:,2))-500,max(Nodes(:,2))+500],...
           'YLim',[min(Nodes(:,3))-500,max(Nodes(:,3))+500],...
           'ZLim',[min(Nodes(:,4))-500,max(Nodes(:,4))+500])
    set(ax.Title,'String',EVmeas{modeN}.ID,'Interpreter','latex')

    %% schleife f�r animation
    while ishandle(fig)
        for k = 1:length(components)
            locTemp = logical(zeros(size(loc)));
            locTemp(components(k).nodes) = loc(components(k).nodes);
            pEV(k).XData = Nodes(locTemp,2) + measEV(locTemp,1)*multi(n);
            pEV(k).YData = Nodes(locTemp,3) + measEV(locTemp,2)*multi(n);
            pEV(k).ZData = Nodes(locTemp,4) + measEV(locTemp,3)*multi(n);
        end
        for k = 1:length(compLines)
            locTemp = compLines{k};           
            linesEV(k).XData = Nodes(locTemp,2) + measEV(locTemp,1)*multi(n);
            linesEV(k).YData = Nodes(locTemp,3) + measEV(locTemp,2)*multi(n);
            linesEV(k).ZData = Nodes(locTemp,4) + measEV(locTemp,3)*multi(n);
        end
        
        if capture
            % Capture the plot as an image 
            frame = getframe(fig); 
            im = frame2im(frame); 
            [imind,cm] = rgb2ind(im,256); 
            if n == 1 
                imwrite(imind,cm,fileName,'gif','DelayTime',0.1,'Loopcount',inf); 
            else 
                imwrite(imind,cm,fileName,'gif','DelayTime',0.1,'WriteMode','append'); 
            end
            if n == length(multi)-1             
                capture = false;
            end
        end
        
        if n == length(multi)
            n = 1;
        end
        n = n+1;
        drawnow
        pause(0.1)
    end
    
    %% Callbacks
    function [] = CB_switchEigenmodes(scr,event)
        modeN = floor(str2num(get(scr,'String')));
        set(eigNrSlider,'Value',modeN);
        updateMode();
    end

    function [] = CB_eigNrSlider(scr,event)
        set(eigNr,'String',num2str(floor(get(scr,'Value'))));
        modeN = floor(get(scr,'Value'));
        updateMode();
    end

    function updateMode()
        measEV  = EVmeas{modeN}.Nodes(:,2:4);
        measEV  = measEV/norm(measEV);
        multi = sin(linspace(0,2*pi,30)) * maxAmpl/max(max(measEV));
        set(ax.Title,'String',EVmeas{modeN}.ID,'Interpreter','latex');       
    end

    function [] = CB_multiSlider(scr,event)
        maxAmpl = get(scr,'Value');
        multi = sin(linspace(0,2*pi,30)) * maxAmpl/max(max(abs(measEV)));
    end

    function [] = CB_linesChkBx(scr,event)
        printLines = get(scr,'Value');
        for m = 1:length(compLines)
            if printLines
                set(linesEV(m),'Visible','on');
                if get(statMdlChkBx,'Value')
                    set(linesStat(m),'Visible','on');
                end
            else
                set(linesEV(m),'Visible','off');
                set(linesStat(m),'Visible','off')
            end
        end
    end

    function CB_nodeNameChkBx(scr,event)
        printNodeNames = get(scr,'Value');
        set(nodeNumberChkBx,'Value',0);
        for m = 1:length(tNames)
            if printNodeNames
                set(tNames(m),'Visible','on')
                set(tNumbers(m),'Visible','off')
            else
                set(tNames(m),'Visible','off')
            end 
        end
    end

    function [] = CB_nodeNumberChkBx(scr,event)
        printNodeNumbers = get(scr,'Value');
        set(nodeNameChkBx,'Value',0);                
        for m = 1:length(tNumbers)
            if printNodeNumbers
                set(tNumbers(m),'Visible','on')
                set(tNames(m),'Visible','off')
            else
                set(tNumbers(m),'Visible','off')
            end 
        end
    end

    function CB_statMdlChkBx(scr,event)
        showStatMdl = get(scr,'Value');
        if showStatMdl
            for m = 1:length(compPlot)
                set(compPlot(m),'Visible','on')
            end
            if get(linesChkBx,'Value')
                for m = 1:length(linesStat)
                    set(linesStat(m),'Visible','on')
                end
            end
        else
            for m = 1:length(compPlot)
                set(compPlot(m),'Visible','off')
            end
            for m = 1:length(linesStat)
                set(linesStat(m),'Visible','off')
            end
        end
    end

    function CB_captureGif(scr,event)
        capture = true;
        n = 1;
    end

end



