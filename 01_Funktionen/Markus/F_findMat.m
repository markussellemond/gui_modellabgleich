function [idxCol,idxRow] = F_findMat(A,x,n)
    if nargin == 2
        n = 1;
    end
    idx = find(A == x);
    idx = idx(1:min(n,length(idx)));
    idxCol = floor((idx-1)/size(A,1))+1; % spaltenindex
    idxRow = idx - (idxCol-1) * size(A,1); % Zeilenindex
end

