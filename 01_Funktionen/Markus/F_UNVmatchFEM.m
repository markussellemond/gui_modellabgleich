function [match] = F_UNVmatchFEM(nodesMeas,nodesSim,A,matchBlackList,maxDist)
%match setzt sich zusammen aus: [index des Knotens in nodesMeas
%                                index des Knotens in nodesSim
%                                Knotennummer in Hypermesh
%                                index der ersten Richtung in den CB-Reduzierten FEM Matritzen
    if isempty(maxDist)
        maxDist = 200;
    end

    match = [];
    
    locMeas = find(nodesMeas(:,5) ~= 0);    % Verwertbare Knoten in der Messung
    locSim= find(nodesSim(:,5) ~= 0);       % Verwertbare Knoten in der Simulation
    
    for n = 1:length(locSim)
        if ismember(nodesSim(locSim(n),1),matchBlackList)
            locSim(n) = 0;
        end
    end
    locSim(locSim==0) = [];
    
%% Abstand zwischen Knoten berechnen    
    dist = zeros(length(locSim),length(locMeas));
    for m = 1:length(locSim)
        for n = 1:length(locMeas)
            dist(m,n) = norm(nodesMeas(locMeas(n),2:4) - nodesSim(locSim(m),2:4));
        end
    end
    maxmax = max(max(dist))*10; % Penalty: wird ben�tigt, um Zeilen zu "l�schen"
 
%% knoten mit geringsten abst�nden matchen
    n = 1;
    for m =1:length(locMeas) % aus der dist matrix jene punkte mit dem geringsten Abstand zueinander finden
        [idxCol,idxRow] = F_findMat(dist,min(min(dist)));
        if dist(idxRow,idxCol) > maxDist
            fprintf('ACHTUNG! Kleinster Abstand zwischen zwei Punkten: %8.0f. Punkte werden ignoriert.\n',dist(idxRow,idxCol))
        else
            match(n,:) = [locMeas(idxCol),locSim(idxRow),nodesSim(locSim(idxRow),1)]; %nur knoten die nicht mehr als 200mm abstand haben werden gematcht
            n = n+1;
        end
        dist(idxRow,:) = maxmax;
        dist(:,idxCol) = maxmax;
    end

%% Match-Matrix schreiben
    match(:,4) = zeros(size(match,1),1);
    for n = 1:size(match,1)
        match(n,4) = find(A == match(n,3)*10+1,1);      % index der ersten zum knoten geh�renden Richtung im Zustandsvektor
    end

    if nargin > 3
        match(ismember(match(:,3),matchBlackList),:) = [];
    end
end










