function [] = F_generateZRM(dataObj,projectRoot)
A = dataObj.simulation.A;
input = dataObj.ZRM.inpNodes;
output = dataObj.ZRM.outpNodes;
normalModes = dataObj.simulation.normalModes;
%% Sortieren        
    [~,d]= ismember(A,input); %Suche Koppelpunkte in input
    [~,v]= ismember(A.*10+1,output);%Geschwindigkeit
    [~,x]= ismember(A.*10+2,output);%Position

% Gefundene Punkte bleiben
    [dd,~]= ismember(input,A); %Suche Koppelpunkte in input     
    [vv,~]= ismember(output,A.*10+1); %Geschwindigkeit
    [xx,~]= ismember(output,A.*10+2); %Position
    input=input(dd);
    gg=vv+xx;
    output=output(logical(gg));
        
%Erstelle Sortiermatrix
con = length(A);
s=zeros(con,2);
g=zeros(2*con,2);
for i=1:con 
    s(i,1)=i;
    s(i,2)=d(i);
    g(i,1)=i;
    g(i,2)=v(i);
    g(i+con,1)=i+con+normalModes.ges;
    g(i+con,2)=x(i);
end

%% Modaltransforamtion
MGes = dataObj.simulation.MDKgekoppelt.M;
KGes = dataObj.simulation.MDKgekoppelt.K;
DGesEq = dataObj.simulation.MDKgekoppelt.DGesEQ;
T = dataObj.simulation.T;

[Mmod, Kmod, Dmod, G, S]=sort4ss(MGes, KGes, DGesEq, T, [], s, g);

 
%Modale Matrizen sortieren
fMod=diag(sqrt(Mmod\Kmod)./(2*pi));
[~,J]=sort(diag(Mmod\Kmod));
T=T(:,J);
Kmod=Kmod(J,J);
Dmod=Dmod(J,J);
 
% Modale D�mpfung einf�gen f�r Ausgleichsmoden
% D_mod=diag(diag(D_mod))%Diagonalisierung D
% D_mod=zeros(size(D_mod))

Dmod=Mmod\Dmod;
    for i=(normalModes.ges+1):length(Dmod)
        Dmod(i,i)=(2*(2*pi*fMod(i))*0.1);    
    end
Dmod=Mmod*Dmod ;

% State Space Modell erzeugen
modalTrans = 1;
[As, Bs, Cs, Ds]=F_ZRM_Gen(Mmod, Kmod, Dmod, [], T, S, G, modalTrans);  % T: sortierungsmatrix, sortiert nach Eigenfrequenzen aufw�rts
in.String=num2str(input);
in.Value=1;
out.String=num2str(output);
out.Value=1;
        
%Zustandsraummodell bilden
sys_CB=ss(As, Bs, Cs, Ds, 'InputName', in.String, 'OutputName', out.String);

%% speichern

    [fileName,path] = uiputfile({'*.mat','mat-File (*.mat)';'*,*','All Files (*.*)'},'ZRM speichern',[projectRoot,'\']);
    if isempty(strfind(fileName,'_zrm.')) % dateiNamenendung nur hinzuf�gen, wenn noch nicht vorhanden
        idx = strfind(fileName,'.');
        fileName = [fileName(1:idx-1),'_zrm',fileName(idx:end)];
    end
    save([path,fileName],'sys_CB');
end

