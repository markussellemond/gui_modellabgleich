function [] = F_EVplotFEM(EVsim,Nodes,components,savePath)
%% INPUT:
%   EVsim: Eigenvektor der Simulation
%   Nodes:  Koordinatenvektor der Knoten
%   components: Komponenteninformationen
%   savePath:   Pfad unter dem die gif gespeichert werden soll

    if nargin == 3
        savePath = '';
    end
    fileName = [savePath,'\exportedGif.gif'];
    capture = false;

    loc = Nodes(:,5) ~= 0;
    NodesTemp = Nodes;
    Nodes = Nodes(loc,:);
    %nodes_ges = nodes_ges(nodes2use,:);

    modeN = 1;
    simEV  = EVsim{1}.Nodes(loc,1:3);
    simEV  = simEV/norm(simEV);

    fig = figure;
    
    eigNr=uicontrol(fig,'Style','edit',...
                    'String','1',...
                    'Units','normalized',...
                    'Position',[.2,.13,.05,.04],...
                    'Callback',@CB_switchEigenmodes);
    eigNrSlider=uicontrol(fig,'Style','slider',...
                    'Units','normalized',...
                    'Position',[.25,.13,.06,.04],...
                    'Min',1,'Max',length(EVsim),...
                    'SliderStep',[1 1]/(length(EVsim)-1),...
                    'Value',1,...
                    'Callback',@CB_eigNrSlider);
    multiSlider=uicontrol(fig,'Style','slider',...
                    'Units','normalized',...
                    'Position',[.05,.15,.03,.7],...
                    'Min',0,'Max',1000,...
                    'SliderStep',[1 1]/(100),...
                    'Value',300,...
                    'Callback',@CB_multiSlider);
    eigNrStr=uicontrol(fig,'Style','text',...
                    'String','Mode No:',...
                    'Units','normalized',...
                    'Position',[.1,.12,.1,.04]);
                
    captureGif = uicontrol(fig,'Style','pushbutton',...
                    'Units','normalized',...
                    'Position',[0.9,0.028,0.0476,0.0476],...
                    'Callback',@CB_captureGif);
                
    nodeNameChkBx = uicontrol(fig,'Style','checkbox',...
                    'Units','normalized',...
                    'Position',[0.25,0.05,0.04,0.04],...
                    'Value',1,...
                    'Callback',@CB_nodeNameChkBx);                
    nodeNameStr=uicontrol(fig,'Style','text',...
                    'String','Node Names:',...
                    'Units','normalized',...
                    'HorizontalAlignment','left',...
                    'Position',[0.1,0.04,0.14,0.0476]);
    nodeNumberChkBx = uicontrol(fig,'Style','checkbox',...
                    'Units','normalized',...
                    'Position',[0.25,0.01,0.04,0.04],...
                    'Value',0,...
                    'Callback',@CB_nodeNumberChkBx);                
    nodeNumberStr=uicontrol(fig,'Style','text',...
                    'String','Node Numbers:',...
                    'Units','normalized',...
                    'HorizontalAlignment','left',...
                    'Position',[0.1,0.0,0.14,0.0476]);
                                            
    statMdlChkBx = uicontrol(fig,'Style','checkbox',...
                    'Units','normalized',...
                    'Position',[0.5,0.01,0.04,0.04],...
                    'Value',1,...
                    'Callback',@CB_statMdlChkBx);                
    statMdlStr =uicontrol(fig,'Style','text',...
                    'String','Static Model:',...
                    'Units','normalized',...
                    'HorizontalAlignment','left',...
                    'Position',[0.35,0.0,0.14,0.0476]);
                
    maxAmpl = 300; %multiplier f�r die Amplitude der Normierten Eigenvektoren
    multi = sin(linspace(0,2*pi,30)+0.001) * maxAmpl/max(max(simEV));
    n = 1;            
    
    %% Knoten der einzelnen Komponenten Ermitteln
    compLoc = cell(1,1);
    for n = 1:length(components)
        compLoc{n} = ismember(Nodes(:,1),components(n).nodes(:,1));
    end
    
    %% plotten             
    ax1 = gca;
    
    compColor = [0 0 0;
                 1 0 0;
                 0 1 0;
                 0 0 1;
                 1 0 1;
                 0 1 1];
    for n = 1:length(compLoc)   
        %statischer Plot
        compPlot(n) = plot3(Nodes(compLoc{n},2),Nodes(compLoc{n},3),Nodes(compLoc{n},4),...
                        'LineStyle','none','Marker','o','Color',compColor(n,:));
        set(ax1,'NextPlot','add');
        
        % animierte Eigenform
        pSim(n) = plot3(Nodes(compLoc{n},2) + multi(1)*simEV(compLoc{n},1),...
                        Nodes(compLoc{n},3) + multi(1)*simEV(compLoc{n},2),...
                        Nodes(compLoc{n},4) + multi(1)*simEV(compLoc{n},3),...
                            'LineStyle','none','Marker','o','Color',compColor(n,:));
    end
    
    %% Knotennummern und Knotenindices plotten
    tNumbers = text(Nodes(:,2),Nodes(:,3),Nodes(:,4),num2str((1:size(Nodes,1))'),'Visible','off'); %Knotennummern wie in Matlab
    tNames = [];
    for n = 1:length(components)
        tNames = [tNames;...
                  text(Nodes(compLoc{n},2),Nodes(compLoc{n},3),Nodes(compLoc{n},4),...
                       num2str(Nodes(compLoc{n},1)),'Color',compColor(n,:))];   %Knotennamen aus LMS 
    end
    
    %% Kosy plotten
    plot3([0,500],[0,0],[0,0],'g-')
    plot3([0,0],[0,500],[0,0],'b-')
    plot3([0,0],[0,0],[0,500],'r-')


%     pMeas = plot3(Nodes(usedNodes(match(:,1)),2) + multi(1)*measEV(:,1),...
%                   Nodes(usedNodes(match(:,1)),3) + multi(1)*measEV(:,1),...
%                   Nodes(usedNodes(match(:,1)),4) + multi(1)*measEV(:,1),'ro');

    xMin = min(Nodes(:,2))-500;
    xMax = max(Nodes(:,2))+500;
    
    yMin = min(Nodes(:,3))-500;
    yMax = max(Nodes(:,3))+500;
    
    zMin = min(Nodes(:,4))-500;
    zMax = max(Nodes(:,4))+500;
    
    axis equal
    set(ax1,'XLim',[xMin,xMax],'YLim',[yMin,yMax],'ZLim',[zMin,zMax])
    set(ax1.Title,'String',sprintf('Mode No.: %.i, Frequenz: %.3f',...
        modeN,EVsim{modeN}.freq),...
        'Interpreter','latex');
    %legend('unverformt sim','unverformt meas','x','y','z','EVsim','EVmeas')

    % animieren
    n = 1;
    while ishandle(ax1)
        for m = 1:length(components)
            pSim(m).XData = Nodes(compLoc{m},2) + multi(n)*simEV(compLoc{m},1);
            pSim(m).YData = Nodes(compLoc{m},3) + multi(n)*simEV(compLoc{m},2);
            pSim(m).ZData = Nodes(compLoc{m},4) + multi(n)*simEV(compLoc{m},3);
        end
        
        if capture
            % Capture the plot as an image 
            frame = getframe(fig); 
            im = frame2im(frame); 
            [imind,cm] = rgb2ind(im,256); 
            if n == 1 
                imwrite(imind,cm,fileName,'gif','DelayTime',0.1, 'Loopcount',inf); 
            else 
                imwrite(imind,cm,fileName,'gif','DelayTime',0.1,'WriteMode','append'); 
            end
            if n == length(multi)-1             
                capture = false;
            end
        end

        if n == length(multi)
            n = 1;
        end
        n = n+1;
        pause(0.1)
        drawnow   
    end
    
    
%% Callbacks    
    function [] = CB_switchEigenmodes(scr,event)
        modeN = floor(str2num(get(scr,'String')));
        set(eigNrSlider,'Value',modeN);
        updateMode();
    end

    function [] = CB_eigNrSlider(scr,event)
        set(eigNr,'String',num2str(floor(get(scr,'Value'))));
        modeN = floor(get(scr,'Value'));
        updateMode();
    end
    
    function updateMode()
        simEV  = EVsim{modeN}.Nodes(loc,1:3);
        simEV  = simEV/norm(simEV);
        multi = sin(linspace(0,2*pi,30)) * maxAmpl/max(max(abs(simEV)));
        set(ax1.Title,'String',sprintf('Mode No.: %.i, Frequenz: %.3f',...
        modeN,EVsim{modeN}.freq),...
        'Interpreter','latex');       
    end

    function [] = CB_multiSlider(scr,event)
        maxAmpl = get(scr,'Value');
        simEV  = EVsim{modeN}.Nodes(loc,1:3);
        simEV  = simEV/norm(simEV);
        multi = sin(linspace(0,2*pi,30)) * maxAmpl/max(max(abs(simEV)));
    end

    function CB_captureGif(scr,event)
        capture = true;
        n = 1;
    end

    function CB_nodeNameChkBx(scr,event)
        printNodeNames = get(scr,'Value');
        set(nodeNumberChkBx,'Value',0);
        for m = 1:length(tNames)
            if printNodeNames
                set(tNames(m),'Visible','on')
                set(tNumbers(m),'Visible','off')
            else
                set(tNames(m),'Visible','off')
            end 
        end
    end

    function [] = CB_nodeNumberChkBx(scr,event)
        printNodeNumbers = get(scr,'Value');
        set(nodeNameChkBx,'Value',0);                
        for m = 1:length(tNumbers)
            if printNodeNumbers
                set(tNumbers(m),'Visible','on')
                set(tNames(m),'Visible','off')
            else
                set(tNumbers(m),'Visible','off')
            end 
        end
    end

    function CB_statMdlChkBx(scr,event)
        showStatMdl = get(scr,'Value');
        if showStatMdl
            for m = 1:length(compPlot)
                set(compPlot(m),'Visible','on')
            end
        else
            for m = 1:length(compPlot)
                set(compPlot(m),'Visible','off')
            end
        end
    end
        
end

