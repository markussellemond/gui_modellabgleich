function [fVal,fValDeriv] = F_objktivK_deriv(kOpt,MDKungekoppelt,komp,ASET,...
    para,paraName,paraNode,paraPrototype,EVmeas,match,options)

    fVal = F_objektivK3(kOpt,MDKungekoppelt,komp,ASET,...
    para,paraName,paraNode,paraPrototype,EVmeas,match,options);

    kOpt2 = kOpt*1.02;
    fVal2 = F_objektivK3(kOpt2,MDKungekoppelt,komp,ASET,...
    para,paraName,paraNode,paraPrototype,EVmeas,match,options);

    fValDeriv = (fVal2-fVal)./(kOpt2-kOpt); % differenzenquotient


end

