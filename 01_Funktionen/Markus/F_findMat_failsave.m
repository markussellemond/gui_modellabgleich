function [idxCol,idxRow] = F_findMat(A,x)
    idx = find(A == x,1);
    idxCol = floor((idx-1)/size(A,1))+1; % spaltenindex
    idxRow = idx - (idxCol-1) * size(A,1); % Zeilenindex
end

