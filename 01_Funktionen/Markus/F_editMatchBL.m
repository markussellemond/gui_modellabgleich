function [] = F_editMatchBL(guiHandle,dataObj)

GUI.fig = figure;
set(GUI.fig,...
        'Units','centimeters',...
        'Position',[10,10,10,18],...
        'Name', 'Match Blacklist',...
        'MenuBar','none',...
        'NumberTitle','off',...
        'CloseRequestFcn',@CB_figCloseRequest);
%         'WindowStyle','modal',...

GUI.UITmatchBL = uitable;
set(GUI.UITmatchBL,...
        'Parent',GUI.fig,...
        'Units','centimeters',...
        'ColumnWidth',{100,250},...
        'ColumnEditable',[true,false],...
        'ColumnName',{'Knotennummer','Beschreibung'},...
        'ColumnFormat',{'numeric','char'},...
        'Data',{[],[]},...
        'Position',[0,0,10,18],...
        'CellEditCallback',@CB_cellEdit);

if ~isempty(dataObj.match.blackList)
    for m = 1:size(dataObj.match.blackList,1)
        compID = F_getComp(dataObj.match.blackList(m));
        GUI.UITmatchBL.Data{m,1} = dataObj.match.blackList(m);
        GUI.UITmatchBL.Data{m,2} = compID;   
    end
    GUI.UITmatchBL.Data{m+1,1} = [];
    GUI.UITmatchBL.Data{m+1,2} = [];   
end

uiwait(guiHandle.fig)

%% Callbacks 
    function CB_cellEdit(scr,eventData)
        if eventData.Indices(1) > size(dataObj.match.blackList,1)   % es wurde ein neuer Knoten Hinzugefügt
            if ismember(eventData.NewData,dataObj.match.blackList)
                msgbox('Knoten Schon vorhanden')
                scr.Data{eventData.Indices(1),1} = [];
            else
                compID = F_getComp(eventData.NewData);
                scr.Data{eventData.Indices(1),2} = compID;
                dataObj.match.blackList(end+1,1) = eventData.NewData;
                scr.Data{end+1,1} = [];
            end                
        else    % ein bestehender Knoten Wird bearbeitet
            if ~isnan(eventData.NewData)
                if ismember(eventData.NewData,dataObj.match.blackList)
                    msgbox('Knoten Schon vorhanden')  
                    scr.Data{eventData.Indices(1),1} = eventData.PreviousData;
                else
                    dataObj.match.blackList(eventData.Indices(1)) = eventData.NewData;
                    compID = F_getComp(eventData.NewData);
                    scr.Data{eventData.Indices(1),2} = compID;
                end
            else    % Feld wurde Gelöscht
                dataObj.match.blackList(eventData.Indices(1)) = [];
                if isempty(dataObj.match.blackList)
                    dataObj.match.blackList = [];
                end
                scr.Data(eventData.Indices(1),:) = [];
            end
        end
    end
    
    function CB_figCloseRequest(~,~)
        delete(GUI.fig)
        uiresume(guiHandle.fig)
    end

%% andere von den Callbacks aufgerufene Funktionen
    function [compID] = F_getComp(nodeNr)
        components = dataObj.simulation.nodesKomp;
        nodeIdx = [];
        compID = [];
        for n = 1:length(components)
            nodeIdx = find(components(n).nodes(:,1) == nodeNr,1,'first');
            if ~isempty(nodeIdx)
                compID = components(n).ID;
%                 nodeCoord = components(n).nodes(nodeIdx,2:4);  
                if ~isempty(F_findMat(dataObj.para.paraNode,nodeNr))
                    [idx2,idx1] = F_findMat(dataObj.para.paraNode,nodeNr);
                    compID = [compID,' : ',dataObj.para.paraName{idx1}];
                end             
            end
        end
    end
    
end

