function [] = F_UNVvsFEM(EVmeas,nodesMeas,EVsim,nodesSim,match,makeGif)

    if nargin == 5
        makeGif = false;
    else
        fileName = 'exportedGif.gif';
        capture = false;
    end
    
    fig = figure;
    plotedit off
    
    eigNrSim=uicontrol(fig,'Style','edit',...
                    'String','1',...
                    'Units','normalized',...
                    'Position',[.22,.03,.05,.04],...
                    'Callback',@CB_switchEigenmodesSim);
    eigNrSliderSim=uicontrol(fig,'Style','slider',...
                    'Units','normalized',...
                    'Position',[.27,.03,.06,.04],...
                    'Min',1,'Max',length(EVsim),...
                    'SliderStep',[1 1]/(length(EVsim)-1),...
                    'Value',1,...
                    'Callback',@CB_eigNrSliderSim);
    eigNrStrSim=uicontrol(fig,'Style','text',...
                    'String','Mode No. Simulation:',...
                    'Units','normalized',...
                    'Position',[.01,.03,.2,.04]);
                
    eigNrMeas=uicontrol(fig,'Style','edit',...
                    'String','1',...
                    'Units','normalized',...
                    'Position',[.22,.08,.05,.04],...
                    'Callback',@CB_switchEigenmodesMeas);
    eigNrSliderMeas=uicontrol(fig,'Style','slider',...
                    'Units','normalized',...
                    'Position',[.27,.08,.06,.04],...
                    'Min',1,'Max',length(EVmeas),...
                    'SliderStep',[1 1]/(length(EVmeas)-1),...
                    'Value',1,...
                    'Callback',@CB_eigNrSliderMeas);
    eigNrStrMeas=uicontrol(fig,'Style','text',...
                    'String','Mode No. Messung:',...
                    'Units','normalized',...
                    'Position',[.01,.08,.2,.04]);
                
    multiSlider=uicontrol(fig,'Style','slider',...
                    'Units','normalized',...
                    'Position',[.05,.15,.03,.7],...
                    'Min',0,'Max',1000,...
                    'SliderStep',[1 1]/(100),...
                    'Value',300,...
                    'Callback',@CB_multiSlider);
    
    measChkBx = uicontrol(fig,'Style','checkbox',...
                    'Units','normalized',...
                    'Position',[0.33,0.078,0.1071,0.0476],...
                    'Value',1,...
                    'Callback',@CB_measChkBx);
                
    simChkBx = uicontrol(fig,'Style','checkbox',...
                    'Units','normalized',...
                    'Position',[0.33,0.028,0.1071,0.0476],...
                    'Value',1,...
                    'Callback',@CB_simChkBx);
    captureGif = uicontrol(fig,'Style','pushbutton',...
                    'Units','normalized',...
                    'Position',[0.9,0.028,0.0476,0.0476],...
                    'Callback',@CB_captureGif);
                
    plot3([0,500],[0,0],[0,0],'g-')
    hold on
    plot3([0,0],[0,500],[0,0],'b-')
    plot3([0,0],[0,0],[0,500],'r-')
    
    maxAmpl = 300; %multiplier f�r die Amplitude der Normierten Eigenvektoren
%% Messung
    modeNmeas = 1;
    
    locMeas =zeros(size(nodesMeas(:,5)));
    locMeas(match(:,1)) = 1;
    locMeas= logical(locMeas);
    if sum(locMeas(match(:,1))) ~= length(match(:,1))
        error('nachschauen ob Knoten konsistent')
    end
    
    measEV = EVmeas{modeNmeas}.Nodes(match(:,1),2:4);
    
    multiMeas = sin(linspace(0,2*pi,30)) * maxAmpl/max(max(abs(measEV)));
    
    plot3(nodesMeas(match(:,1),2),nodesMeas(match(:,1),3),nodesMeas(match(:,1),4),'ko')
    pMeas = plot3(nodesMeas(match(:,1),2),nodesMeas(match(:,1),3),nodesMeas(match(:,1),4),'ro');
    
    
%% Simulation    
    modeNsim = 1;
    
    locSim = zeros(size(nodesSim(:,5)));
    locSim(match(:,2)) = 1;
    locSim = logical(locSim);
    if sum(locSim(match(:,2))) ~= length(match(:,2))
        error('nachschauen ob Knoten konsistent')
    end
    
    simEV = EVsim{modeNsim}.Nodes(match(:,2),1:3);
                
    multiSim = sin(linspace(0,2*pi,30)) * maxAmpl/max(max(abs(simEV)));
    
    plot3(nodesSim(match(:,2),2),nodesSim(match(:,2),3),nodesSim(match(:,2),4),'bo')
    pSim = plot3(nodesSim(match(:,2),2),nodesSim(match(:,2),3),nodesSim(match(:,2),4),'go');
                
                
%% AX bearbeiten
    ax = gca;
    
    xMin = min(nodesMeas(:,2))-500;
    xMax = max(nodesMeas(:,2))+500;
    
    yMin = min(nodesMeas(:,3))-500;
    yMax = max(nodesMeas(:,3))+500;
    
    zMin = min(nodesMeas(:,4))-500;
    zMax = max(nodesMeas(:,4))+500;
    
    axis equal
    set(ax,'XLim',[xMin,xMax],'YLim',[yMin,yMax],'ZLim',[zMin,zMax])
    set(ax.Title,'String',sprintf('$$f_{meas (%i)} = %.2f ; f_{sim (%i)} = %.2f ; MAC: %.3f$$',...
                modeNmeas,EVmeas{modeNmeas}.freq,modeNsim,EVsim{modeNsim}.freq,...
                F_calculateMAC([simEV(:,1);simEV(:,2);simEV(:,3)],[measEV(:,1);measEV(:,2);measEV(:,3)])),'Interpreter','latex');
    n = 1; 
    plotedit off
    while ishandle(fig)
        pSim.XData = nodesSim(match(:,2),2) + simEV(:,1)*multiSim(n);
        pSim.YData = nodesSim(match(:,2),3) + simEV(:,2)*multiSim(n);
        pSim.ZData = nodesSim(match(:,2),4) + simEV(:,3)*multiSim(n);
        
        pMeas.XData = nodesMeas(match(:,1),2) + measEV(:,1)*multiMeas(n);
        pMeas.YData = nodesMeas(match(:,1),3) + measEV(:,2)*multiMeas(n);
        pMeas.ZData = nodesMeas(match(:,1),4) + measEV(:,3)*multiMeas(n);
        
        if makeGif & capture
            % Capture the plot as an image 
            frame = getframe(fig); 
            im = frame2im(frame); 
            [imind,cm] = rgb2ind(im,256); 
            if n == 1 
                imwrite(imind,cm,fileName,'gif','DelayTime',0.1,'Loopcount',inf); 
            else 
                imwrite(imind,cm,fileName,'gif','DelayTime',0.1,'WriteMode','append'); 
            end
            if n == length(multiMeas)-1             
                capture = false;
            end
        end
        
        if n == length(multiMeas)
            n = 1;
        end
        n = n+1;
        pause(0.1)
        drawnow        
    end
                
    function CB_switchEigenmodesSim(scr,event)
        modeNsim = floor(str2num(get(scr,'String')));
        set(eigNrSliderSim,'Value',modeNsim);
        updateMode();
    end

    function CB_switchEigenmodesMeas(scr,event)
        modeNmeas = floor(str2num(get(scr,'String')));
        set(eigNrSliderMeas,'Value',modeNmeas);
        updateMode();
    end

    function CB_eigNrSliderSim(scr,event)
        set(eigNrSim,'String',num2str(floor(get(scr,'Value'))));
        modeNsim = floor(get(scr,'Value'));
        updateMode();
    end

    function CB_eigNrSliderMeas(scr,event)
        set(eigNrMeas,'String',num2str(floor(get(scr,'Value'))));
        modeNmeas = floor(get(scr,'Value'));
        updateMode();
    end

    function updateMode()
        simEV  = EVsim{modeNsim}.Nodes(match(:,2),1:3);
        simEV  = simEV/norm(simEV);
        multiSim = sin(linspace(0,2*pi,30)) * maxAmpl/max(max(abs(simEV)));
        
        measEV  = EVmeas{modeNmeas}.Nodes(match(:,1),2:4);
        measEV  = measEV/norm(measEV);
        if sum(sum(simEV.*measEV)) < 0
            measEV = -measEV;
        end
        multiMeas = sin(linspace(0,2*pi,30)) * maxAmpl/max(max(abs(measEV)));
              
        set(ax.Title,'String',sprintf('$$f_{meas (%i)} = %.1f ; f_{sim (%i)} = %.1f ; MAC: %.3f$$',...
                modeNmeas,EVmeas{modeNmeas}.freq,modeNsim,EVsim{modeNsim}.freq,...
                F_calculateMAC([simEV(:,1);simEV(:,2);simEV(:,3)],[measEV(:,1);measEV(:,2);measEV(:,3)])),'Interpreter','latex');
      
    end

    function CB_multiSlider(scr,event)
        maxAmpl = get(scr,'Value');
        multiMeas = sin(linspace(0,2*pi,30)) * maxAmpl/max(max(abs(measEV)));
        multiSim  = sin(linspace(0,2*pi,30)) * maxAmpl/max(max(abs(simEV)));
        
    end

    function CB_measChkBx(scr,event)
        if get(scr,'Value')
            set(pMeas,'Visible','on');
        else
            set(pMeas,'Visible','off');
        end
    end

    function CB_simChkBx(scr,event)
        if get(scr,'Value')
            set(pSim,'Visible','on');
        else
            set(pSim,'Visible','off');
        end      
    end

    function CB_captureGif(scr,event)
        capture = true;
        n = 1;
    end

end


function [MAC] = F_calculateMAC(Phi1,Phi2)
    MAC = abs(Phi1'*Phi2)^2/((Phi1'*Phi1)*(Phi2'*Phi2));
end
