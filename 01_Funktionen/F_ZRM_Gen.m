function [A_CB_mod,B_CB_mod,C_CB_mod,D_CB_mod]=F_ZRM_Gen(M_mod,K_mod,D_mod, T, V_Ges, S, G, modal)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%F_ZRM_Gen - Kurzberschreibung
%Funktion ersatellt aus den dyn. Matrizen ein Zustandsraummodell (ZRM) und
%sortiert die Ausgänge und Eingänge nach dem Simulinkmodell von rb
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FUNKTIONSAUFRUF:
%[sys_CB]=F_ZRM_Gen(M_Ges,K_Ges,Damp,V_Ges,T,s)
%PARAMETERS:
%S - Sortiermatrix zu Reihenfolge Simulinkmodell rb
%INPUT:
%M_Ges - Massenmatrix in Craig-Bampton-Form
%K_Ges - Steifigkeitsmatrix in Craig-Bampton-Form
%Damp - Dämpfungsmatrix in Craig-Bampton-Form
%V_Ges - Craig-Bampton-Eigenvektoren
%T - Sortiermatrix: Sortiert nach Eigenfrequenz aufsteigend
%s - Zuordnung Koppelpunkte zu Eingängen Simulinkmodell rb
%g - Zuordnung Koppelpunkte zu Geschwindigkeitsausgänge Simulinkmodell rb
%p - Zuordnung Koppelpunkte zu Postionsausgänge Simulinkmodell rb
%OUTPUT:
%sys_CB - ZRM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isempty(G)
   G=1; 
end
if isempty(S)
   S=1; 
end
if isempty(T)
   T=eye(size(M_mod)); 
end

A11_CB_mod= -M_mod\D_mod;
A12_CB_mod= -M_mod\K_mod;
A21_CB_mod= eye(size(A11_CB_mod));
A22_CB_mod= zeros(size(A11_CB_mod));

if modal==1
    %Wenn Modalraum, dann Mit V_Ges
    B11_CB_mod=S'*(M_mod\(T*V_Ges'))';
    B21_CB_mod=zeros(size(B11_CB_mod));
    
    C1_CB_mod=[V_Ges*T',zeros(size(V_Ges*T'))];
    C2_CB_mod=[zeros(size(V_Ges*T')),V_Ges*T'];
else
    B11_CB_mod=S'*(M_mod\(T))';
    B21_CB_mod=zeros(size(B11_CB_mod));
    
    C1_CB_mod=[T',zeros(size(T'))];
    C2_CB_mod=[zeros(size(T')),T'];
end

A_CB_mod=[A11_CB_mod, A12_CB_mod; A21_CB_mod, A22_CB_mod];
B_CB_mod=[B11_CB_mod'; B21_CB_mod'];
C_CB_mod=[C1_CB_mod; C2_CB_mod];

%Output ändern
C_CB_mod=G'*C_CB_mod;
D_CB_mod=zeros(size(C_CB_mod,1), size(B_CB_mod,2));

%----- F_ZRM_Gen END