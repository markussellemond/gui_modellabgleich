function [output] = F_cb_result(omega_Ges,V_Ges,ASET)
            
% Eigenvectors
grid=floor((ASET.GES)./10).*10+1;%auf sz format bringen
omega0=diag(omega_Ges)
[omega,I]=sort(omega0)%Sort omega 
V_Ges0=V_Ges(:,I) %Sort omega 


for l=1:length(omega)
for k=1:(length(ASET.GES)/6)%for each ASET point
output.BOPHIG{1,1}{1,l}(k,1) = grid(6*(k-1)+1);
output.BOPHIG{1,1}{1,l}(k,2) = V_Ges0(6*(k-1)+1,l);
output.BOPHIG{1,1}{1,l}(k,3) = V_Ges0(6*(k-1)+2,l);
output.BOPHIG{1,1}{1,l}(k,4) = V_Ges0(6*(k-1)+3,l);
output.BOPHIG{1,1}{1,l}(k,5) = 1;
output.BOPHIG{1,1}{1,l}(k,6) = 1;
output.BOPHIG{1,1}{1,l}(k,7) = 1;
end
end

 % Eigenfrequencies
 for n=1:length(omega)
output.LAMA{1,1}(n,1) = n;
output.LAMA{1,1}(n,2) = n;
output.LAMA{1,1}(n,3) = omega(n);
output.LAMA{1,1}(n,4) = sqrt(omega(n));
output.LAMA{1,1}(n,5) = sqrt(omega(n))/(2*pi);
output.LAMA{1,1}(n,6) = 1;
output.LAMA{1,1}(n,7) = output.LAMA{1,1}(n,3);
 end

 end