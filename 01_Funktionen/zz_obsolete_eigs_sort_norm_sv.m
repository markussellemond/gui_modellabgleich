function  [V_sort,w_sort]=eigs_sort_norm_sv(K,M,anz_AM)

[V_unsort, w_unsort]=eigs(K,M,anz_AM,'sm');

% m_r=V_unsort'*M*V_unsort;
% k_r=V_unsort'*K*V_unsort;
% 
% m_r=diag(diag(m_r));
% 
% w_unsort_norm=m_r\k_r;
% V_unsort_norm=V_unsort*sqrt(inv(m_r));

% [freq_sort_norm,order]=sort(diag(w_unsort_norm));%'descend'
[freq_sort,order]=sort(diag(w_unsort),'descend');%'descend'


w_sort=diag(freq_sort);
V_sort=V_unsort(:,order);
end


