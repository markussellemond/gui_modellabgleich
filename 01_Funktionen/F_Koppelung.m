function [Ges]=F_Koppelung(con,normal_modes,komp,AAX)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%F_Koppelung - Kurzberschreibung
%Funktion koppelt die dynamischen Matrizen der einzelnen Komponenten nach
%Craig-Bampton
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FUNKTIONSAUFRUF: 
%[Ges]=F_Koppelung(con,normal_modes,komp,AAX)
%PARAMETERS:
%INPUT:
%con - Anzahl an Koppelstellenfreiheitsgraden im Gesamtsystem(constrains)
%normal_modes - Anzahl an Normalmodes der K�rper
%komp - Name der Komponente
%M_Ges - gekoppelte Massenmatrix zum normieren
%AAX - Dyn. Matrix in Craig-Bampton-Form length(AAX) = normalmodes(der Komponente + alle constraintmodes
%OUTPUT:
%Ges - Gekoppelte dyn. Matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

M11=zeros(con,con);     % links oben alle constraintmodes
M12=[];
M21=[];
M22=zeros(normal_modes.ges,normal_modes.ges);   %rechts unten alle normal modes
for i=1:length(komp)
    M11=[AAX.(sprintf(komp{i}))(1:con,1:con)+M11];
        [a,c]=size(M21);
    M12_temp=AAX.(sprintf(komp{i}))(1:con,con+1:end);
    M12=[M12,M12_temp];
    M21=[M21;M12_temp'];
        b=a+length(M12_temp(1,:));
    M22(a+1:b,a+1:b)=[AAX.(sprintf(komp{i}))(con+1:end,con+1:end)];    
end
Ges=[M11 ,M12; M21, M22];

%----- F_Koppelung END