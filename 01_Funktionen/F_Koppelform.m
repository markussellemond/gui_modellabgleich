function [AAX_sort]=F_Koppelform(AAX,A,ASET)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%F_Koppelform - Kurzberschreibung
%Funktion bringt die dynamischen Matrizen der einzelnen Komponenten nach
% in die passende Koppelform des Gesamtsystems (Einf�gen von
% Leerzeilen/Leerspalten bis so dass gleiche Punkte in selber Zeile/Spalte
% stehen.)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FUNKTIONSAUFRUF: 
%[AAX_sort]=Koppelform(AAX,A,ASET)
%PARAMETERS:
%aset_couple - Zeilennummer des Koppelpunkte im Gesamtsystem
%INPUT:
%AAX - Dyn. Matrix in Craig-Bampton-Form
%A - alle Koppelfreiheitsgrade im System, sortiert
%ASET - Koppelfreiheitsgrade der aktuellen Komponente
%OUTPUT:
%AAX_sort - erweiterte und geordnete dyn Matrix anhand der Koppelstellen
%des Gesamtsystems
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[a,aset_couple]= ismember(ASET(:,3),A);
modes=length(AAX);
delta=length(A)-length(ASET); % constraintmodes der anderen komponenten
AAX_sort0=zeros(delta+modes);
AAX_sort=zeros(delta+modes);
%Lehrspalten einf�gen f�r Koppel-DOF der anderen K�rper
for i=1:modes
    if i <= length(ASET)
        AAX_sort0(1:modes,aset_couple(i))=AAX(:,i) ;
    else
        AAX_sort0(1:modes,delta+i)=AAX(:,i);
    end
end   
%Lehrzeilen einf�gen f�r Koppel-DOF der anderen K�rper
for i=1:modes
    if i <= length(ASET)
        AAX_sort(aset_couple(i),:)=AAX_sort0(i,:) ;
    else
        AAX_sort(delta+i,:)=AAX_sort0(i,:);
    end
end  
end
%----- F_Koppelform END