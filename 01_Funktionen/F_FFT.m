function [freq, absH, H_fft] = ...
    F_FFT(xdata, ydata, L_signal, N_mit, T_pause, Integration, ...
    time_start, time_stop, UF, OF)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%F_FFT - Kurzberschreibung
%Funktion berechnet FFT vom Datenvektor ydata �ber Mittelungen N_mit
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FUNKTIONSAUFRUF: 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%[freq_out, absH, H_fft] = F_FFT(xdata, ydata, L_signal, N_mit, T_pause, Integration, ...
%    time_start, time_stop, UF, OF)
%PARAMETERS:
%max_Vector_length - maximale laenge, die Vektor fuer DFT haben darf
%INPUT:
%ydata - Datenvektor im Zeitbereich, der Schwingungen enth�lt (SPALTENVEKTOR)
%xdata - Zeitvektor (SPALTENVEKTOR)
%L_signal - L�nge des tats�chlichen Nutzsignals
%N_mit - Anzahl der Mittelungen
%T_pause - Pause zwischen den Signalen in sek
%Integration - Gibt an wie oft integriert werden soll
%time_start - Zeit ab der das Nutzsignal ausgewertet wird
%time_stop - Zeit bis das Nutzsignal ausgewertet wird
%UF - Untere Frequenz f�r die fineFFT
%OF - Obere Frequenz f�r die fineFFT
%OUTPUT:
%absH - absolute Amplituden der FFT
%freq_out - Frequenzvektor
%H_fft - imagin�re Werte der FFT

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin == 2 
    L_signal = length(xdata);
    N_mit = 1; 
    T_pause = 0;
    Integration = 0;
    time_start = 0;
    time_stop = 0;
    UF = 0;
    OF = 0;
end

if nargin == 6 
    time_start = 0;
    time_stop = 0;
    UF = 0;
    OF = 0;
end

if nargin == 8 
    UF = 0;
    OF = 0;
end

Ts = xdata(2) - xdata(1); %Bestimmen der Abtastzeit, vom Zeitvektor, 
%Abtastzeit ist die Differenz von zwei aufeinander folgenden Werten
L_pause = floor(T_pause/Ts);
samp_start = floor(time_start/Ts);
samp_stop = L_signal - floor(time_stop/Ts);

if ((time_stop == 0) || (samp_stop < 0))
    samp_stop=0;
end

if (samp_start < 0)
    samp_start=0;
end

N = L_signal - samp_start - samp_stop;

if (UF == 0) && (OF == 0) %FFT oder DFT?
    
    deltaf = 1/(N*Ts);
    
    freq = (0:deltaf:1/(2*Ts)-deltaf)'; %Frequenzvektor, f�r FFT-Diagramm,
    %von Null(keine Frequenz enthalten z.B. station�r), in den kleinsten
    %Frequenzschritten, 1/Ts ist die Abtastfrequenz und durch N ensteht
    %die kleinste Frequenzeinheit (Frequenzaufl�sung) wenn ich wenig
    %Messwerte habe, habe ich eine schlechte Aufl�sung
else
    %Maximal verfuegbaren Speicher fuer ein float Array (8byte) abfragen
    maxmem = memory;
    maxmem = maxmem.MaxPossibleArrayBytes/8;
    %durch 3 teilen fuer komplexen Vektor
    maxmem = (maxmem/3);
    
    %Ueberpruefen, ob komplexe Vektormultiplikation moeglich
    if length(ydata) < maxmem
        deltaf = (OF-UF)/100;
        freq = (UF:deltaf:OF)';
        h = waitbar(0,'Bitte warten...');
        steps = length(freq(:,1))*N_mit;
    else % sonst DFT deaktivieren
        UF = 0;
        OF = 0;
        deltaf = 1/(N*Ts);
        freq = (0:deltaf:1/(2*Ts)-deltaf)';
    end
end

for i=1:N_mit

    start_temp = (i-1)*(L_signal+L_pause)+1+L_pause;
    stop_temp = i*(L_signal + L_pause);
    
    start_temp = start_temp + samp_start;
    stop_temp = stop_temp - samp_stop;
    
    if (UF == 0) && (OF == 0) %FFT oder DFT?
        H_work = fft(ydata(start_temp:stop_temp));
        
        H_work = H_work/N;
        H_fft_work(:,1) = 2*H_work(1:floor(N/2),1); %H mal 2, weil aufgrund der FFT die Leistung aufgeteilt wird(Spiegelung),
        H_fft_work(1,1) = H_work(1); %o wird nicht gespiegelt, der erste WERT enspricht der Frequenz Null,
        clear H_work;
    else
        
        H_fft_work = complex(zeros(length(freq(:,1)),1),...
            zeros(length(freq(:,1)),1));
        
        for k = 1:length(freq(:,1))
            H_work = exp(-1i*2*pi*(freq(k,1)*...
                (xdata(start_temp:stop_temp))'))*...
                ydata(start_temp:stop_temp);
            
            H_work = H_work/N;
            if k == 1
                H_fft_work(k,1) = H_work;
            else
                H_fft_work(k,1) = 2*H_work;
            end
            %Waitbar aktualisieren
            waitbar((k + ((i-1)*steps/N_mit))/steps);
        end
        clear H_work;
    end
    
    
    for j = 1:Integration
        H_fft_work = H_fft_work./(2*pi*1i*freq);
    end
    
    H_fft(:,i) = H_fft_work(:,1);
    absH(:,i) = abs(H_fft_work(:,1)); 
end

if ~((UF == 0) && (OF == 0))
    close(h);
end
%----- FFT END



    
        