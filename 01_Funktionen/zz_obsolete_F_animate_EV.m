%% Animate Eigenform

function F_animate_EV(V_Ges_anime,w_sort,A,nodes_ges)
        if ~isempty(V_Ges_anime)
            n1=[]; n2=[]; n3=[]; nod1=[]; nod2=[]; nod3=[];
            
                n1=[n1; nodes_ges(:,2)];
                n2=[n2; nodes_ges(:,3)];
                n3=[n3; nodes_ges(:,4)];
                
                nod1=[nod1; nodes_ges(:,1)*10+1];
                nod2=[nod2; nodes_ges(:,1)*10+2];
                nod3=[nod3; nodes_ges(:,1)*10+3];

            
            %Knotenzuordnung
            [~,m1]=ismember(nod1,A);
            [~,m2]=ismember(nod2,A);
            [~,m3]=ismember(nod3,A);
            
            %Leere Zuordnungen l�schen
            n1=n1(m1~=0);
            n2=n2(m1~=0);
            m2=m2(m1~=0);
            n3=n3(m1~=0);
            m3=m3(m1~=0);
            m1=m1(m1~=0);
            
            n1=n1(m2~=0);
            m1=m1(m2~=0);
            n2=n2(m2~=0);
            n3=n3(m2~=0);
            m3=m3(m2~=0);
            m2=m2(m2~=0);
            
            %Plotten
            anime=figure('MenuBar','none',...
                'Name','Animate Eigenforms',...
                'ToolBar','figure',...
                'NumberTitle', 'off',...
                'Units','normalized',...
                'Position',[.1,.1,.8,.8]);
            % Toolbar Modifizierung
            tbh = findall(anime,'Type','uitoolbar');
            delete(findall(tbh,'tag','Standard.NewFigure'))
            delete(findall(tbh,'tag','Standard.FileOpen'))
            delete(findall(tbh,'tag','Standard.SaveFigure'))
            delete(findall(tbh,'tag','Standard.PrintFigure'))
            delete(findall(tbh,'tag','Exploration.Brushing'))
            delete(findall(tbh,'tag','DataManager.Linking'))
            delete(findall(tbh,'tag','Annotation.InsertColorbar'))
            delete(findall(tbh,'tag','Plottools.PlottoolsOn'))
            delete(findall(tbh,'tag','Plottools.PlottoolsOff'))
            
            %Men� erstellen
            set_view_anime=uimenu(anime,'Label','Set View');
            uimenu(set_view_anime,'Label','Isometric','Tag','0','Callback',@set_view_ani);
            uimenu(set_view_anime,'Label','XY-Plane','Tag','1','Callback',@set_view_ani);
            uimenu(set_view_anime,'Label','XZ-Plane','Tag','2','Callback',@set_view_ani);
            uimenu(set_view_anime,'Label','YZ-Plane','Tag','3','Callback',@set_view_ani);
            
            %Axis
            ax_anime=axes('Parent',anime, ...
                'Units', 'normalized', ...
                'HandleVisibility','callback', ...
                'Position',[.05,.15,.9,.8]);
            
            %#Eigenmode
            uicontrol(anime,'Style','text',...
                'String','Eigenmode:',...
                'Units','normalized',...
                'Position',[.37,.05,.05,.04]);
            eig_nr_e=uicontrol(anime,'Style','edit',...
                'String','1',...
                'Units','normalized',...
                'Position',[.42,.06,.05,.04],...
                'Callback',@change_eig);
            
            %Eigenfrequenz
            freq=uicontrol(anime,'Style','text',...
                'String',['Frequency: ' num2str(w_sort(1)) 'Hz'],...
                'Units','normalized',...
                'Position',[.48,.05,.1,.04]);
            
            %Faktor
            uicontrol(anime,'Style','text',...
                'String','Scale:',...
                'Units','normalized',...
                'Position',[.58,.05,.05,.04]);
            scale_e=uicontrol(anime,'Style','edit',...
                'String','1000',...
                'Units','normalized',...
                'Position',[.62,.06,.05,.04],...
                'Callback',@change_eig);
            
            animate_e=uicontrol(anime,'Style','checkbox',...
                'String','animate',...
                'Value',1,...
                'Units','normalized',...
                'Position',[.72,.06,.05,.04]);
            
            %Slider
            eig_nr_s=uicontrol(anime,'Style','slider',...
                'String','Eigenvalue',...
                'Min',1,'Max',1,'Value',1,...
                'Units','normalized',...
                'Position',[.01,.01,.98,.04],...
                'Callback',@change_eig);
            
            %Plots
            scatter3(ax_anime,n1,n2,n3,'*'); grid('on'); hold('on');
            p=scatter3(ax_anime,n1,n2,n3,'*'); grid('on'); hold('on');
            axis('image');
            view(ax_anime,50,30);
            
            %Eigenvektor
            scale=str2double(scale_e.String);
            eig_nr=1;
            v=V_Ges_anime(:,eig_nr)*scale;
            
            %Slider anpassen
            eig_nr_s.Max=length(v);
            eig_nr_s.SliderStep=[1/length(v) 5/length(v)];
            
            %Achsen einrichten
            val=n1+v(m1);
            val2=n1+v(m1)*-1;
            xlim1=min([val; val2]);
            xlim2=max([val; val2]);
            xlim([xlim1 xlim2]);
            
            val=n2+v(m2);
            val2=n2+v(m2)*-1;
            ylim1=min([val; val2]);
            ylim2=max([val; val2]);
            ylim([ylim1 ylim2]);
            
            val=n3+v(m3);
            val2=n3+v(m3)*-1;
            zlim1=min([val; val2]);
            zlim2=max([val; val2]);
            zlim([zlim1 zlim2]);
            
            %Animation
            a=1;
            fak1=-1:0.1:1;
            fak2=1:-0.1:-1;
            
            while ishandle(anime)
                try
                    v=V_Ges_anime(:,eig_nr)*scale;
                    
                    if animate_e.Value
                        fak=[fak1, fak2];
                    else
                        fak=ones(size([fak1, fak2]));
                    end
                    
                    p.XData=n1+v(m1)*fak(a);
                    p.YData=n2+v(m2)*fak(a);
                    p.ZData=n3+v(m3)*fak(a);
                    
                    a=mod(a, length(fak));
                    a=a+1;
                    
                    %Pause
                    pause(0.1);
                end
            end
        end

    function change_eig(a,~)
            if strcmp(a.Style,'slider')
                eig_nr=round(eig_nr_s.Value);
                eig_nr_e.String=num2str(eig_nr);
                scale=str2double(scale_e.String);
            else
                eig_nr=round(str2double(eig_nr_e.String));
                eig_nr_s.Value=eig_nr;
                scale=str2double(scale_e.String);
            end
            
            freq.String=['Frequency: ' num2str(w_sort(eig_nr)) 'Hz'];
            v=V_Ges_anime(:,eig_nr)*scale;
            
            %Achsen einrichten
            val=n1+v(m1);
            val2=n1+v(m1)*-1;
            xlim1=min([val; val2]);
            xlim2=max([val; val2]);
            xlim([xlim1 xlim2]);
            
            val=n2+v(m2);
            val2=n2+v(m2)*-1;
            ylim1=min([val; val2]);
            ylim2=max([val; val2]);
            ylim([ylim1 ylim2]);
            
            val=n3+v(m3);
            val2=n3+v(m3)*-1;
            zlim1=min([val; val2]);
            zlim2=max([val; val2]);
            zlim([zlim1 zlim2]);
    end
        
            function set_view_ani(hObject, ~)
            tag=str2double(hObject.Tag);
            switch tag
                case 0
                    view(ax_anime,50,30);
                case 1
                    view(ax_anime,90,0);
                case 2
                    view(ax_anime,90,90);
                case 3
                    view(ax_anime,180,0);
            end
        end
end