function [sys_CB_mag,sys_Ref_mag,sys_CB_phase,sys_Ref_phase,f_grid]=F_Auswertung_ZRM(sys_CB,sys_Ref,i_CB,o_CB,i_Ref,o_Ref,f_lim,int)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%F_Auswertung_ZRM - Kurzberschreibung
%Funktion vergleich die Bode-Plots des Referenz ZRM mit dem gekoppelten
%ZRM 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FUNKTIONSAUFRUF: 
%[AAX_sort]=F_Auswertung_ZRM(sys_CB,sys_Ref,i_CB,o_CB,i_Ref,o_Ref,f_lim)
%PARAMETERS:
%INPUT:
%sys_CB - Craig-Bampton-ZRM
%sys_Ref - Referenz ZRM
%i_CB - Input Nummer f�r CB-ZRM
%o_CB - Output Nummer f�r CB-ZRM
%i_Ref - Input Nummer f�r Ref-ZRM
%o_Ref - Output Nummer f�r Ref-ZRM
%OUTPUT:

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% plot ZRMs

 figure

opt = bodeoptions;
opt.FreqScale = 'linear';
opt.FreqUnits = 'Hz';
opt.MagUnits = 'abs';
opt.MagScale = 'log';
opt.PhaseVisible = 'off';
% opt.PhaseWrapping = 'on';
% opt.PhaseWrappingBranch = -180;
opt.Xlim = [0 f_lim];%800
f_grid = [0:0.05:(2*f_lim)];%Hz %2000
w_grid = 2*pi*f_grid;%rad/s

[sys_CB_mag,sys_CB_phase] = bode(sys_CB(o_CB,i_CB),opt,w_grid);
if int ==1
semilogy(f_grid,squeeze(sys_CB_mag)/1000./w_grid','c')
else
 semilogy(f_grid,squeeze(sys_CB_mag)/1000,'c')
end
hold on;

[sys_Ref_mag,sys_Ref_phase] = bode(sys_Ref(o_Ref,i_Ref),opt,w_grid);
if int==1
semilogy(f_grid,squeeze(sys_Ref_mag)/1000./w_grid','m')
else
semilogy(f_grid,squeeze(sys_Ref_mag)/1000,'m')
end
hold on;


legend('CB gekoppelt modales ZRM',...
      'Referenz: Gesamt ZRM'    );
  
title('Nachgiebigkeit in m/N')  

grid on;
xlim([0 f_lim])
hold off;
%----- F_Auswertung_ZRM END