clear all
clc

 name='zSchlitten_mSpindel_komplett';
% name='Bett_komplett';
% name='ySchlitten_komplett';
% name='xSchlitten_komplett';


path_in=[cd '\01_Daten_FE\01_PCH\' name '.pch'];
[ASET0, KAAX0, MAAX0, BAAX0, DAAX0, nodes]=F_read_pch(path_in);


path_out=[cd '\01_Daten_FE\02_DynMat\' name '.mat'];
save(path_out,'ASET0','BAAX0','DAAX0','KAAX0','MAAX0','nodes')

% omega
[V_Ges,omega_Ges]=eig(MAAX0\KAAX0);
w_Ges=diag(sqrt(abs(omega_Ges))./2./pi);
[w_sort,I]=sort(w_Ges);
V_Ges_sort=V_Ges(:,I);