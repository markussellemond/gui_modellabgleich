function [status, MAC_diag, FigHandle] = F_calc_MAC(proj_dir_name,...
    model_dir_name,exp_or_model_dir_name,mode,nodes_ges)
% berechnet Modalkorrelation nach MAC-Kriterium zwischen Modellzustaenden
% last check:
%
% INPUT
% proj_dir_name: aktuelles Projektverzeichnis
% model_dir_name: aktuelles modell
% model_dir_name2: anderes modell zum vergleich
% mode: 'noplot' fuer keinen Plot
%       'matrix' um die MAC-Matrix zu berechnen
%       'matrix_limit' um die MAC-Matrix fuer bestimmte Moden zu berechnen
%       'matrix_sim' um die MAC-Matrix mit einem anderen Modellzustand zu
%       berechnen
%       'matrix_sim_limit' um die MAC-Matrix fuer bestimmte Moden mit einem
%       anderen Modellzustand zu berechnen
%
%
%
status = 0;
%## -- Pfad-Check -- ######################################################
% if ~(exist('init_tool.m','file'))
%     disp([mfilename,':']);
%     disp('falscher Arbeitspfad');
%     status = -4;
%     return
% end
%## -- argin-Check -- #####################################################

% if (nargin < 3)
%     disp([mfilename,':']);
%     disp('Zu wenige Input Argumente');
%     status = -1;
%     return
% end

if (nargin < 4)
    mode = 'noplot';
end

%Rueckgabe-Variable initialisieren
FigHandle = [];
MAC_diag = [];

%Maximale Distanz f�r die Knotenzuordnung
max_Dist = 50;

%--------------------------------------
%Einheitenkonvertierungsfaktor abfragen
%--------------------------------------
answer = inputdlg({sprintf('Faktor zur Konvertierung\nExperiment -> Simulation:\n')},'Einheitenfaktor', ...
    1,{num2str(1)},struct('WindowStyle','normal'));
if ~isempty(answer)
    ConvertExptoSimUnitFactor = str2double(answer{1});
else
    helpdlg('Kein Faktor angegeben!','Info');
    return
end

%---------------------------------------------------
%UNV-Datei der experimentellen Modalanalyse einlesen
%---------------------------------------------------
if ~strcmp(mode,'matrix_sim') && ~strcmp(mode,'matrix_sim_limit')
    %UNV-File auswaehlen
    [UNV_File, UNV_Path] = uigetfile({'*.unv','Universal File (*.unv)'},...
        'Select Experimental Modal Data File',[proj_dir_name,'\',exp_or_model_dir_name]);
else
    [UNV_File, UNV_Path] = uigetfile({'*.unv','Universal File (*.unv)'},...
        'Select Coordinate Modal Data File',proj_dir_name);
end

if UNV_File~=0
    %UNV File einlesen
    UNV_content = F_readuff([UNV_Path,UNV_File]);
else
    helpdlg('Kein UNV-File angegeben!','Info');
    return
end

[Nodes] = GetUNVCoordinates(UNV_content,ConvertExptoSimUnitFactor);

if ~strcmp(mode,'matrix_sim') && ~strcmp(mode,'matrix_sim_limit')
    [Phi_Exp,Nodes] = LoadPhiExp(UNV_content,Nodes,ConvertExptoSimUnitFactor);
    if ~isempty(Phi_Exp)
        [Phi_Sim,MeasNodesLines] = LoadPhiSim(proj_dir_name,model_dir_name,Nodes, max_Dist,nodes_ges);
        for count=1:length(Phi_Exp)%sv: reduce Phi_Exp to simulated nodes
        Phi_Exp(count).Vector=Phi_Exp(count).Vector(MeasNodesLines,:)
        end
        if ~isempty(Phi_Sim)
            [status,Phi_Sim] = SelectPhiSimModes(Phi_Sim);
            if status
                [MACArray,YLabel,XLabel,MAC_diag] = Calc_MAC_Array(Phi_Sim,Phi_Exp);
            else
                helpdlg('Keine simulierten Frequenzen ausgew�hlt!','Info');
                return
            end
        else
            return
        end
    else
        return
    end
else
    [Phi_Sim] = LoadPhiSim(proj_dir_name,model_dir_name,Nodes, max_Dist,nodes_ges);
    if ~isempty(Phi_Sim)
        [Phi_Sim2] = LoadPhiSim(proj_dir_name,exp_or_model_dir_name,Nodes, max_Dist,nodes_ges);
        if ~isempty(Phi_Sim2)
            [status,Phi_Sim2] = SelectPhiSimModes(Phi_Sim2);
            if status
                [MACArray,YLabel,XLabel,MAC_diag] = Calc_MAC_Array(Phi_Sim,Phi_Sim2);
            else
                helpdlg('Keine simulierten Frequenzen ausgew�hlt!','Info');
                return
            end
        else
            return
        end
    else
        return
    end
end

%-------------------------------
%Matrixdarstellung der MAC-Werte
%-------------------------------
if ~strcmp(mode,'noplot')
    [FigHandle,AxHandle] = Plot_MAC_Matrix(MACArray,YLabel,XLabel,Nodes,mode);
    Datum = date;
    if ~strcmp(mode,'matrix_sim') && ~strcmp(mode,'matrix_sim_limit')
        title(AxHandle,['Matrix MAC-Werte','(',Datum,'): ',proj_dir_name,sprintf('\\\n'),...
            model_dir_name,' - ',UNV_File,' - Verwendete Punkte fuer MAC: ',num2str(length(MeasNodesLines))],'Interpreter','None');
    else
        title(AxHandle,['Matrix MAC-Werte','(',Datum,'): ',proj_dir_name,sprintf('\\\n'),...
            model_dir_name,' - ',exp_or_model_dir_name],'Interpreter','None');
    end
end

status = 1;
end

function [MACValue] = MAC(Phi_Sim_Vector, Phi_Exp_Vector)
MACValue = abs(sum(sum(Phi_Sim_Vector.*conj(Phi_Exp_Vector)))).^2/...
    (sum(sum(Phi_Sim_Vector.*conj(Phi_Sim_Vector))).*...
    sum(sum(Phi_Exp_Vector.*conj(Phi_Exp_Vector))));
end

function [Nodes] = GetUNVCoordinates(UNV_content,ConvertExptoSimUnitFactor)
%Messkoordinaten aus UNV-File herausholen
entry = 1;
%entry mit Messkoordinaten bestimmen
while entry <= length(UNV_content(1,:))
    if UNV_content{1,entry}.dsType == 2411
        break
    else
        entry = entry + 1;
    end
end
Nodes = [UNV_content{1,entry}.nodeN,...
    UNV_content{1,entry}.x*ConvertExptoSimUnitFactor,...
    UNV_content{1,entry}.y*ConvertExptoSimUnitFactor,...
    UNV_content{1,entry}.z*ConvertExptoSimUnitFactor,...
    zeros(length(UNV_content{1,entry}.nodeN(:,1)),1)];
Nodes = sortrows(Nodes,5);
end

function [Phi_Exp,Nodes] = LoadPhiExp(UNV_content,Nodes,ConvertExptoSimUnitFactor)
%Matrix der gemessenen Eigenvektoren aufbauen und ins
%Simulationseinheitensystem umrechnen
Phi_Exp = struct([]);
count = 0;
for i = 1:length(UNV_content(1,:))
    if UNV_content{1,i}.dsType == 55 && isfield(UNV_content{1,i},'modeFreq')
        count = count + 1;
        Phi_Exp(count).Freq = UNV_content{1,i}.modeFreq;
        Phi_Exp(count).ModeNumber = UNV_content{1,i}.modeNum;
        Phi_Exp(count).Vector = sortrows([...
            UNV_content{1,i}.nodeNum,...
            UNV_content{1,i}.r1.*sqrt(ConvertExptoSimUnitFactor),...
            UNV_content{1,i}.r2.*sqrt(ConvertExptoSimUnitFactor),...
            UNV_content{1,i}.r3.*sqrt(ConvertExptoSimUnitFactor)],1);
    end
end

%Alle gemessenen Knoten bestimmen und evtl. Nodes verkuerzen
ExpNodes = [];
for i = 1:length(Phi_Exp)
    ExpNodes = [ExpNodes;Phi_Exp(i).Vector(:,1)];
end
ExpNodes = sortrows(unique(ExpNodes));
if isempty(setdiff(ExpNodes,intersect(Nodes(:,1),ExpNodes)))
    [~,ExpNodesLines,~] = intersect(Nodes(:,1),ExpNodes);
    Nodes = Nodes(ExpNodesLines,:);
    Nodes = sortrows(Nodes,5);
else
    Phi_Exp = struct([]);
    helpdlg('Eigenvektoren ohne Koordinaten gefunden, Abbruch!','Info');
    return
end
end

function [Phi_Sim,MeasNodesLines] = LoadPhiSim(proj_dir_name,model_dir_name,Nodes, max_Dist,nodes_ges)
Phi_Sim = struct([]);
%--------------------------------------------
%Simulationsergebnisse vorhanden? -> einlesen
%--------------------------------------------
if exist([proj_dir_name,'\',model_dir_name,'\03_Berechnung\Results_MS.mat'],'file')
    
    %Results mat laden
    load([proj_dir_name,'\',model_dir_name,'\03_Berechnung\Results_MS.mat'],'result');
    
    %BOPHIG und LAMA vorhanden?
    if ~(isfield(result,'BOPHIG') && isfield(result,'LAMA'))
        helpdlg('In Results_MS-Ergebnisdatei keine LAMA oder BOPHIG gefunden!','Info');
        return
    end
    
    %Varianten vorhanden? -> plugin_data laden und Decknamen einlesen
    Var_Name = [];
    Var_Ind = 1;
    if size(result.BOPHIG,1)>1
        %plugin_data laden
        load([proj_dir_name,'\',model_dir_name,'\data.mat'],'plugin_data');
        if ~(isfield(plugin_data,'ms') && isfield(plugin_data.ms,'meshname'))
            helpdlg('Keine Variantennamen in data.mat gefunden!','Info');
            return
        end
        [status,Var_Name,Var_Ind] = SelectVar(plugin_data.ms.meshname,model_dir_name);
        clear('plugin_data');
        if ~status
            helpdlg('Keine Variante ausgew�hlt!','Info');
            return
        end
    end
else
    helpdlg('Keine Results_MS-Ergebnisdatei gefunden!','Info');
    return
end

%------------------
%BDF-Datei einlesen
%------------------
% if isdir([proj_dir_name,'\',model_dir_name,'\02_Vernetzung']) &&...
%         ~isempty(dir([proj_dir_name,'\',model_dir_name,'\02_Vernetzung\*.bdf']))
%     if ~isempty(Var_Name)
%         bdffile = dir([proj_dir_name,'\',model_dir_name,'\02_Vernetzung\',Var_Name,'.bdf']);
%     else
%         bdffile = dir([proj_dir_name,'\',model_dir_name,'\02_Vernetzung\*.bdf']);
%     end
%     
%     %bdf model einlesen
%     [~, ~, model_data ] = F_read_bdf_Worker([proj_dir_name,'\',...
%         model_dir_name,'\02_Vernetzung\'],bdffile.name);
%     
%     %[~, ~, model_data ] = F_read_bdf([proj_dir_name,'\',...
%     %    model_dir_name,'\02_Vernetzung\'],bdffile.name);
%    
%    %Knotennummern aus Modell mit Hilfe der Messkoordinaten bestimmen
%     [~, Nodes(:,5), min_dist] = F_find_node(model_data, Nodes(:,2:4));
%     clear('model_data');

% find nodes sv temp
num_nodes = size(Nodes,1);
nodes0 = zeros(1,num_nodes);
min_dist = zeros(1,num_nodes);

for n=1:num_nodes
    x = Nodes(n,2);
    y = Nodes(n,3);
    z = Nodes(n,4);
    clear dist min_dist_n index new_node;
    %Abstand berechnen
    dist = sqrt((nodes_ges(:,2)-x).^2 + (nodes_ges(:,3)-y).^2 + (nodes_ges(:,4)-z).^2);
    %geringsten Abstand finden
    [min_dist_n,index] = min(dist);
    new_node = nodes_ges(index,1);
    nodes0(1,n) = new_node;
    min_dist(1,n) = min_dist_n;
end   
Nodes(:,5)=nodes0;
    %Knoten mit Abstand gr��er max_Dist vernachl�ssigen
    Nodes = Nodes((min_dist<max_Dist)',:);
   
    %Nach Knotennummern sortieren
    Nodes = sortrows(Nodes,5);
    
    %Pr�fen ob Knoten doppelt zugeordnet wurden
    if numel(unique(Nodes(:,5)))~=numel(Nodes(:,5))
        fprintf('Warnung: Knoten doppelt zugeordnet!\n');
        fprintf('Knotenzuordnung unter MAC_Node_aligment.mat im Modellzustands-Verzeichnis abgelegt!\n');
        %save([proj_dir_name,'\',model_dir_name,'\MAC_Node_aligment.mat'],'Nodes');
    end
    
    fprintf('Maximale Distanz bei Knotenzuordnung: %3.2g, Mittelwert: %3.2g, %d Knoten zugeordnet\n',...
        max(min_dist), mean(min_dist), size(Nodes,1));

%Matrix der simulierten Eigenvektoren aufbauen
Phi_Sim = struct([]);
count = 0;
for i = 1:length(result.BOPHIG{Var_Ind,1})
    count = count + 1;
    %Nodenummer korrigieren
    SimNodes = floor(result.BOPHIG{Var_Ind,1}{1,i}(:,1)./10);
    
    %Gemessene Knoten aus simuliertem Vektor heraussuchen
    [~,SimNodesLines,MeasNodesLines] = intersect(SimNodes,Nodes(:,5));
    
    %In Matrix schreiben
    Phi_Sim(count).Freq = result.LAMA{Var_Ind,1}(i,5);
    Phi_Sim(count).ModeNumber = result.LAMA{Var_Ind,1}(i,1);
    Phi_Sim(count).Vector = [floor(result.BOPHIG{Var_Ind,1}{1,i}(SimNodesLines,1)./10),...
        result.BOPHIG{Var_Ind,1}{1,i}(SimNodesLines,2:4)];
    %Vektor nach Knotennummern sortieren
    Phi_Sim(count).Vector = sortrows(Phi_Sim(count).Vector,1);
    %Wenn Nodes und Vektornummern gleich sind, Knotennummern in Vektor
    %uebernehmen und danach sortieren
    if (size(Phi_Sim(count).Vector(:,1),1)==size(Nodes(:,5),1)) && ...
            isequal(Phi_Sim(count).Vector(:,1),Nodes(:,5))
        Phi_Sim(count).Vector(:,1) = Nodes(:,1);
        Phi_Sim(count).Vector = sortrows(Phi_Sim(count).Vector,1);
    else
        helpdlg(sprintf('Fehler bei Knotenzuordnung in Mode %d!',Phi_Sim(count).ModeNumber),'Fehler');
        return
    end
end
clear('result');
pause(0.5);
end

function [MACArray,YLabel,XLabel,MAC_diag] = Calc_MAC_Array(Phi_1,Phi_2)
%-----------------------
%MAC-Kriterium berechnen
%-----------------------
%MAC-Werte fuer alle Vektorkombinationen berechnen
MACArray = zeros(length(Phi_1)+1,length(Phi_2)+1);
MAC_diag = zeros(max(arrayfun(@(x) (x.ModeNumber), Phi_1)),1);
YLabel = zeros(length(Phi_1)+1,1);
XLabel = zeros(1,length(Phi_2)+1);
for i = 1:length(Phi_1)
    YLabel(i) = Phi_1(i).Freq;
    %YLabel(i) = Phi_1(i).ModeNumber;
    for j = 1:length(Phi_2)
        if i==1
            XLabel(j) = Phi_2(j).Freq;
            %XLabel(j) = Phi_2(j).ModeNumber;
        end
        MACArray(i,j) = MAC(Phi_1(i).Vector(:,2:4), Phi_2(j).Vector(:,2:4));
        %MAC-Werte der Diagonale in Rueckgabevektor eintragen
        if i==j
            MAC_diag(Phi_1(i).ModeNumber,1)= MACArray(i,j);
        end
    end
end
end

function [status,Phi_Sim] = SelectPhiSimModes(Phi_Sim)
status = 0;
%Fuer den Vergleich zu berueksichtigende simulierte Eigenfrequenzen waehlen
listbox_string = cell(length(Phi_Sim),1);
for i = 1:length(listbox_string)
    listbox_string{i} = sprintf('EF %2d: %.2f Hz',Phi_Sim(i).ModeNumber,Phi_Sim(i).Freq);
end

[FindInd,ok] = listdlg('ListString',listbox_string,'ListSize',[300 400],...
    'InitialValue',1:1:length(listbox_string),'Name','Simulierte Eigenfrequenzen',...
    'PromptString','Eigenfrequenzen f�r MAC-Plot ausw�hlen:');
if ok
    %Phi Sim auf ausgewaehlte Eigenfrequenzen verkuerzen
    Phi_Sim = Phi_Sim(FindInd);
    status = 1;
else
    return
end
end

function [status,Meshnames,VarInd] = SelectVar(Meshnames,Modelname)
status = 0;
%Fuer den Vergleich zu berueksichtigende Variante waehlen
[VarInd,ok] = listdlg('ListString',Meshnames,'ListSize',[300 400],...
    'InitialValue',1,'SelectionMode','single','Name',Modelname,...
    'PromptString','Variante f�r Vergleich w�hlen:');
if ok
    %Meshnames auf ausgewaehlte Variante verkuerzen
    Meshnames = Meshnames{VarInd};
    status = 1;
else
    VarInd = [];
    return
end
end

function [FigHandle,AxHandle] = Plot_MAC_Matrix(MACArray,YLabel,XLabel,Nodes,mode)
FigHandle = [];
AxHandle = [];
Lower_Limit = 0.3;
%Sollen nur bestimmte Moden angezeigt werden
if strcmp(mode,'matrix_limit') || strcmp(mode,'matrix_sim_limit')
    choice = inputdlg({sprintf('Welche Moden sollen verwendet werden?\nH�chste Modennummer: %g\n\nVon Mode Nr.:',...
        min(size(MACArray))-1),'bis einschl. Mode Nr.:'},...
        'Modenauswahl',1,{num2str(1),num2str(min(size(MACArray))-1)},...
        struct('WindowStyle','normal'));
    if isempty(choice)
        return
    else
        min_mode = str2double(choice{1});
        max_mode = str2double(choice{2})+1;
        if min_mode > max_mode
            min_mode = str2double(choice{2});
            max_mode = str2double(choice{1})+1;
        end
        if max_mode > min(size(MACArray))
            max_mode = min(size(MACArray));
        end
        MACArray = MACArray(min_mode:max_mode,min_mode:max_mode);
        XLabel = XLabel(min_mode:max_mode);
        YLabel = YLabel(min_mode:max_mode);
    end
    clear('choice');
else
    min_mode = 1;
end

FigHandle = figure('Name','Matrix MAC-Werte'...
    ,'Units', 'centimeters','Position', [0.63 0.63 28.41 19.72]);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperUnits', 'centimeters');
set(gcf, 'PaperSize', [29.68 20.98]);
set(gcf, 'PaperPosition', [0.63 0.63 28.41 19.72]);
%Achsen und Matrix um 1 erweitern um pcolor richtig zu nutzen
pcolor(1:1:length(XLabel),(1:1:length(YLabel))',MACArray);
%XLabel und YLabel fuer Beschriftung kuerzen
XLabel = XLabel(1:(end-1));
YLabel = YLabel(1:(end-1));
%Invertierte graue colormap von 0 bis 1
% select the colormap you wish to invert:
colormap gray;
% get the matrix containing that colormap:
cmap = colormap;
% flip the matrix:
cmap = flipud(cmap);
% apply the new inverted colormap:
colormap(cmap);
c = colorbar;
AxHandle = gca;
set(AxHandle,'CLim',([Lower_Limit 1]));
%Beschriftung einfuegen
set(AxHandle,'FontSize',12);
if ~strcmp(mode,'matrix_sim') && ~strcmp(mode,'matrix_sim_limit')
    xlabel({' ';' ';' ';'exp. Eigenfrequenz in Hz'});
    ylabel('sim. Eigenfrequenz in Hz');
else
    xlabel({' ';' ';' ';'Vergleichsmodell: Eigenfrequenz in Hz'});
    ylabel('Basismodell: Eigenfrequenz in Hz');
end
set(AxHandle,'XTick',(1:1:length(XLabel))+0.5);
set(AxHandle,'XTickLabel',num2str(XLabel(:),'%3.1f'));
set(AxHandle,'YTick',((1:1:length(YLabel))')+0.5);
set(AxHandle,'YTickLabel',num2str(YLabel(:),'%3.1f'));
%Vertikale Y-Beschriftung hinzufuegen
usethesevalues=get(AxHandle,'XTickLabel');
set(AxHandle,'XTickLabel',[]);
putthemhere=get(AxHandle,'XTick');
ylimits=get(AxHandle,'Ylim');
ypos=ylimits(1)-.01*(ylimits(2)-ylimits(1));
th=text(putthemhere,ypos*ones(1,length(putthemhere)),usethesevalues,...
    'HorizontalAlignment','right','rotation',90,'parent',AxHandle,'FontSize',12);
XLim = get(AxHandle,'XLim');
YLim = get(AxHandle,'YLim');
% if max(XLim)>(max(YLim)+7)
%     xlim([min(XLim) (max(YLim)+7)]);
% end
% XLim = get(gca,'XLim');
CLabel = get(c,'YTickLabel');
%CLabel = [repmat([' '],size(CLabel,1),1),CLabel];%sv
CLabel(1,:) = ['<',CLabel(1,2:end)];
set(c,'YTickLabel',CLabel);
ylabel(c,sprintf('Modal Assurance Criterion (%d Messpunkte)',size(Nodes,1)));
%Diagonale mit weissen, gestrichelten Linien kennzeichnen
line([min(XLim) max(XLim)-1],[min(XLim)+1 max(XLim)],'LineStyle','--','LineWidth',0.5,'Color',[0 0 0]);
line([min(XLim)+1 max(XLim)],[min(XLim) max(XLim)-1],'LineStyle','--','LineWidth',0.5,'Color',[0 0 0]);
%Modennummer als text auf die Diagonale schreiben
for i = 1:length(XLabel)
    if (size(MACArray,1)-1>=i) && (size(MACArray,2)-1>=i)
        if (MACArray(i,i) > Lower_Limit+0.2)
            TColor = 'w';
        else
            TColor = 'k';
        end
        text(i+0.5,i+0.5,num2str(min_mode-1+i),'HorizontalAlignment','center',...
            'Color',TColor,'FontSize',12,'parent',AxHandle);
    end
end
end